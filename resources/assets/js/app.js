var id = 0;
// Validation icon good or bad
function goodOrBad(sectionNum, className){
	if(className==='good'){
		if($('#section-'+sectionNum).hasClass('bad')){
			$('#section-'+sectionNum).removeClass('bad');
		}
		$('#section-'+sectionNum).addClass('good');
	}else if(className==='bad'){
		if($('#section-'+sectionNum).hasClass('good')){
			$('#section-'+sectionNum).removeClass('good');
		}
		$('#section-'+sectionNum).addClass('bad');
	}else{
		alert('Something wrong. Please contact us by email.');
	}
}

// All fields validation start

$(document).on('click', '.js-send-button-1', function(){
	var buttonTwo = homeFormSixBlocksValidation();
	
	if(buttonTwo){
		$('#home-form').submit();
	}
});
$(document).on('click', '.js-send-button-2', function(){
	var buttonOne = homeFormSixBlocksValidation();

	// 7 PERSONAL DETAILS validation
	if (!personalDetailsValidation()) {buttonOne = false;}else{buttonOne = true;}
	
	if(buttonOne){
		$('#home-form').submit();
	}
});
function homeFormSixBlocksValidation(){
	var returnedVar = false;
	// 1 SELECT OCCASION TYPE validation
	if($('input[name="event_type"]').val() == ''){
		goodOrBad('01', 'bad');
		returnedVar = false;
	}else{
		goodOrBad('01', 'good');
		returnedVar = true;
	}

	// 2 SELECT VENUE LOCATION validation
	if(($('input[name="location"]').val() == '') || ($('input[name="coordinates"]').val() == '')){
		goodOrBad('02', 'bad');
		returnedVar = false;
	}else{
		goodOrBad('02', 'good');
		returnedVar = true;
	}
	
	// 3 SELECT REQUIRED DATES validation
	if(!dateValidation(id)){
		returnedVar = false;
	}	
	
	// 5 VENUE OPTIONS validation
	if (!venueOptionsValidation()) {returnedVar = false;}

	// 6 BUDGET validation
	if(!($('.js-budget-overall').val()>0 || $('.js-budget-perhead').val()>0)){
		goodOrBad('06', 'bad');
		returnedVar = false;
	}

	return returnedVar;
}
// All fields validation end
$('.occasion-type1').on('click', function(){
	$('.wedding_venue_types').css('display', 'block');
	$('.party_venue_types').css('display', 'none');
});

$('.occasion-type2').on('click', function(){
	$('.wedding_venue_types').css('display', 'block');
	$('.party_venue_types').css('display', 'none');
});

$('.occasion-type3').on('click', function(){
	$('.wedding_venue_types').css('display', 'none');
	$('.party_venue_types').css('display', 'block');
});

// 1 SELECT OCCASION TYPE start
$(document).on('click', '.js-occasion-type', function(){
	var occasionTypeName = $(this).find('.js-home-tabs-name').attr('id').toLowerCase().replace(/\s+/g, '');
	$('input[name="event_type"]').val(occasionTypeName);
	
	$('div.home-tabs-content').find('.js-home-tabs-img').removeClass('is-active-border');
	$(this).find('.js-home-tabs-img').addClass('is-active-border');
	goodOrBad('01', 'good');
	
});
// 1 SELECT OCCASION TYPE end

// 2 SELECT VENUE LOCATION start

var base_price;
var t;
var map;
var myOptions;
var map_location = 'England';
var marker;
var cityCircle;
var geocoder;
var latitude='';
var longitude='';

/* FUNCTIONS FOR CONTROLLING MAP */
function make_map(when) {
	geocoder = new google.maps.Geocoder();
	if(when == 'button') {
		if (($("input[name='location']").val() != '')) {
			map_location = $("input[name='location']").val() + ', United Kingdom';
			geocoder.geocode( { 'address': map_location }, function(results, status) {
				if ((status == google.maps.GeocoderStatus.OK) && ($("input[name='location']").val().length > 2)) {
					// latitude and longitude to pass to database
					latitude =results[0].geometry.location.lat();
					longitude =results[0].geometry.location.lng();
					myOptions = {
						scrollwheel:false,
						panControl: false,
						zoomControl: true,
						mapTypeControl: false,
						scaleControl: false,
						streetViewControl: false,
						overviewMapControl: false,
						zoom: 4,
						center: results[0].geometry.location,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};
					map = new google.maps.Map(document.getElementById('map-holder'), myOptions);
					
					google.maps.event.addListener(map, 'click', function(event) {
						placeMarker(event.latLng);
						setLocationBox(event.latLng);
					});
					
					
					if($("input[name='location']").val() != '') {
						placeMarker(results[0].geometry.location);
					}

					goodOrBad('02', 'good');
				} else {
					alert('Our map does not recognise the location you have chosen!');
					goodOrBad('01', 'bad');
				}	
			});
		}
	} else {
		map_location = map_location + ', England';
		geocoder.geocode( { 'address': map_location }, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				latitude =results[0].geometry.location.lat();
				longitude =results[0].geometry.location.lng();
				myOptions = {
					scrollwheel:false,
					panControl: false,
					zoomControl: true,
					mapTypeControl: false,
					scaleControl: false,
					streetViewControl: false,
					overviewMapControl: false,
					zoom: 4,
					center: results[0].geometry.location,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(document.getElementById('map-holder'), myOptions);
				
				google.maps.event.addListener(map, 'click', function(event) {
					setLocationBox(event.latLng);
				});
				
				
				$("#map_canvas").css({ 'height' : 250 });
				
				if($("input[name='location']").val() != '' && $("input[name='location']").val() != 'LOCATION OR POSTCODE...') {

					placeMarker(results[0].geometry.location);
				}
			} else {
				alert('Our map does not recognise the location you have chosen!');
				goodOrBad('02', 'bad');
			}	
		});
	}
	//$(".location-list").hide();

	if ((typeof latitude != "undefined" || latitude != null) && (typeof longitude != "undefined" || longitude != null)) {
		$("input[name='latitude_val']").val(latitude);
		$("input[name='longitude_val']").val(longitude);

	}
}

function add_radius() {
	if(cityCircle) {
		cityCircle.setMap(null);
	}

	var radius = $("select[name='radius']").val();
	var mapCircle = {
		strokeColor: "#aa0000",
		strokeOpacity: 0.8,
		strokeWeight: 2,
		fillColor: "#aa0000",
		fillOpacity: 0.35,
		map: map,
		center: marker.position,
		radius: radius*1609
	};
	determine_zoom(radius);
	cityCircle = new google.maps.Circle(mapCircle);
}

function placeMarker(place_location) {

	if(marker) {
		marker.setMap(null);
	}
	marker = new google.maps.Marker({
		position: place_location,
		draggable: true,
		map: map
	});
	google.maps.event.addListener(marker, 'mouseup', function(event) {
		placeMarker(event.latLng);
		setLocationBox(event.latLng);
	}); 
	
	map.setCenter(place_location);

	if($("select[name='radius']").val() != '') {
		add_radius();
	} else {
		map.setZoom(10);
	}
				// Add latitude and longitude values to hidden fields:
				var location_string = place_location.toString();
				var index = location_string.lastIndexOf(')');
				var extract = location_string.substring(1,index);
				var latLong = extract.split(',');

				$(".latitude").val(latLong[0]);
				$(".longitude").val(latLong[1]);
			}

			function setLocationBox(latlng) {

				geocoder.geocode({ 'latLng': latlng }, function(results, status) {
					if(status == google.maps.GeocoderStatus.OK) {
						for(var x=0;x<results[1].address_components.length;x++) {
							if(results[1].address_components[x].types[0] == 'postal_code') {
								var pcode = results[1].address_components[x].long_name	
							}
							if(results[1].address_components[x].types[0] == 'postal_town') {
								var ptown = results[1].address_components[x].long_name	
							}
						}
						if(ptown == 'undefined' || ptown == null || ptown == '') {
							$("input[name='location']").val(pcode);
						} else {
							if(pcode == 'undefined' || pcode == null || pcode == '') {
								$("input[name='location']").val(ptown);
							} else {
								$("input[name='location']").val(ptown+' ('+pcode+')');
							}
						}
					} 
				});
			}

			function determine_zoom(radius) {
				if(radius == 5) { map.setZoom(10); }
				if(radius == 10) { map.setZoom(9); }
				if(radius == 15) { map.setZoom(9); }
				if(radius == 20) { map.setZoom(8); }
				if(radius == 25) { map.setZoom(7); }
				if(radius == 50) { map.setZoom(7); }
			}

function getLatLong() {
	var address_string = $("input[name='post_code']").val() + ', UK';
        // alert(address_string);
        // var address_string = "SO52 9NQ, UK";
    geocoder.geocode({ 'address': address_string }, function(results, status) {
    	if (status == google.maps.GeocoderStatus.OK) {
    		var location = results[0].geometry.location.toString();
    		var index = location.lastIndexOf(')');
    		var extract = location.substring(1,index);
    		var latLong = extract.split(',');

    		$(".latitude_reg").val(latLong[0]);
    		$(".longitude_reg").val(latLong[1]);

    	} else {
    		// alert('Our map does not recognise the location you have chosen!');
    	}
    });
}

/* END MAP CONTROL */

//  Suggestions typeahead.js start
var substringMatcher = function(strs) {
	return function findMatches(q, cb) {
		var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
    	if (substrRegex.test(str)) {
    		matches.push(str);
    	}
    });

    cb(matches);
};
};

var states = ['Aberaeron', 'Aberdare', 'Aberdeen', 'Aberfeldy', 'Abergavenny', 'Abergele', 'Abertillery', 'Aberystwyth', 'Abingdon', 'Accrington', 'Adlington', 'Airdrie', 'Alcester', 'Aldeburgh', 'Aldershot', 'Aldridge', 'Alford', 'Alfreton', 'Alloa', 'Alnwick', 'Alsager', 'Alston', 'Amesbury', 'Amlwch', 'Ammanford', 'Ampthill', 'Andover', 'Annan', 'Antrim', 'Appleby in Westmorland', 'Arbroath', 'Armagh', 'Arundel', 'Ashbourne', 'Ashburton', 'Ashby de la Zouch', 'Ashford', 'Ashington', 'Ashton in Makerfield', 'Atherstone', 'Auchtermuchty', 'Axminster', 'Aylesbury', 'Aylsham', 'Ayr', 'Bacup', 'Bakewell', 'Bala', 'Ballater', 'Ballycastle', 'Ballyclare', 'Ballymena', 'Ballymoney', 'Ballynahinch', 'Banbridge', 'Banbury', 'Banchory', 'Banff', 'Bangor', 'Barmouth', 'Barnard Castle', 'Barnet', 'Barnoldswick', 'Barnsley', 'Barnstaple', 'Barrhead', 'Barrow in Furness', 'Barry', 'Barton upon Humber', 'Basildon', 'Basingstoke', 'Bath', 'Bathgate', 'Batley', 'Battle', 'Bawtry', 'Beaconsfield', 'Bearsden', 'Beaumaris', 'Bebington', 'Beccles', 'Bedale', 'Bedford', 'Bedlington', 'Bedworth', 'Beeston', 'Bellshill', 'Belper', 'Berkhamsted', 'Berwick upon Tweed', 'Betws y Coed', 'Beverley', 'Bewdley', 'Bexhill on Sea', 'Bicester', 'Biddulph', 'Bideford', 'Biggar', 'Biggleswade', 'Billericay', 'Bilston', 'Bingham', 'Birkenhead', 'Birmingham', 'Bishop Auckland', 'Blackburn', 'Blackheath', 'Blackpool', 'Blaenau Ffestiniog', 'Blandford Forum', 'Bletchley', 'Bloxwich', 'Blyth', 'Bodmin', 'Bognor Regis', 'Bollington', 'Bolsover', 'Bolton', 'Bootle', 'Borehamwood', 'Boston', 'Bourne', 'Bournemouth', 'Brackley', 'Bracknell', 'Bradford', 'Bradford on Avon', 'Brading', 'Bradley Stoke', 'Bradninch', 'Braintree', 'Brechin', 'Brecon', 'Brentwood', 'Bridge of Allan', 'Bridgend', 'Bridgnorth', 'Bridgwater', 'Bridlington', 'Bridport', 'Brigg', 'Brighouse', 'Brightlingsea', 'Brighton', 'Bristol', 'Brixham', 'Broadstairs', 'Bromsgrove', 'Bromyard', 'Brynmawr', 'Buckfastleigh', 'Buckie', 'Buckingham', 'Buckley', 'Bude', 'Budleigh Salterton', 'Builth Wells', 'Bungay', 'Buntingford', 'Burford', 'Burgess Hill', 'Burnham on Crouch', 'Burnham on Sea', 'Burnley', 'Burntisland', 'Burntwood', 'Burry Port', 'Burton Latimer', 'Bury', 'Bushmills', 'Buxton', 'Caernarfon', 'Caerphilly', 'Caistor', 'Caldicot', 'Callander', 'Calne', 'Camberley', 'Camborne', 'Cambridge', 'Camelford', 'Campbeltown', 'Cannock', 'Canterbury', 'Cardiff', 'Cardigan', 'Carlisle', 'Carluke', 'Carmarthen', 'Carnforth', 'Carnoustie', 'Carrickfergus', 'Carterton', 'Castle Douglas', 'Castlederg', 'Castleford', 'Castlewellan', 'Chard', 'Charlbury', 'Chatham', 'Chatteris', 'Chelmsford', 'Cheltenham', 'Chepstow', 'Chesham', 'Cheshunt', 'Chester', 'Chester le Street', 'Chesterfield', 'Chichester', 'Chippenham', 'Chipping Campden', 'Chipping Norton', 'Chipping Sodbury', 'Chorley', 'Christchurch', 'Church Stretton', 'Cinderford', 'Cirencester', 'Clacton on Sea', 'Cleckheaton', 'Cleethorpes', 'Clevedon', 'Clitheroe', 'Clogher', 'Clydebank', 'Coalisland', 'Coalville', 'Coatbridge', 'Cockermouth', 'Coggeshall', 'Colchester', 'Coldstream', 'Coleraine', 'Coleshill', 'Colne', 'Colwyn Bay', 'Comber', 'Congleton', 'Conwy', 'Cookstown', 'Corbridge', 'Corby', 'Coventry', 'Cowbridge', 'Cowdenbeath', 'Cowes', 'Craigavon', 'Cramlington', 'Crawley', 'Crayford', 'Crediton', 'Crewe', 'Crewkerne', 'Criccieth', 'Crickhowell', 'Crieff', 'Cromarty', 'Cromer', 'Crowborough', 'Crowthorne', 'Crumlin', 'Cuckfield', 'Cullen', 'Cullompton', 'Cumbernauld', 'Cupar', 'Cwmbran', 'Dalbeattie', 'Dalkeith', 'Darlington', 'Dartford', 'Dartmouth', 'Darwen', 'Daventry', 'Dawlish', 'Deal', 'Denbigh', 'Denton', 'Derby', 'Dereham', 'Devizes', 'Dewsbury', 'Didcot', 'Dingwall', 'Dinnington', 'Diss', 'Dolgellau', 'Donaghadee', 'Doncaster', 'Dorchester', 'Dorking', 'Dornoch', 'Dover', 'Downham Market', 'Downpatrick', 'Driffield', 'Dronfield', 'Droylsden', 'Dudley', 'Dufftown', 'Dukinfield', 'Dumbarton', 'Dumfries', 'Dunbar', 'Dunblane', 'Dundee', 'Dunfermline', 'Dungannon', 'Dunoon', 'Duns', 'Dunstable', 'Durham', 'Dursley', 'Easingwold', 'East Grinstead', 'East Kilbride', 'Eastbourne', 'Eastleigh', 'Eastwood', 'Ebbw Vale', 'Edenbridge', 'Edinburgh', 'Egham', 'Elgin', 'Ellesmere', 'Ellesmere Port', 'Ely', 'Enniskillen', 'Epping', 'Epsom', 'Erith', 'Esher', 'Evesham', 'Exeter', 'Exmouth', 'Eye', 'Eyemouth', 'Failsworth', 'Fairford', 'Fakenham', 'Falkirk', 'Falkland', 'Falmouth', 'Fareham', 'Faringdon', 'Farnborough', 'Farnham', 'Farnworth', 'Faversham', 'Felixstowe', 'Ferndown', 'Filey', 'Fintona', 'Fishguard', 'Fivemiletown', 'Fleet', 'Fleetwood', 'Flint', 'Flitwick', 'Folkestone', 'Fordingbridge', 'Forfar', 'Forres', 'Fort William', 'Fowey', 'Framlingham', 'Fraserburgh', 'Frodsham', 'Frome', 'Gainsborough', 'Galashiels', 'Gateshead', 'Gillingham', 'Glasgow', 'Glastonbury', 'Glossop', 'Gloucester', 'Godalming', 'Godmanchester', 'Goole', 'Gorseinon', 'Gosport', 'Gourock', 'Grange over Sands', 'Grangemouth', 'Grantham', 'Grantown on Spey', 'Gravesend', 'Grays', 'Great Yarmouth', 'Greenock', 'Grimsby', 'Guildford', 'Haddington', 'Hadleigh', 'Hailsham', 'Halesowen', 'Halesworth', 'Halifax', 'Halstead', 'Haltwhistle', 'Hamilton', 'Harlow', 'Harpenden', 'Harrogate', 'Hartlepool', 'Harwich', 'Haslemere', 'Hastings', 'Hatfield', 'Havant', 'Haverfordwest', 'Haverhill', 'Hawarden', 'Hawick', 'Hay on Wye', 'Hayle', 'Haywards Heath', 'Heanor', 'Heathfield', 'Hebden Bridge', 'Helensburgh', 'Helston', 'Hemel Hempstead', 'Henley on Thames', 'Hereford', 'Herne Bay', 'Hertford', 'Hessle', 'Heswall', 'Hexham', 'High Wycombe', 'Higham Ferrers', 'Highworth', 'Hinckley', 'Hitchin', 'Hoddesdon', 'Holmfirth', 'Holsworthy', 'Holyhead', 'Holywell', 'Honiton', 'Horley', 'Horncastle', 'Hornsea', 'Horsham', 'Horwich', 'Houghton le Spring', 'Hove', 'Howden', 'Hoylake', 'Hucknall', 'Huddersfield', 'Hungerford', 'Hunstanton', 'Huntingdon', 'Huntly', 'Hyde', 'Hythe', 'Ilford', 'Ilfracombe', 'Ilkeston', 'Ilkley', 'Ilminster', 'Innerleithen', 'Inveraray', 'Inverkeithing', 'Inverness', 'Inverurie', 'Ipswich', 'Irthlingborough', 'Irvine', 'Ivybridge', 'Jarrow', 'Jedburgh', 'Johnstone', 'Keighley', 'Keith', 'Kelso', 'Kempston', 'Kendal', 'Kenilworth', 'Kesgrave', 'Keswick', 'Kettering', 'Keynsham', 'Kidderminster', 'Kilbarchan', 'Kilkeel', 'Killyleagh', 'Kilmarnock', 'Kilwinning', 'Kinghorn', 'Kingsbridge', 'Kington', 'Kingussie', 'Kinross', 'Kintore', 'Kirkby', 'Kirkby Lonsdale', 'Kirkcaldy', 'Kirkcudbright', 'Kirkham', 'Kirkwall', 'Kirriemuir', 'Knaresborough', 'Knighton', 'Knutsford', 'Ladybank', 'Lampeter', 'Lanark', 'Lancaster', 'Langholm', 'Largs', 'Larne', 'Laugharne', 'Launceston', 'Laurencekirk', 'Leamington Spa', 'Leatherhead', 'Ledbury', 'Leeds', 'Leek', 'Leicester', 'Leighton Buzzard', 'Leiston', 'Leominster', 'Lerwick', 'Letchworth', 'Leven', 'Lewes', 'Leyland', 'Lichfield', 'Limavady', 'Lincoln', 'Linlithgow', 'Lisburn', 'Liskeard', 'Lisnaskea', 'Littlehampton', 'Liverpool', 'Llandeilo', 'Llandovery', 'Llandrindod Wells', 'Llandudno', 'Llanelli', 'Llanfyllin', 'Llangollen', 'Llanidloes', 'Llanrwst', 'Llantrisant', 'Llantwit Major', 'Llanwrtyd Wells', 'Loanhead', 'Lochgilphead', 'Lockerbie', 'London', 'Londonderry', 'Long Eaton', 'Longridge', 'Looe', 'Lossiemouth', 'Lostwithiel', 'Loughborough', 'Loughton', 'Louth', 'Lowestoft', 'Ludlow', 'Lurgan', 'Luton', 'Lutterworth', 'Lydd', 'Lydney', 'Lyme Regis', 'Lymington', 'Lynton', 'Mablethorpe', 'Macclesfield', 'Machynlleth', 'Maesteg', 'Magherafelt', 'Maidenhead', 'Maidstone', 'Maldon', 'Malmesbury', 'Malton', 'Malvern', 'Manchester', 'Manningtree', 'Mansfield', 'March', 'Margate', 'Market Deeping', 'Market Drayton', 'Market Harborough', 'Market Rasen', 'Market Weighton', 'Markethill', 'Markinch', 'Marlborough', 'Marlow', 'Maryport', 'Matlock', 'Maybole', 'Melksham', 'Melrose', 'Melton Mowbray', 'Merthyr Tydfil', 'Mexborough', 'Middleham', 'Middlesbrough', 'Middlewich', 'Midhurst', 'Midsomer Norton', 'Milford Haven', 'Milngavie', 'Milton Keynes', 'Minehead', 'Moffat', 'Mold', 'Monifieth', 'Monmouth', 'Montgomery', 'Montrose', 'Morecambe', 'Moreton in Marsh', 'Moretonhampstead', 'Morley', 'Morpeth', 'Motherwell', 'Musselburgh', 'Nailsea', 'Nailsworth', 'Nairn', 'Nantwich', 'Narberth', 'Neath', 'Needham Market', 'Neston', 'New Mills', 'New Milton', 'Newbury', 'Newcastle', 'Newcastle Emlyn', 'Newcastle upon Tyne', 'Newent', 'Newhaven', 'Newmarket', 'Newport', 'Newport Pagnell', 'Newport on Tay', 'Newquay', 'Newry', 'Newton Abbot', 'Newton Aycliffe', 'Newton Stewart', 'Newton le Willows', 'Newtown', 'Newtownabbey', 'Newtownards', 'Normanton', 'North Berwick', 'North Walsham', 'Northallerton', 'Northampton', 'Northwich', 'Norwich', 'Nottingham', 'Nuneaton', 'Oakham', 'Oban', 'Okehampton', 'Oldbury', 'Oldham', 'Oldmeldrum', 'Olney', 'Omagh', 'Ormskirk', 'Orpington', 'Ossett', 'Oswestry', 'Otley', 'Oundle', 'Oxford', 'Padstow', 'Paignton', 'Painswick', 'Paisley', 'Peebles', 'Pembroke', 'Penarth', 'Penicuik', 'Penistone', 'Penmaenmawr', 'Penrith', 'Penryn', 'Penzance', 'Pershore', 'Perth', 'Peterborough', 'Peterhead', 'Peterlee', 'Petersfield', 'Petworth', 'Pickering', 'Pitlochry', 'Pittenweem', 'Plymouth', 'Pocklington', 'Polegate', 'Pontefract', 'Pontypridd', 'Poole', 'Port Talbot', 'Portadown', 'Portaferry', 'Porth', 'Porthcawl', 'Porthmadog', 'Portishead', 'Portrush', 'Portsmouth', 'Portstewart', 'Potters Bar', 'Potton', 'Poulton le Fylde', 'Prescot', 'Prestatyn', 'Presteigne', 'Preston', 'Prestwick', 'Princes Risborough', 'Prudhoe', 'Pudsey', 'Pwllheli', 'Ramsgate', 'Randalstown', 'Rayleigh', 'Reading', 'Redcar', 'Redditch', 'Redhill', 'Redruth', 'Reigate', 'Retford', 'Rhayader', 'Rhuddlan', 'Rhyl', 'Richmond', 'Rickmansworth', 'Ringwood', 'Ripley', 'Ripon', 'Rochdale', 'Rochester', 'Rochford', 'Romford', 'Romsey', 'Ross on Wye', 'Rostrevor', 'Rothbury', 'Rotherham', 'Rothesay', 'Rowley Regis', 'Royston', 'Rugby', 'Rugeley', 'Runcorn', 'Rushden', 'Rutherglen', 'Ruthin', 'Ryde', 'Rye', 'Saffron Walden', 'Saintfield', 'Salcombe', 'Sale', 'Salford', 'Salisbury', 'Saltash', 'Saltcoats', 'Sandbach', 'Sandhurst', 'Sandown', 'Sandwich', 'Sandy', 'Sawbridgeworth', 'Saxmundham', 'Scarborough', 'Scunthorpe', 'Seaford', 'Seaton', 'Sedgefield', 'Selby', 'Selkirk', 'Selsey', 'Settle', 'Sevenoaks', 'Shaftesbury', 'Shanklin', 'Sheerness', 'Sheffield', 'Shepshed', 'Shepton Mallet', 'Sherborne', 'Sheringham', 'Shildon', 'Shipston on Stour', 'Shoreham by Sea', 'Shrewsbury', 'Sidmouth', 'Sittingbourne', 'Skegness', 'Skelmersdale', 'Skipton', 'Sleaford', 'Slough', 'Smethwick', 'Soham', 'Solihull', 'Somerton', 'South Molton', 'South Shields', 'South Woodham Ferrers', 'Southam', 'Southampton', 'Southborough', 'Southend on Sea', 'Southport', 'Southsea', 'Southwell', 'Southwold', 'Spalding', 'Spennymoor', 'Spilsby', 'Stafford', 'Staines', 'Stamford', 'Stanley', 'Staveley', 'Stevenage', 'Stirling', 'Stockport', 'Stockton on Tees', 'Stoke on Trent', 'Stone', 'Stowmarket', 'Strabane', 'Stranraer', 'Stratford upon Avon', 'Strood', 'Stroud', 'Sudbury', 'Sunderland', 'Sutton Coldfield', 'Sutton in Ashfield', 'Swadlincote', 'Swanage', 'Swanley', 'Swansea', 'Swindon', 'Tadcaster', 'Tadley', 'Tain', 'Talgarth', 'Tamworth', 'Taunton', 'Tavistock', 'Teignmouth', 'Telford', 'Tenby', 'Tenterden', 'Tetbury', 'Tewkesbury', 'Thame', 'Thatcham', 'Thaxted', 'Thetford', 'Thirsk', 'Thornbury', 'Thrapston', 'Thurso', 'Tilbury', 'Tillicoultry', 'Tipton', 'Tiverton', 'Tobermory', 'Todmorden', 'Tonbridge', 'Torpoint', 'Torquay', 'Totnes', 'Totton', 'Towcester', 'Tredegar', 'Tregaron', 'Tring', 'Troon', 'Trowbridge', 'Truro', 'Tunbridge Wells', 'Tywyn', 'Uckfield', 'Ulverston', 'Uppingham', 'Usk', 'Uttoxeter', 'Ventnor', 'Verwood', 'Wadebridge', 'Wadhurst', 'Wakefield', 'Wallasey', 'Wallingford', 'Walsall', 'Waltham Abbey', 'Waltham Cross', 'Walton on Thames', 'Walton on the Naze', 'Wantage', 'Ware', 'Wareham', 'Warminster', 'Warrenpoint', 'Warrington', 'Warwick', 'Washington', 'Watford', 'Wednesbury', 'Wednesfield', 'Wellingborough', 'Wellington', 'Wells', 'Wells next the Sea', 'Welshpool', 'Welwyn Garden City', 'Wem', 'Wendover', 'West Bromwich', 'Westbury', 'Westerham', 'Westhoughton', 'Weston super Mare', 'Wetherby', 'Weybridge', 'Weymouth', 'Whaley Bridge', 'Whitby', 'Whitchurch', 'Whitehaven', 'Whitley Bay', 'Whitnash', 'Whitstable', 'Whitworth', 'Wick', 'Wickford', 'Widnes', 'Wigan', 'Wigston', 'Wigtown', 'Willenhall', 'Wincanton', 'Winchester', 'Windermere', 'Winsford', 'Winslow', 'Wisbech', 'Witham', 'Withernsea', 'Witney', 'Woburn', 'Woking', 'Wokingham', 'Wolverhampton', 'Wombwell', 'Woodbridge', 'Woodstock', 'Wootton Bassett', 'Worcester', 'Workington', 'Worksop', 'Worthing', 'Wotton under Edge', 'Wrexham', 'Wymondham', 'Yarm', 'Yarmouth', 'Yate', 'Yateley', 'Yeadon', 'Yeovil', 'York'];

$('.js-loc-entry').typeahead({
	hint: true,
	highlight: true,
	minLength: 1
},
{
	name: 'states',
	source: substringMatcher(states)
});

//  Suggestions typeahead.js end

$(document).on('click', '.js-map-button', function(){
	make_map('button');
});
$(document).on('change', '.js-change-radius', function(){
	make_map('button');
});
$('.js-loc-entry').blur(function() {
	setTimeout(function() { make_map('button') },1000);
});
// 2 SELECT VENUE LOCATION end 





// 3 SELECT REQUIRED DATES start 
// Genereate Add and Remove day
$(document).ready(function() {
	datesBlockRowUpdate();
});
// Show days after choosing format
function showDateType(element) {
	$('.dates-block .date_required').attr('disabled','disabled');
	$('.dates-block .date_required').removeClass('date_required').addClass('date_required_hidden');
	$('.dates-block').hide();
	switch($(element).val()) {
		case '1':
		$('.single-day-block').show();
		$('.single-day-block .date_required_hidden').removeClass('date_required_hidden').addClass('date_required');
		$('.single-day-block .date_required').removeAttr('disabled');
		break;
		case '2':
		$('.consecutive-day-block').show();
		$('.consecutive-day-block .date_required_hidden').removeClass('date_required_hidden').addClass('date_required');
		$('.consecutive-day-block .date_required').removeAttr('disabled');
		$('.consecutive-day-block .time-wrapper.disabled .date_required').removeClass('date_required').addClass('date_required_hidden');
		$('.consecutive-day-block .time-wrapper.disabled .date_required').attr('disabled','disabled');
		break;
		case '3':
		$('.un-consecutive-day-block').show();
		$('.un-consecutive-day-block .date_required_hidden').removeClass('date_required_hidden').addClass('date_required');
		$('.un-consecutive-day-block .date_required').removeAttr('disabled');
		datesBlockRowUpdate();
		break;
	}
}
$(document).on('click', '.js-show-calendar', function(e){
	var elId = e.target.id.replace('date', '');
	showCalendar(elId);
});
$(document).on('change', '.js-check-calendar', function(e){
	var elIdOnChange = e.target.id.replace(/date|start_time|end_time/g, '');
	console.log('elIdOnChange ' , elIdOnChange);
	checkCalendarTimeVal(elIdOnChange);
});

function showCalendar(id) {
	var startDate = new Date();
	$("#date"+id).datepicker( { minDate: startDate, dateFormat: 'dd/mm/yy' } );
	$("#date"+id).show();
	$("#date"+id).datepicker('show');
}

function checkCalendarTimeVal(idloc){
	id = parseInt(idloc);
	// get field parametrs
	var calDate = $("#date"+id).val();
	var startDate = $("#start_time"+id).val();
	var endDate = $("#end_time"+id).val();
	// convert date parametrs into unixstamp
	var calDateArray = calDate.split('/');
	var unixTimeStampStart = '';
	if(startDate != ''){
		unixTimeStampStart = Date.parse(calDateArray[1]+'/'+calDateArray[0]+'/'+calDateArray[2]+' '+startDate+' GMT')/1000;
	}
	var unixTimeStampEnd='';
	if(endDate != ''){
		unixTimeStampEnd = Date.parse(calDateArray[1]+'/'+calDateArray[0]+'/'+calDateArray[2]+' '+endDate+' GMT')/1000;
	}
	var unixTimeStampDay = Date.parse(calDateArray[1]+'/'+calDateArray[0]+'/'+calDateArray[2]+' GMT')/1000;

	// add unixstamps into hidden fields
	if(id==2){
		$("#timestampstart"+id).val(0);
	}else{
		$("#timestampstart"+id).val(unixTimeStampStart);
	}
	if(id==1){
		$("#timestampend"+id).val(0);
	}else{
		$("#timestampend"+id).val(unixTimeStampEnd);
	}
	$("#timestampday"+id).val(unixTimeStampDay);
	dateValidation(id);
}

function dateValidation(id){
	
	
	// Quantity of rows in MULTI-DAY (UNCONSECUTIVE)
	var numItems = $('.un-consecutive-day-block .dates-block-row').length;

	if($.isNumeric($("#timestampstart"+id).val()) && $.isNumeric($("#timestampend"+id).val()) && $.isNumeric($("#timestampday"+id).val())){

		if(id===0){
			if($("#timestampstart"+id).val() < $("#timestampend"+id).val()){
				goodOrBad('03', 'good');
				return true;
			}else{
				goodOrBad('03', 'bad');
			}
		}

		if(id===1 || id===2){
			var timestampHidenField1 = $("#timestampday1").val();
			var timestampHidenField2 = $("#timestampday2").val();
			if( $.isNumeric(timestampHidenField1) && $.isNumeric(timestampHidenField2) ){

				if((timestampHidenField2 > timestampHidenField1)){
					goodOrBad('03', 'good');
					return true;
				}else{
					goodOrBad('03', 'bad');
				}
			}else{
				goodOrBad('03', 'bad');
			}
		}

		if(id>=3){
			if (id===3) {
				if( $.isNumeric($("#timestampday"+id).val()) && $.isNumeric($("#timestampday"+(id+1)).val()) ){
					if($("#timestampday"+id).val() < $("#timestampday"+(id+1)).val()){
						if ($("#timestampstart"+id).val() < $("#timestampend"+id).val()) {
							goodOrBad('03', 'good');
							return true;
						}else{
							goodOrBad('03', 'bad');
						}
					}else{
					}
				}else{
				}
			}
			if (id>3 && id<3+numItems-1) {
				if( $.isNumeric($("#timestampday"+(id-1)).val()) && $.isNumeric($("#timestampday"+id).val()) && $.isNumeric($("#timestampday"+id+1).val()) ){
					if($("#timestampday"+(id-1)).val() < $("#timestampday"+id).val() < $("#timestampday"+(id+1)).val()){
						if ($("#timestampstart"+id).val() < $("#timestampend"+id).val()) {
							goodOrBad('03', 'good');
							return true;
						}else{
							goodOrBad('03', 'bad');
						}
					}else{
					}
				}else{
				}
			}
			if (id===3+numItems-1) {
				if( $.isNumeric($("#timestampday"+id).val()) && $.isNumeric($("#timestampday"+(id-1)).val()) ){
					if($("#timestampday"+id).val() > $("#timestampday"+(id-1)).val()){
						if (($("#timestampstart"+id).val() < $("#timestampend"+id).val()) 
							&& $.isNumeric($("#timestampstart"+(id-1)).val()) 
							&& $.isNumeric($("#timestampend"+(id-1)).val())
							) {
							goodOrBad('03', 'good');
						return true;
					}else{
						goodOrBad('03', 'bad');
					}
				}else{
					goodOrBad('03', 'bad');
				}
			}else{
			}
		}
	}

}else{
	goodOrBad('03', 'bad');
	return false;
}

}


// MULTI-DAY (UNCONSECUTIVE) add and remove day - start
function addNewUnconsecutiveDate() {
	var bottomLinks = $('.modify-day-link').clone();
	$('.modify-day-link').remove();
	var elementKey = $('.un-consecutive-day-block .dates-block-row').length+1;
	var newElement = $('.un-consecutive-day-block .dates-block-row.first').clone();
	newElement.removeClass('first');
	newElement.find('.date-row .step-input-label').html('DAY '+elementKey);
	newElement.find('.date-row .step-input-label').html('DAY '+elementKey);
	newElement.find('#date3').remove();
	// if don't delete a clonned element then ui shows a mistake.
	newElement.find('#timestampstart3').after('<input type="text" id="date3" name="date[3]" class="dima js-show-calendar js-check-calendar date_time enquiry-input-field date-box date_required_hidden clear-val text" value="CHOOSE A DATE..." style="">');
	newElement.find('#timestampstart3').attr({id:'timestampstart'+(elementKey+2)});
	newElement.find('#timestampend3').attr('id','timestampend'+(elementKey+2));
	newElement.find('#timestampday3').attr('id','timestampday'+(elementKey+2));
	newElement.find('#date3').attr({
		id: 'date'+(elementKey+2),
		name:'date['+(elementKey+2)+']',
	});
	newElement.find('#start_time3').attr({
		id:'start_time'+(elementKey+2),
		name:'time_start['+(elementKey+2)+']',
	});
	newElement.find('#end_time3').attr({
		id:'end_time'+(elementKey+2),
		name:'end_start['+(elementKey+2)+']',
	});

	$('.un-consecutive-day-block').append(newElement).append(bottomLinks);
	datesBlockRowUpdate();
	goodOrBad('03', 'bad');
}
function removeNewUnconsecutiveDate() {
	if($('.un-consecutive-day-block .dates-block-row').length>1) {
		var elemID = $('.un-consecutive-day-block .dates-block-row').last().find('.step-input-label').next().attr('id').replace('timestampstart', '');
		$('.un-consecutive-day-block .dates-block-row').last().remove();
		datesBlockRowUpdate();
		dateValidation(elemID-1);
	}
}

// Check if not less the one day and hide remove button
function datesBlockRowUpdate() {
	if($('.un-consecutive-day-block .dates-block-row').length>2) {
		$('.modify-day-link.remove').show();
	}
	else {
		$('.modify-day-link.remove').hide();
	}
}
// MULTI-DAY (UNCONSECUTIVE) add and remove day - end


// 3 SELECT REQUIRED DATES end 


// 5 VENUE OPTIONS start
$(document).on('click', '.js-venue-options-input', function(){
	$(this).val('');
});

$(document).on('blur', '.js-venue-options-input', function(){
	venueOptionsValidation();
});
$(document).on('change', '.js-venue-options-select', function(){
	venueOptionsValidation();
});
function venueOptionsValidation(){
	var boolVar = false;
	if($('.js-venue-options-input').val() > 0 ){
		$('.js-venue-options-select').each(function(){
			if(parseInt($(this).val())===0 || parseInt($(this).val())===1){
				goodOrBad('05', 'good');
				boolVar = true;
			}else{
				goodOrBad('05', 'bad');
				boolVar = false;
			}
		});
	}else{
		goodOrBad('05', 'bad');
		boolVar = false;
	}
	return boolVar;
}

$(document).on('click', '.js-enquiry-checkbox', function(){
	tick_box($(this));
	checkOptionsSelection($(this));
});



function tick_box(element) {
	if($(element).hasClass('ticked')) {
		$(element).removeClass('ticked');
		$(element).find('input').prop('checked', false);
		// $(element).removeAttr('style');
	} else {
		$(element).addClass('ticked');
		$(element).find('input').prop('checked', true);
		// $(element).css('background-image','url(/images/icon-tick.jpg)');
	}
}

function checkOptionsSelection(element) {
	var group = '';
	if(element.find('input').hasClass('tv_checkbox')){
		group= $('.tv_checkbox');
	}else if(element.find('input').hasClass('food_checkbox')){
		group= $('.food_checkbox');
	}else if(element.find('input').hasClass('drink_checkbox')){
		group= $('.drink_checkbox');
	}
	//if checkbox belongs to a group and says 'None', uncheck all other boxes belonging to that group:
	//var nodeValue = element.parent('div').find('span').text();
	var nodeValue = element.parent('div').find('span').text();

	if(nodeValue=='None') {
		$.each(group, function() {
			var tmpNodeValue = $(this).parent('div').parent('div').find('span').text();
			//make sure 'None' checkbox doesn't get unchecked again:
			if(tmpNodeValue != nodeValue) {
				$(this).attr('checked',false);
				$(this).parent('div').removeClass('ticked');
				// $(this).parent('div').removeAttr('style');
			}
		});
	} else {
		$.each(group, function() {
			var tmpNodeValue = $(this).parent('div').parent('div').find('span').text();
			//make sure only 'None' checkbox gets unchecked:
			if(tmpNodeValue == 'None') {
				$(this).attr('checked',false);
				$(this).parent('div').removeClass('ticked');
				// $(this).parent('div').removeAttr('style');
			}
		});
	}
}
// 5 VENUE OPTIONS end


// 6 BUDGET start
$(document).on('click', '.js-budget-overall', function(){
	$(this).val('');
});
$(document).on('click', '.js-budget-perhead', function(){
	$(this).val('');
});

$(document).on('blur', '.js-budget-overall', function(){
	if($(this).val() >0){
		$('.js-budget-perhead').val('PER HEAD...');
		goodOrBad('06', 'good');
	}else{
		if($('.js-budget-perhead').val()>0){
			$('.js-budget-overall').val('OVERALL...');
			goodOrBad('06', 'good');
		}else{
			$(this).val('OVERALL...');
			goodOrBad('06', 'bad');
			
		}
		
	}
});
$(document).on('blur', '.js-budget-perhead', function(){
	if($(this).val() >0){
		$('.js-budget-overall').val('OVERALL...');
		goodOrBad('06', 'good');
	}else{
		if($('.js-budget-overall').val()>0){
			$('.js-budget-perhead').val('PER HEAD...');
			goodOrBad('06', 'good');
		}else{
			$(this).val('PER HEAD...');
			goodOrBad('06', 'bad');
			
		}
	}
});

// 6 BUDGET end


// 7 PERSONAL DETAILS start
var corectAnsvers=0;
$(document).on('click', '.js-presonal-input-click', function(){
	$(this).val('');
});

$(document).on('blur', '.js-presonal-input-blur', function(){
	personalDetailsValidation();
});
function personalDetailsValidation(){
	if(isEmail($("input[name='email']").val())){
		if($("input[name='password2']").val()!='' && $("input[name='password2']").val()!='' && $("input[name='password']").val() == $("input[name='password2']").val()){
			if($("input[name='firstname']").val()!='' && $("input[name='firstname']").val()!='FIRST NAME...'){
				if($("input[name='lastname']").val()!='' && $("input[name='lastname']").val()!='LAST NAME...'){
					if(isPhone($("input[name='phone']").val())){
						goodOrBad('07', 'good');
						return true;
					}else{
						goodOrBad('07', 'bad');
						return false;
					}
					
				}else{
					goodOrBad('07', 'bad');
					return false;
				}

			}else{
				goodOrBad('07', 'bad');
				return false;
			}
			
		}else{
			goodOrBad('07', 'bad');
			return false;
		}
	}else{
		goodOrBad('07', 'bad');
		return false;
	}
}
function isEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}
function isPhone(phone) {
	var regex = /^(\(?(0|\+44)\d+)$/;
	return regex.test(phone);
}
function isPostcode(postcode) {
	var regex =  /[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z]{2}/gi;
	return regex.test(postcode);
}
// 7 PERSONAL DETAILS end



// Registration form start

$(document).on('click', '.js-reg-form-submit', function(){
	var validVar = false;
	$('#register_form').find('.js-reg-input-req').each(function() {
		if ($( this ).val() == ''){
			swal({   title: "Error!",   text: "Please fill in all required fields (*)",   type: "error",   confirmButtonText: "OK" });
			validVar = false;
		}else if(!isPostcode($('.js-postcode').val())){
			swal({   title: "Error!",   text: "Please enter valid postcode",   type: "error",   confirmButtonText: "OK" });
			validVar = false;
		}else if(!isEmail($('.js-email').val())){
			swal({   title: "Error!",   text: "Please enter valid email",   type: "error",   confirmButtonText: "OK" });
			validVar = false;
		}else if(!isPhone($('.js-phone').val())){
			swal({   title: "Error!",   text: "Please enter valid phone number format 0123456789 or +44123456789",   type: "error",   confirmButtonText: "OK" });
			validVar = false;
		}else if ($('.password1').val() != $('.password2').val()) {
			swal({   title: "Error!",   text: "Passwords don't match",   type: "error",   confirmButtonText: "OK" });
			validVar = false;
		}else{
			validVar = true;
		}
	});

	getLatLong();
	if(validVar){
		$('#register_form').submit();
	}
});

// Registration form end

// Options content Swap based on Venue Pick
$('#example-tabs').on('change.zf.tabs', function() {

  if($('#panel1:visible').length){
    $('#Av_Facilities_Options').show();
    $('#Entertainment_Options').hide();
    $('#Food_Options').hide();
    $('#Drinks_Options').hide();

  }
  if($('#panel2:visible').length){
    $('#Av_Facilities_Options').hide();
    $('#Entertainment_Options').show();
    $('#Food_Options').show();
    $('#Drinks_Options').show();
  }
  if($('#panel3:visible').length){
    $('#Av_Facilities_Options').hide();
    $('#Entertainment_Options').show();
    $('#Food_Options').show();
    $('#Drinks_Options').show();
  }

});