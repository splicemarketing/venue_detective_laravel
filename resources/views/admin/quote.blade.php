@extends('layouts.master')

@section('content')
<div class="row">
	<div class="small-12 column Admin_Panel">
		@include('partials.adminnav')			

		@include('partials.enquiriesSidebar')

		<div class="Panel small-12 medium-8 large-9 columns">
			<div class="Panel_Title Grey_Gradient">
				<h4>Quote</h4>
			</div>
			@if ($quote->awaiting_quote == 1)
			<div class="Panel_Content Quoting_Form">
				{!! Form::open(['url' => '/sendQuote', 'method' => 'post']) !!}
				{{ Form::hidden('quote_id', $quote->id) }}
				<div class="row">
					<div class="small-3 columns">
						{!! Form::label('min_attendance', 'Venue Capacity', ['class' => 'text-right']) !!}
					</div>
					<div class="small-3 columns">
						{!! Form::label('min_attendance', 'Min') !!}
						{!! Form::number('min_attendance') !!}
					</div>
					<div class="small-3 columns">
						{!! Form::label('max_attendance', 'Max') !!}
						{!! Form::number('max_attendance') !!}
					</div>
				</div>

				<div class="row">
					<div class="small-3 columns">
						{!! Form::label('disabled_access', 'Disabled Access Available?', ['class' => 'text-right']) !!}
					</div>
					<div class="small-9 columns">
						{!! Form::select('disabled_access', array('1' => 'Yes', '0' => 'No')) !!}
					</div>
				</div>

				<div class="row">
					<div class="small-3 columns">
						{!! Form::label('included_in_cost', 'Your Quote Includes', ['class' => 'text-right']) !!}
					</div>
					<div class="small-9 columns">
						{!! Form::textarea('included_in_cost') !!}
					</div>
				</div>

				<div class="row">
					<div class="small-3 columns">
						{!! Form::label('extras', 'Extras', ['class' => 'text-right']) !!}
					</div>
					<div class="small-9 columns">
						{!! Form::textarea('extras') !!}
					</div>
				</div>

				<div class="row">
					<div class="small-3 columns">
						{!! Form::label('price_breakdown', 'Quote Breakdown', ['class' => 'text-right']) !!}
					</div>
					<div class="small-9 columns">
						{!! Form::textarea('price_breakdown') !!}
					</div>
				</div>

				<div class="row">
					<div class="small-3 columns">
						{!! Form::label('plug', 'Why Choose your Venue?', ['class' => 'text-right']) !!}
					</div>
					<div class="small-9 columns">
						{!! Form::textarea('plug') !!}
					</div>
				</div>

				<div class="row">
					<div class="small-3 columns">
						{!! Form::label('notes', 'Notes/Comments', ['class' => 'text-right']) !!}
					</div>
					<div class="small-9 columns">
						{!! Form::textarea('notes') !!}
					</div>
				</div>
	
				<div class="row">
					<div class="small-3 columns">
						{!! Form::label('total_cost', 'Total Quote', ['class' => 'text-right']) !!}
					</div>
					<div class="small-9 columns">
						{!! Form::text('total_cost') !!}
					</div>
				</div>


				{!! Form::submit('Send Quote') !!}
				{!! Form::close() !!}				
			</div>
			@endif
			@if ($quote->quoted == 1)
			<div class="Panel_Content Quoting_Form">
				<div class="row">
					<div class="small-3 columns">
						<div class="callout primary No_Border">
							<p>Disabled Access Available?</p>
						</div>
					</div>
					<div class="small-9 columns">
						<div class="callout secondary No_Border">
							<p>{{ $quote->disabled_access }}</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="small-3 columns">
						<div class="callout primary No_Border">
							<p>Your quote includes</p>
						</div>
					</div>
					<div class="small-9 columns">
						<div class="callout secondary No_Border">
							<p>{!! $quote->included_in_cost !!}</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="small-3 columns">
						<div class="callout primary No_Border">
							<p>Extras</p>
						</div>
					</div>
					<div class="small-9 columns">
						<div class="callout secondary No_Border">
							<p>{!! $quote->extras !!}</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="small-3 columns">
						<div class="callout primary No_Border">
							<p>Quote Breakdown</p>
						</div>
					</div>
					<div class="small-9 columns">
						<div class="callout secondary No_Border">
							<p>{!! $quote->price_breakdown !!}</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="small-3 columns">
						<div class="callout primary No_Border">
							<p>Why Choose your Venue?</p>
						</div>
					</div>
					<div class="small-9 columns">
						<div class="callout secondary No_Border">
							<p>{!! $quote->plug !!}</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="small-3 columns">
						<div class="callout primary No_Border">
							<p>Notes / Comments</p>
						</div>
					</div>
					<div class="small-9 columns">
						<div class="callout secondary No_Border">
							<p>{!! $quote->notes !!}</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="small-3 columns">
						<div class="callout primary No_Border">
							<p>Total Quote</p>
						</div>
					</div>
					<div class="small-9 columns">
						<div class="callout secondary No_Border">
							<p>£{!! $quote->total_cost !!}</p>
						</div>
					</div>
				</div>

			</div>
			@endif
				
		</div>

	</div>
</div>
@endsection

@section('scripts')
<script>
	tinymce.init({ selector:'textarea' });
</script>

@endsection