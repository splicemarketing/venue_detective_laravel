@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="small-12 column Admin_Panel" id="Admin_Panel">
			@include('partials.adminnav')

			<div class="Panel small-12 medium-12 large-12 columns">
				@foreach($venues as $venue)
					<div class="Panel_Title Grey_Gradient">					
						<h4>{{ $venue->venue_name }}</h4>
					</div>
					<div class="Panel_Content">
						
					{!! Form::open(['url' => "/my_venues/update/$venue->id", 'method' => 'post']) !!}
					<div class="small-12 medium-8 large-6 columns">

					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('venue_name', 'Venue Name', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('venue_name', $venue->venue_name) !!}
						</div>
					</div>
					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('address_one', 'Address One', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('address_one', $venue->address_one) !!}
						</div>
					</div>
					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('address_two', 'Address Two', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('address_two', $venue->address_two) !!}
						</div>
					</div>
					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('city', 'City', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('city', $venue->city) !!}
						</div>
					</div>
					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('county', 'County', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('county', $venue->county) !!}
						</div>
					</div>
					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('post_code', 'Post Code', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('post_code', $venue->post_code) !!}
						</div>
					</div>
					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('phone', 'Phone', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::number('phone', $venue->phone) !!}
						</div>
					</div>	
					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('website', 'Website', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('website', $venue->website) !!}
						</div>
					</div>
					</div>
					<div class="small-12 medium-8 large-6 columns">
						<div class="row">
						<fieldset class="small-12 columns">
						    <legend>Which events will your venue cater for?</legend>
						    <legend>The leads you receive will be based on your choices</legend>
						    <legend>Business</legend>
						    <input id="will_receive_agm" name="will_receive_agm" type="checkbox" @if ($venue->will_receive_agm) checked  @endif><label for="will_receive_agm">AGM</label>
						    <input id="will_receive_breakfast_meeting" name="will_receive_breakfast_meeting" type="checkbox" @if ($venue->will_receive_breakfast_meeting) checked  @endif><label for="will_receive_breakfast_meeting">Breakfast Meeeting</label>
						    <input id="will_receive_conference" name="will_receive_conference" type="checkbox" @if ($venue->will_receive_conference) checked  @endif><label for="will_receive_conference">Conference</label>
						</fieldset>
						<fieldset class="small-12 columns">
						    <input id="will_receive_christmas_party" name="will_receive_christmas_party" type="checkbox" @if ($venue->will_receive_christmas_party) checked  @endif><label for="will_receive_christmas_party">Christmas Party</label>
						    <input id="will_receive_corporate_hospitality" name="will_receive_corporate_hospitality" type="checkbox" @if ($venue->will_receive_corporate_hospitality) checked  @endif><label for="will_receive_corporate_hospitality">Corporate Hospitality</label>
						    <input id="will_receive_interviews" name="will_receive_interviews" type="checkbox" @if ($venue->will_receive_interviews) checked  @endif><label for="will_receive_interviews">Interviews</label>
						</fieldset>
						<fieldset class="small-12 columns">
						    <input id="will_receive_meeting" name="will_receive_meeting" type="checkbox" @if ($venue->will_receive_meeting) checked  @endif><label for="will_receive_meeting">Meeting</label>
						    <input id="will_receive_product_launch" name="will_receive_product_launch" type="checkbox" @if ($venue->will_receive_product_launch) checked  @endif><label for="will_receive_product_launch">Product Launch</label>
						    <input id="will_receive_seminar" name="will_receive_seminar" type="checkbox" @if ($venue->will_receive_seminar) checked  @endif><label for="will_receive_seminar">Seminar</label>
						</fieldset>
						<fieldset class="small-12 columns">
						    <input id="will_receive_sit_down_dinner" name="will_receive_sit_down_dinner" type="checkbox" @if ($venue->will_receive_sit_down_dinner) checked  @endif><label for="will_receive_sit_down_dinner">Sit Down Dinner</label>
						    <input id="will_receive_stand_up_reception" name="will_receive_stand_up_reception" type="checkbox" @if ($venue->will_receive_stand_up_reception) checked  @endif><label for="will_receive_stand_up_reception">Stand Up</label>
						    <input id="will_receive_team_meeting" name="will_receive_team_meeting" type="checkbox" @if ($venue->will_receive_team_meeting) checked  @endif><label for="will_receive_team_meeting">Team Meeting</label>
						</fieldset>
						<fieldset class="small-12 columns">
						<input id="will_receive_training_course" name="will_receive_training_course" type="checkbox" @if ($venue->will_receive_training_course) checked  @endif><label for="will_receive_training_course">Training Course</label>					    
						</fieldset>
						<fieldset class="small-12 columns">
						<legend>Party</legend>
							<input id="will_receive_anniversary" name="will_receive_anniversary" type="checkbox" @if ($venue->will_receive_anniversary) checked  @endif><label for="will_receive_anniversary">Anniversary</label>
						    <input id="will_receive_barmitzvah" name="will_receive_barmitzvah" type="checkbox" @if ($venue->will_receive_barmitzvah) checked  @endif><label for="will_receive_barmitzvah">Bar/Bat Mitzvah</label>
						    <input id="will_receive_retirement" name="will_receive_retirement" type="checkbox" @if ($venue->will_receive_retirement) checked  @endif><label for="will_receive_retirement">Retirement</label>
						</fieldset>
						<fieldset class="small-12 columns">
						    <input id="will_receive_baptism" name="will_receive_baptism" type="checkbox" @if ($venue->will_receive_baptism) checked  @endif><label for="will_receive_baptism">Baptism</label>
						    <input id="will_receive_birthday" name="will_receive_birthday" type="checkbox" @if ($venue->will_receive_birthday) checked  @endif><label for="will_receive_birthday">Birthday</label>
						    <input id="will_receive_christmas" name="will_receive_christmas" type="checkbox" @if ($venue->will_receive_christmas) checked  @endif><label for="will_receive_christmas">Christmas</label>
						</fieldset>
						<fieldset class="small-12 columns">
						    <input id="will_receive_engagement" name="will_receive_engagement" type="checkbox" @if ($venue->will_receive_engagement) checked  @endif><label for="will_receive_engagement">Engagement</label>
						    <input id="will_receive_hen_stag" name="will_receive_hen_stag" type="checkbox" @if ($venue->will_receive_hen_stag) checked  @endif><label for="will_receive_hen_stag">Hen / Stag</label>
						</fieldset>
						<fieldset class="small-12 columns">
							<legend>Wedding</legend>
						    <input id="will_receive_civil_ceremony" name="will_receive_civil_ceremony" type="checkbox" @if ($venue->will_receive_civil_ceremony) checked  @endif><label for="will_receive_civil_ceremony">Civil Ceremony</label>
						    <input id="will_receive_ethnic" name="will_receive_ethnic" type="checkbox" @if ($venue->will_receive_ethnic) checked  @endif><label for="will_receive_ethnic">Ethnic</label>
						    <input id="will_receive_reception_only" name="will_receive_reception_only" type="checkbox" @if ($venue->will_receive_reception_only) checked  @endif><label for="will_receive_reception_only">Reception Only</label>
						</fieldset>
						<fieldset class="small-12 columns">
						    <input id="will_receive_reception_ceremony" name="will_receive_reception_ceremony" type="checkbox" @if ($venue->will_receive_reception_ceremony) checked  @endif><label for="will_receive_reception_ceremony">Reception &amp; Ceremony</label>
						</fieldset>
						</div>
						<div class="Login_Panel">
							<button class="btn Login_Submit" type="submit" value="Login">Update</button>
						</div>
					</div>					
									

					{!! Form::close() !!}
						
					</div>
				@endforeach
			</div>	
		</div>
	</div>


@endsection
