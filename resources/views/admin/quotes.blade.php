@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="small-12 column Admin_Panel">
			@include('partials.adminnav')			

			@include('partials.enquiriesSidebar')

			<div class="Panel small-12 medium-8 large-9 columns">
				<div class="Panel_Title Grey_Gradient">
					<span><a href="#"></a></span>
					<h4>Quotes</h4>
				</div>
				<div class="Panel_Content">
					<table>
						<thead>
							<tr>
								<th>Quote Ref</th>
								<th>Lead Ref</th>
								<th>Event Type</th>
								<th>Date of Event</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($quotes as $quote)
							<tr>
								<td>{{ $quote->id }}</td>
								<td>{{ $quote->lead_id }}</td>
								<td>{{ $quote->lead->eventType->name }}</td>
								<td>{{ date('F d, Y', strtoTime($quote->lead->dates->first()->date)) }}</td>
								<td></td>
								<td><a href="/quote/{{ $quote->id }}">Details</a></td>
							</tr>						
						@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>

@endsection