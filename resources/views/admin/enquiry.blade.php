@extends('layouts.master')

@section('content')
<div class="row">
	<div class="small-12 column Admin_Panel">
		@include('partials.adminnav')

		@include('partials.enquiriesSidebar')

		<div class="Panel small-12 medium-8 large-9 columns">
			<div class="Panel_Title Grey_Gradient">
				<h4>Enquiry Details - Reference {{ $enquiry->id }}</h4>
			</div>
			<div class="Panel_Content">
			
				
				<div class="callout Enquiry_Box warning small-6">
				<h5>Enquiry Summary</h5>
				<p>Reference Number: {{ $enquiry->id }}</p>
				<p>Event Type: {{ $enquiry->eventType->name }}</p>
				<p>Event Date: {{ date('F d, Y', strtoTime($enquiry->dates->first()->date)) }}</p>
				<p>From {{ date('g:i a', strtoTime($enquiry->dates->first()->start)) }} til {{ date('g:i a', strtoTime($enquiry->dates->first()->finish)) }} </p>
				<p>Received: {{ date('F d, Y', strtotime($enquiry->created_at)) }} at {{ date('g:i a', strtotime($enquiry->created_at)) }}</p>
				</div>
				<div class="callout Enquiry_Box primary small-6">
				  <h5>Buying Details</h5>
				  <p>Buy this lead now for £{!! $enquiry->lead_cash_price !!}</p>
				   	{{ Form::open(['url' => '#', 'method' =>'post', 'class' => 'swa-confirm']) }}
					{{ Form::hidden('lead_id', $enquiry->id) }}
					{{ Form::hidden('venue_id', $venue_id) }}
					{{ Form::submit("Purchase with Cash") }}
					{{ Form::close() }}
					<br>
				  <p>Use {!! $enquiry->lead_credit_price !!} subscription credits to purchase this lead</p>
				  {{ Form::open(['url' => 'purchase_quote', 'method' =>'post', 'class' => 'swa-confirm']) }}
				  {{ Form::hidden('lead_id', $enquiry->id) }}
				  {{ Form::hidden('venue_id', $venue_id) }}
				  {{ Form::submit('Purchase with Credits') }}
				  {{ Form::close() }}				 
				</div>
			
				
				<table class="Enquiry_Detail">
					<tbody>
						<tr>
							<td>Event Type</td>
							<td>{{ $enquiry->eventType->name }}</td>
						</tr>
						<tr>
							<td>Location</td>
							<td>Within {{ $enquiry->radius }} miles of {{ $enquiry->location }}</td>
						</tr>
						<tr>
							<td>Event Date</td>
							<td>{{ date('F d, Y', strtoTime($enquiry->dates->first()->date)) }}</td>
						</tr>
						<tr>
							<td>Number of People</td>
							<td>{{ $enquiry->number_of_people }}</td>
						</tr>
						<tr>
							<td>Accommodation Required</td>
							<td>{{ $enquiry->accomodation_required }}</td>
						</tr>
						<tr>
							<td>Disabled Access Required</td>
							<td>{{ $enquiry->disabled_access_required }}</td>
						</tr>
						<tr>
							<td>Food</td>
							<td>{{ $enquiry->is_food_not_required }}</td>
						</tr>
						<tr>
							<td>Drinks</td>
							<td>{{ $enquiry->is_drinks_not_required }}</td>
						</tr>
						<tr>
							<td>Wireless Internet Required</td>
							<td>{{ $enquiry->wireless_internet_required }}</td>
						</tr>
						<tr>
							<td>Av Facilities Required</td>
							<td>{{ $enquiry->av_facilities_required }}</td>
						</tr>
						<tr>
							<td>Budget</td>
							<td>£ {{ $enquiry->budget }}</td>
						</tr>
					</tbody>
				</table>
				<table class="Enquiry_Detail">
					<thead>
						<tr>
							<td>Venue Types</td>
							<td>Accepted</td>
						</tr>
					</thead>
					<tbody>						
						<tr>
							<td>Pubs and Restaurants</td>
							<td>{{ $enquiry->venue_type_pubs_restaurants }}</td>
						</tr>
						<tr>
							<td>Hotels</td>
							<td>{{ $enquiry->venue_type_hotels }}</td>
						</tr>
						<tr>
							<td>Unusual (Musuems, Zoos)</td>
							<td>{{ $enquiry->venue_type_unusual }}</td>
						</tr>
						<tr>
							<td>Church</td>
							<td>{{ $enquiry->venue_type_church }}</td>
						</tr>
						<tr>
							<td>Town Hall</td>
							<td>{{ $enquiry->venue_type_town_hall }}</td>
						</tr>
						<tr>
							<td>Garden</td>
							<td>{{ $enquiry->venue_type_garden }}</td>
						</tr>
						<tr>
							<td>Stately Homes</td>
							<td>{{ $enquiry->venue_type_stately_home }}</td>
						</tr>
						<tr>
							<td>Sporting &amp; Leisure</td>
							<td>{{ $enquiry->venue_type_sporting_leisure }}</td>
						</tr>
						<tr>
							<td>Dedicated</td>
							<td>{{ $enquiry->venue_type_dedicated }}</td>
						</tr>
						<tr>
							<td>Barn</td>
							<td>{{ $enquiry->venue_type_barn }}</td>
						</tr>
						<tr>
							<td>Marquee</td>
							<td>{{ $enquiry->venue_type_marquee }}</td>
						</tr>
						<tr>
							<td>Historic / Period</td>
							<td>{{ $enquiry->venue_type_historic }}</td>
						</tr>
						<tr>
							<td>Castles</td>
							<td>{{ $enquiry->venue_type_castle }}</td>
						</tr>
						<tr>
							<td>Nightclubs</td>
							<td>{{ $enquiry->venue_type_nightclub }}</td>
						</tr>						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')

<script>
	$(".swa-confirm").on("click", function(e) {
	    e.preventDefault();
	    swal({
	        title: "Are you sure?",
	        text: $(this).data("swa-text"),
	        type: "info",
	        showCancelButton: true,
	        confirmButtonColor: "#cc3f44",
	        confirmButtonText: "Yes! I want it!",
	        closeOnConfirm: true,
	        closeOnCancel: true,
	        html: false
	    }, function(confirmed) {
	        if(confirmed)
	            $(".swa-confirm").submit();
	    });
	});
</script>

@endsection