@extends('layouts.master')


@section('content')
	<div class="row">
		<div class="small-12 column Admin_Panel">
			@include('partials.adminnav')
			
			<div class="Panel small-12 medium-6 large-6 columns">
				<div class="Panel_Title Grey_Gradient">
					<span><a href="/my_leads">View All Enquiries</a></span>
					<h4>Enquiries Summary</h4>
				</div>
				<div class="Panel_Content">
					<table>
					  <tbody>
					    <tr>
					      <td>New Venue Detective Leads</td>
					      <td>{{ $summary['total'] }}</td>
					    </tr>
					    <tr>
					    	<td>Leads For You</td>
					    	<td>{{ $summary['leadsforuser'] }}</td>
					    </tr>
					    <tr>
					      <td>Leads Awaiting Quote</td>
					      <td>{{ $summary['leadstoquote'] }}</td>					      
					    </tr>
					    <tr>
					      <td>Quoted</td>
					      <td>{{ $summary['leadsquoted'] }}</td>					     
					    </tr>
					  </tbody>
					</table>
				</div>
			</div>

			<div class="Panel small-12 medium-6 large-6 columns">
				<div class="Panel_Title Grey_Gradient">
					<span><a href="/venue_reports">View Full Reporting</a></span>
					<h4>Report</h4>
				</div>
				<div class="Panel_Content">
					<canvas id="myChart" width="350" height="200"></canvas>
				</div>
			</div>

			<div class="Panel small-12 medium-6 large-6 columns">
				<div class="Panel_Title Grey_Gradient">
					<span><a href="/my_accounts">View Package</a></span>
					<h4>Credits / Subscriptions</h4>
				</div>
				<div class="Panel_Content">
					<div class="callout">
						<p>- You currently have 5 credits for leads under £5000</p>
						<p>- You currently have 2 credits for leads over £5000</p>
						<p>- Your next subscription is due on 1st August 2016</p>
					</div>				
				</div>
			</div>
			<div class="Panel small-12 medium-6 large-6 columns">
				<div class="Panel_Title Grey_Gradient">
					<span><a href="/my_leads">View All Leads</a></span>
					<h4>New Comments</h4>
				</div>
				<div class="Panel_Content">
					<table>
					  <tbody>
					    <tr>
					      <td>Ref: 145</td>
					      <td>R. Smith: Can you confirm the band is able to play Abba songs?</td>
					    </tr>
					    <tr>
					      <td>Ref. 126</td>
					      <td>J. Pike: We had a lovely time, thank you very much to all of your staff.</td>					      
					    </tr>
					  </tbody>
					</table>
				</div>
			</div>

		</div>
	</div>

@endsection

@section('scripts')
<script type="text/javascript">
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["July", "August", "September", "October", "November", "December"],
        datasets: [{
            label: '£ Value of Leads',
            data: [7000, 19000, 8000, 5000, 7000, 23000],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
@endsection



