@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="small-12 column Admin_Panel">
			@include('partials.adminnav')			

			@include('partials.enquiriesSidebar')

			<div class="Panel small-12 medium-8 large-9 columns">
				<div class="Panel_Title Grey_Gradient">
					<!-- <span><a href="#">Pagination</a></span> -->
					<h4>Enquiries</h4>
				</div>
				<div class="Panel_Content">
					<table>
						<thead>
							<tr>
								<th>Ref</th>
								<th>Type</th>
								<th>Date of Event</th>
								<th>Venue</th>
								<th>Budget</th>
								<th>Received</th>
								<th>Available</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($enquiries as $enquiry)
							<tr>
								<td>{{ $enquiry->id }}</td>
								<td>{{ $enquiry->eventType->name }}</td>
								<td>{{ date('F d, Y', strtoTime($enquiry->dates->first()->date)) }}</td>								
								<td>{{ $enquiry->venueInRange }}</td>
								<td>£{{ $enquiry->budget }}</td>
								<td>{{ date('F d, Y', strtotime($enquiry->created_at)) }}</td>
								@if ($enquiry->number_of_quotes == 3)
								<td><i class="fa fa-check-circle-o fa-2x green" aria-hidden="true"></i><i class="fa fa-check-circle-o fa-2x green" aria-hidden="true"></i><i class="fa fa-check-circle-o fa-2x green" aria-hidden="true"></i></td>
								@endif
								@if ($enquiry->number_of_quotes == 2)
								<td><i class="fa fa-check-circle-o fa-2x green" aria-hidden="true"></i><i class="fa fa-check-circle-o fa-2x green" aria-hidden="true"></i></td>
								@endif
								@if ($enquiry->number_of_quotes == 1)
								<td><i class="fa fa-check-circle-o fa-2x green" aria-hidden="true"></i></td>
								@endif
								<td><a href="/my_leads/lead/{{ $enquiry->id}}?venue={{$enquiry->venue_id}}">Details</a></td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>

@endsection




