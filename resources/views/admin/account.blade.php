@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="small-12 column Admin_Panel">
			@include('partials.adminnav')
<div class="Subscription_Block">
			<div class="Panel small-12 medium-6 large-6 columns">
				<div class="Panel_Title Grey_Gradient">
					<h4>Manage Subscription</h4>
				</div>
				<div class="Panel_Content">

				<div class="row">
					<div class="Credit_Block small-6 columns">
					  		<div class="Credit_Block_Content">
					  			<p>5 Credits</p>
					  			<p>£15.00 per credit</p>
					  			<form action="" method="POST">
					  			{!! csrf_field() !!}
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key={{ config('stripe.publishable_key') }}
    data-amount="7500"
    data-name="Venue Detective"
    data-description="Credits"
    data-image="/img/documentation/checkout/marketplace.png"
    data-locale="auto"
    data-currency="gbp"
    data-label="Subscribe">
  </script>
</form>
					  		</div>					  		
					  	</div>
					  	<div class="Credit_Block small-6 columns">
					  		<div class="Credit_Block_Content">
					  			<p>10 Credits</p>
					  			<p>£15 per credit</p>
					  			<form action="" method="POST">
					  			{!! csrf_field() !!}
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key={{ config('stripe.publishable_key') }}
    data-amount="15000"
    data-name="Venue Detective"
    data-description="Credits"
    data-image="/img/documentation/checkout/marketplace.png"
    data-locale="auto"
    data-currency="gbp"
    data-label="Subscribe">
  </script>
</form>
					  		</div>					  		
					  	</div>
				</div>
				
				</div>
			</div>

			
			</div>
		</div>
	</div>

@endsection

@section('scripts')
	<script src="https://js.stripe.com/v2/"></script>
@endsection

