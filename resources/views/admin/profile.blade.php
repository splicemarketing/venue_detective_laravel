@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="small-12 column Admin_Panel">
			@include('partials.adminnav')
			<div class="Subscription_Block">
			<div class="Panel small-12 medium-6 large-6 columns">
				<div class="Panel_Title Grey_Gradient">
					<h4>Edit Profile</h4>
				</div>
				<div class="Panel_Content">
					{!! Form::open(['url' => '/venue_profile/edit', 'method' => 'post']) !!}
					
					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('first_name', 'First Name', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('first_name', $profile->first_name) !!}
						</div>
					</div>

					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('last_name', 'Last Name', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('last_name', $profile->last_name) !!}
						</div>
					</div>

					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('email', 'Email', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::email('email', $profile->email) !!}
						</div>
					</div>

					<div class="row">
						<div class="small-3 columns">
							{!! Form::label('job_title', 'Job Title', ['class' => 'text-right']) !!}
						</div>
						<div class="small-9 columns">
							{!! Form::text('job_title', $profile->job_title) !!}
						</div>
					</div>
						
					<div class="Login_Panel">
							<button class="btn Login_Submit" type="submit" value="Login">Update</button>
						</div>

					{!! Form::close() !!}
				</div>
			</div>

			</div>
			
		</div>
	</div>

@endsection


