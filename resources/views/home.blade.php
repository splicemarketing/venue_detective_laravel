@extends('layouts.master')

@section('content')
<div class="row">
	<div class="small-12 columns mt-24">
		<div class="customer-howto-redesign">
			<div class="header">

				<h1 class="top-block-h1">Your Free Venue Finding Service</h1>

			</div>
			<div class="steps">
				<div class="dotted-border">
					<ol>
						<li>
							<img src="/images/venue-detective-3-step-1-992.jpg" alt="Three steps to find your venue">
							<div class="js-three-pic-text three-pic-text">1. YOU TELL US WHAT YOU NEED</div>
						</li>
						<li>
							<img src="/images/venue-detective-3-step-2-992.jpg" alt="Three steps to find your venue">
							<div class="js-three-pic-text three-pic-text">2. WE MATCH YOUR REQUIREMENTS</div>
						</li>
						<li>
							<img src="/images/venue-detective-3-step-3-992.jpg" alt="Three steps to find your venue">
							<div class="js-three-pic-text three-pic-text">3. YOU RECEIVE QUOTES FROM VENUES</div>
						</li>
					</ol>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="small-12 medium-10 large-7 columns small-centered">
		<p class="home-descr-block text-center">Simply fill out this short form and let us do the rest, absolutely FREE! Just tell us a few details about your event and we'll find you the perfect venue and send it straight to your inbox.</p>
	</div>
</div>
{!! Form::open(['url' => '/enquiry', 'method' => 'post', 'id' => 'home-form']) !!}
	<!-- OCCASION TYPE -->
	<div class="row mb-24">
		<div class="small-12 columns">
			<div class="home-section-header clear">
				<span class="step-number">1</span><span class="step-title">SELECT OCCASION TYPE</span>
				<div id="section-01" class="type-checkresult validation-result"></div>
				<input type="hidden" value="" name="event_type" />
			</div>
			<ul class="tabs home-header-tabs" data-tabs id="example-tabs">
				<?php $i=1; ?>
				@foreach ($events as $event)
				<li class="tabs-title tabs-title-width"><a href="#panel{{$i}}" class="occasion-type{{$i}}" @if($i===1) aria-selected="true" @endif >
					<img src="{{ $event['ev_src'] }}" alt="{{ $event['ev_name'] }}" />
					<div class="ev-name">{{ $event['ev_name'] }}</div>

				</a></li>
				<?php $i++; ?>
				@endforeach
			</ul>
			<?php $j=1; ?>
			<div class="tabs-content home-tabs-content" data-tabs-content="example-tabs">
				@foreach ($events as $event)
				<div class="tabs-panel" id="panel{{$j}}">
					@foreach ($event['ev_subcat'] as $event_subcat)
					<div class="home-tabs-content-block js-occasion-type">

						<img class="home-tabs-img js-home-tabs-img" src="{{ $event_subcat['src'] }}">
						<div id="{{ $event_subcat['id'] }}" class="home-tabs-name js-home-tabs-name">{{ $event_subcat['name'] }}</div>
					</div>
					@endforeach
					<?php $j++; ?>
				</div>
				@endforeach
				<div class="clear"></div>
			</div>
		</div>
	</div>

<!-- VENUE LOCATION -->
<div class="row mb-24">
	<div class="small-12 columns">
		<div class="home-section-header clear">
			<span class="step-number">2</span><span class="step-title">SELECT VENUE LOCATION</span>
			<div id="section-02" class="location-checkresult validation-result"></div>
		</div>
		<div class="step-column block-gradient">
			<div class="venue-block-1 mb-24">
				<input type="hidden" name="latitude_val" value="" />
				<input type="hidden" name="longitude_val" value="" />
				<input type="text" name="location" class="enquiry-input-field clear-val location_required text js-loc-entry" onblur="setTimeout(function() { make_map('button') },1000);" value="" placeholder="LOCATION OR POSTCODE..." autocomplete="off" style="">
				<div class="fs-18">

					Within
					<div class="enquiry-select-field">
						<select name="radius" class="js-change-radius">
							<option value="" selected="selected">WITHIN...</option>
							<option value="5">5</option>
							<option value="10" selected="">10</option>
							<option value="15">15</option>
							<option value="20">20</option>
							<option value="25">25</option>
							<option value="50">50</option>
						</select>
					</div>
					miles
					</div>
					<div class="enquiry-form-button map-button js-map-button"><i class="fa fa-chevron-right" aria-hidden="true"></i><span class="fs-15"> UPDATE MAP</span></div>
				</div>

				<div class="venue-block-2">
					<div class="map-holder" id="map-holder"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>	

	<!-- DATES -->
	<div class="row mb-24">
		<div class="small-12 columns">
			<div class="home-section-header clear">
				<span class="step-number">3</span><span class="step-title">SELECT REQUIRED DATES</span>
				<div id="section-03" class="date-checkresult validation-result"></div>
			</div>

			<div class="date-holder step-column enquiry-stepholder-gradient block-gradient">

				<div class="date-type-row" style="">
					<label class="step-input-label" >FORMAT</label>
					<select name="date-type" class="date-type"  onchange="showDateType(this)" style="background-color: #fff;">
						<option value="" selected="selected">PLEASE SELECT</option>
						<option value="1" >SINGLE DAY</option>
						<option value="2" >MULTI-DAY (CONSECUTIVE)</option>
						<option value="3" >MULTI-DAY (UNCONSECUTIVE)</option>
					</select>
				</div>

				<div class="dates-block-wrapper">
					<div class="single-day-block dates-block">
						<div class="date-row home-date-row" style="">
							<div class="time-wrapper">
								<label class="step-input-label" >EVENT DATE</label>
								<input id="timestampstart0" name="timestampstart0" type="hidden" value="" />
								<input id="timestampend0" name="timestampend0" type="hidden" value="" />
								<input id="timestampday0" name="timestampday0" type="hidden" value="" />
								<input 
								type="text" 
								id="date0" 
								class="js-show-calendar js-check-calendar enquiry-input-field date-box date_required_hidden clear-val text" 
								value="CHOOSE A DATE..." 
								style="">
							</div>
						</div>

						<div class="time-row home-time-row" >
							<div class="time-wrapper">
								<label class="step-input-label">START TIME</label>
								<select id="start_time0" disabled class="js-start-time js-check-calendar date_required_hidden date_time text home-date-time" style="">
									<option value="" selected="selected">START...</option>

									@for ($i = 0; $i < 24; $i++)
									<option value="{{$i}}:00">{{$i}}:00</option>
									@endfor

								</select>
							</div>
						</div>

						<div class="time-row home-time-row" >
							<div class="time-wrapper">
								<label class="step-input-label" >END TIME</label>
								<select id="end_time0" disabled class="js-end-time js-check-calendar date_required_hidden date_time text home-date-time" >
									<option value="" selected="selected">FINISH...</option>
									@for ($i = 1; $i < 24; $i++)
									<option value="{{$i}}:00">{{$i}}:00</option>
									@endfor
									<option value="23.59">23:59</option>
								</select>
							</div>
						</div>
						<div class="clear"></div>
					</div>

					<div class="consecutive-day-block dates-block">
						<div class="dates-block-row">
							<div class="date-row home-date-row" style="">
								<label class="step-input-label" >START DATE</label>
								<input id="timestampstart1" name="timestampstart1" type="hidden" value="" />
								<input id="timestampend1" name="timestampend1" type="hidden" value="" />
								<input id="timestampday1" name="timestampday1" type="hidden" value="" />
								<input 
								type="text" 
								id="date1"
								class="js-show-calendar js-check-calendar enquiry-input-field date-box date_required_hidden clear-val text from-date-input"
								value="CHOOSE A DATE..." 
								style="">
							</div>

							<div class="time-row home-time-row" >
								<div class="time-wrapper">
									<label class="step-input-label" >START TIME</label>
									<select id="start_time1" class="js-check-calendar date_required_hidden date_time text home-date-time" >
										<option value="" selected="selected">START...</option>
										@for ($i = 0; $i < 24; $i++)
										<option value="{{$i}}:00">{{$i}}:00</option>
										@endfor
									</select>
								</div>
							</div>

							<div class="time-row home-time-row" >
								<div class="time-wrapper disabled">
									<label class="step-input-label" >END TIME</label>
									<select id="end_time1" disabled class="js-check-calendar date_required_hidden date_time text home-date-time" >
										<option value="" selected="selected">FINISH...</option>
									</select>
								</div>
							</div>
						</div>

						<div class="dates-block-row">
							<div class="date-row home-date-row" style="">
								<label class="step-input-label" >END DATE</label>
								<input id="timestampstart2" name="timestampstart2" type="hidden" value="" />
								<input id="timestampend2" name="timestampend2" type="hidden" value="" />
								<input id="timestampday2" name="timestampday2" type="hidden" value="" />
								<input 
								type="text" 
								id="date2"								
								class="js-show-calendar js-check-calendar enquiry-input-field date-box date_required_hidden clear-val text for-date-input"
								value="CHOOSE A DATE..." 
								style="">
							</div>

							<div class="time-row home-time-row" >
								<div class="time-wrapper disabled">
									<label class="step-input-label" >START TIME</label>
									<select id="start_time2" disabled class="js-check-calendar date_required_hidden date_time text home-date-time" >
										<option value="" selected="selected">START...</option>

									</select>
								</div>
							</div>

							<div class="time-row home-time-row" >
								<div class="time-wrapper">
									<label class="step-input-label" >END TIME</label>
									<select id="end_time2" disabled class="js-check-calendar date_required_hidden date_time text home-date-time">
										<option value="" selected="selected">FINISH...</option>
										@for ($i = 1; $i < 24; $i++)
										<option value="{{$i}}:00">{{$i}}:00</option>
										@endfor
										<option value="23.59">23:59</option>
									</select>
								</div>
							</div>
						</div>
					</div>

					<div class="un-consecutive-day-block dates-block">
						<div class="dates-block-row clear first">
							<div class="date-row home-date-row" style="">
								<label class="step-input-label" >DAY 1</label>
								<input id="timestampstart3" name="timestampstart3" type="hidden" value="" />
								<input id="timestampend3" name="timestampend3" type="hidden" value="" />
								<input id="timestampday3" name="timestampday3" type="hidden" value="" />
								<input 
								type="text" 
								id="date3"  
								class="js-show-calendar js-check-calendar date_time enquiry-input-field date-box date_required_hidden clear-val text" 
								value="CHOOSE A DATE..." 
								style="">
							</div>

							<div class="time-row home-time-row" >
								<div class="time-wrapper">
									<label class="step-input-label" >START TIME</label>
									<select id="start_time3" disabled class="js-check-calendar date_required_hidden date_time text home-date-time" >
										<option value="" selected="selected">START...</option>
										@for ($i = 0; $i < 24; $i++)
										<option value="{{$i}}:00">{{$i}}:00</option>
										@endfor
									</select>
								</div>
							</div>

							<div class="time-row home-time-row" >
								<div class="time-wrapper">
									<label class="step-input-label" >END TIME</label>
									<select id="end_time3" disabled class="js-check-calendar date_required_hidden date_time text home-date-time" >
										<option value="" selected="selected">FINISH...</option>
										@for ($i = 1; $i < 24; $i++)
										<option value="{{$i}}:00">{{$i}}:00</option>
										@endfor
										<option value="23:59">23:59</option>
									</select>
								</div>
							</div>
						</div>

						<div class="dates-block-row clear">
							<div class="date-row home-date-row" style="">
								<label class="step-input-label" >DAY 2</label>
								<input id="timestampstart4" name="timestampstart4" type="hidden" value="" />
								<input id="timestampend4" name="timestampend4" type="hidden" value="" />
								<input id="timestampday4" name="timestampday4" type="hidden" value="" />
								<input 
								type="text" 
								id="date4"
								class="js-show-calendar js-check-calendar enquiry-input-field date-box date_required_hidden date_time clear-val text"
								value="CHOOSE A DATE..." 
								style="">
							</div>

							<div class="time-row home-time-row" >
								<div class="time-wrapper">
									<label class="step-input-label" >START TIME</label>
									<select id="start_time4" disabled class="js-check-calendar date_required_hidden date_time text home-date-time" >
										<option value="" selected="selected">START...</option>
										@for ($i = 0; $i < 24; $i++)
										<option value="{{$i}}:00">{{$i}}:00</option>
										@endfor
									</select>
								</div>
							</div>

							<div class="time-row home-time-row" >
								<div class="time-wrapper">
									<label class="step-input-label" >END TIME</label>
									<select id="end_time4" disabled class="js-check-calendar date_required_hidden date_time text home-date-time" >
										<option value="" selected="selected">FINISH...</option>
										@for ($i = 1; $i < 24; $i++)
										<option value="{{$i}}:00">{{$i}}:00</option>
										@endfor
										<option value="23.59">23:59</option>
									</select>
								</div>
							</div>
						</div>

					
							<a class="modify-day-link" href="javascript:addNewUnconsecutiveDate()">+Add a Day</a>
							<a class="modify-day-link remove" href="javascript:removeNewUnconsecutiveDate()">-Remove last Day</a>
						<div class="clear"></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>			
		</div>
	</div>

	<!-- DATES -->
	<div class="row mb-24">
		<div class="small-12 columns">
			<div class="home-section-header clear">
				<span class="step-number">4</span><span class="step-title">VENUE TYPES</span>
				<div id="section" class="location-checkresult validation-result"></div>
			</div>
			<div class="venue-options-holder venue-padding enquiry-stepholder-gradient block-gradient clearfix">
				<div>
				<div class="party_venue_types">				
					<div class="step-column-resp float-left">
						<div class="av-facilities">
							<div class="step-column-small-resp float-left">						
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_pubs_restaurants">
									</div>
									<span>Pubs / Restaurants</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_hotels">
									</div>
									<span>Hotels</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_unusual">
									</div>
									<span>Museums / Zoo's</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_church">
									</div>
									<span>Church / Village Hall</span>
								</div>
							</div>
							<div class="step-column-small-resp float-right">
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_town_hall">
									</div>
									<span>Town Hall</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_garden">
									</div>
									<span>Outdoor / Garden</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_stately_home">
									</div>
									<span>Stately Homes</span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="step-column-resp float-right">
						<div class="av-facilities">
							<div class="step-column-small-resp float-left">						
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_sporting_leisure">
									</div>
									<span>Sporting &amp; Leisure</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_dedicated">
									</div>
									<span>Dedicated</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_barn">
									</div>
									<span>Barn</span>
								</div>
							</div>
							<div class="step-column-small-resp float-right">
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_marquee">
									</div>
									<span>Marquee</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_historic">
									</div>
									<span>Historic / Period</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_castle">
									</div>
									<span>Castles</span>
								</div>
							</div>
						</div>
					</div>
					</div>					
					<div class="clearfix"></div>
					<div class="wedding_venue_types" style="display:none;">						
					<div class="step-column-resp float-left">
						<div class="av-facilities">
							<div class="step-column-small-resp float-left">						
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_garden">
									</div>
									<span>Outdoor / Garden</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_sporting_leisure">
									</div>
									<span>Sporting &amp; Leisure</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_pubs_restaurants">
									</div>
									<span>Pubs / Restaurants</span>
								</div>
							</div>
							<div class="step-column-small-resp float-right">
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_hotels">
									</div>
									<span>Hotels</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_barn">
									</div>
									<span>Barn</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_church">
									</div>
									<span>Church / Village Hall</span>
								</div>								
							</div>							
						</div>
					</div>
					<div class="step-column-resp float-right">
						<div class="av-facilities">
							<div class="step-column-small-resp float-left">						
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_marquee">
									</div>
									<span>Marquee</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_unusual">
									</div>
									<span>Museums / Zoo's</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_stately_home">
									</div>
									<span>Stately Homes</span>
								</div>															
							</div>
							<div class="step-column-small-resp float-right">								
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_historic">
									</div>
									<span>Historic / Period</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_nightclub">
									</div>
									<span>Nightclub</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="" class="enquiry-checkbox field_option" name="venue_type_town_hall">
									</div>
									<span>Town Hall</span>
								</div>
							</div>
						</div>
					</div>
					</div>					
				</div>
			</div>
		</div>

	</div>

	<!-- VENUE OPTIONS -->
	<div class="row mb-24">
		<div class="small-12 columns">
			<div class="home-section-header clear">
				<span class="step-number">5</span><span class="step-title">VENUE OPTIONS</span>
				<div id="section-05" class="options-checkresult validation-result"></div>
			</div>

			<div class="venue-options-holder venue-padding enquiry-stepholder-gradient block-gradient">
				<div>
					<div class="step-column-full float-left">
						<input type="text" class="js-venue-options-input enquiry-input-field clear-val options_required number input-field-style" name="number_of_people" value="NUMBER OF ATTENDEES..." style="background: url(/images/icon-person.png) no-repeat; background-position: 19px 7px; background-color: #fff;">
						<div class="enquiry-select-field">
							<select name="accomodation_required" class="js-venue-options-select input-field-style options_required text" style="background: url(/images/icon-accommodation.png) no-repeat; background-position: 14px 12px; background-color: #fff;">
								<option value="" selected="selected">ACCOMMODATION REQUIRED...?</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
						<div class="enquiry-select-field">
							<select name="disabled_access_required" class="js-venue-options-select input-field-style options_required text" style="background: url(/images/icon-disabled.png) no-repeat; background-position: 14px 10px; background-color: #fff;">
								<option value="" selected="selected">DISABLED ACCESS REQUIRED...?</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
						<div class="wireless-internet">
							<div class="enquiry-select-field">
								<select name="wireless_internet_required" class="js-venue-options-select input-field-style field_option" style="background: url(/images/icon-wireless.png) no-repeat; background-position: 14px 10px; background-color: #fff;">
									<option value="" selected="selected">WIRELESS INTERNET REQUIRED...?</option>
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
							</div>
						</div>
						<div class="childrens-facilities">
							<div class="enquiry-select-field">
								<select class="js-venue-options-select input-field-style" name="kids_facilities_requried" style="background: url(/images/icon-childrens-facilities.png) no-repeat; background-position: 14px 10px; background-color: #fff;">
									<option value="" selected="selected">CHILDREN&rsquo;S FACILITIES REQUIRED...?</option>
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
							</div>
						</div>
					</div>
					<div id="Av_Facilities_Options" class="step-column-full float-left">
						<div class="av-facilities">
							<div class="column-heading">
								<img src="/images/icon-av-facilities.png" alt="Icon">
								<span>AV FACILITIES REQUIRED?</span>
							</div>
							<div class="options-list">
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox ticked">
										<input type="checkbox" id="71" name="av_facilities_not_required" class="enquiry-checkbox tv_checkbox field_option" checked>
									</div>
									<span>None</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="73" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>Lecturn</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="75" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_projector_screen">
									</div>
									<span>Projector/Screen</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="77" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_microphone">
									</div>
									<span>Microphone</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="72" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_autocue">
									</div>
									<span>Autocue</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="74" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_pa_system">
									</div>
									<span>PA System</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="76" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_video_recordingav_facility_video_recording">
									</div>
									<span>Video Recording</span>
								</div>
							</div>
						</div>
					</div>
					<div id="Entertainment_Options" class="step-column-full float-left">
						<div>
							<div class="column-heading">
								<img src="/images/icon-av-facilities.png" alt="Icon">
								<span>ENTERTAINMENT OPTIONS</span>
							</div>
							<div class="options-list">
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox ticked">
										<input type="checkbox" id="77" name="av_facilities_not_required" class="enquiry-checkbox tv_checkbox field_option" checked>
									</div>
									<span>None</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>In-House Toastmaster</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>In-House DJ</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>Disco Allowed</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>Dance Floor Provided</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>Stand-Up</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>Reception</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>Themed Evening</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>In-House Magician</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>Live Music Permitted</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>External Entertainment Allowed</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="78" class="enquiry-checkbox tv_checkbox field_option" name="av_facility_lecturn">
									</div>
									<span>Additional Entertainment Options</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div>
					<div id="Food_Options" class="step-column-full float-left">
						<div class="column-heading">
							<img src="/images/icon-food.png" alt="Icon">
							<span>FOOD REQUIRED?</span>
						</div>
						<div class="options_required food">
							<div class="options-list">
								<div class="clear"></div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox ticked">
										<input type="checkbox" id="56" name="is_food_not_required" class="enquiry-checkbox food_checkbox field_option" checked>
									</div>
									<span>None</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="58" class="enquiry-checkbox food_checkbox field_option" name="sit_down_meal">
									</div>
									<span>Sit-down Meal</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="60" class="enquiry-checkbox food_checkbox field_option" name="external_catering">
									</div>
									<span>External Catering</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="60" class="enquiry-checkbox food_checkbox field_option" name="external_catering">
									</div>
									<span>In-house Catering</span>
								</div>						
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="57" class="enquiry-checkbox food_checkbox field_option" name="finger_buffet">
									</div>
									<span>Finger Buffet</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="59" class="enquiry-checkbox food_checkbox field_option" name="canapes">
									</div>
									<span>Canapes</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="59" class="enquiry-checkbox food_checkbox field_option" name="canapes">
									</div>
									<span>BBQ/Hog Roast</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="60" class="enquiry-checkbox food_checkbox field_option" name="external_catering">
									</div>
									<span>Kitchen Hire</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="60" class="enquiry-checkbox food_checkbox field_option" name="external_catering">
									</div>
									<span>Additional Food Requirements</span>
								</div>	
							</div>							
						</div>
					</div>

					<div id="Drinks_Options" class="step-column-full float-left">
						<div class="column-heading clear" style="margin-top: -8px;">
							<img src="/images/icon-drink.png" alt="Icon">
							<span>DRINKS REQUIRED?</span>
						</div>
						<div class="options_required drinks">
							<div class="options-list">
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox ticked">
										<input type="checkbox" id="61" class="enquiry-checkbox drink_checkbox field_option"  name="drinks_not_required" checked>
									</div>
									<span>None</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="63" class="enquiry-checkbox drink_checkbox field_option"  name="soft_drinks_only">
									</div>
									<span>Soft Drinks Only</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">
										<input type="checkbox" id="62" class="enquiry-checkbox drink_checkbox field_option" name="tea_coffee">
									</div>
									<span>Tea/Coffee</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="87" class="enquiry-checkbox drink_checkbox field_option" name="paid_bar">
									</div>
									<span>Paid bar</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="87" class="enquiry-checkbox drink_checkbox field_option" name="paid_bar">
									</div>
									<span>Open bar</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="87" class="enquiry-checkbox drink_checkbox field_option" name="paid_bar">
									</div>
									<span>Bring Your Own</span>
								</div>
								<div class="enquiry-checkbox-field">
									<div class="enquiry-checkbox-div js-enquiry-checkbox">						
										<input type="checkbox" id="87" class="enquiry-checkbox drink_checkbox field_option" name="paid_bar">
									</div>
									<span>Additional Drink Requirements</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>


	<!-- BUDGET -->
	<div class="row mb-24">
		<div class="small-12 columns">
			<div class="home-section-header clear">
				<span class="step-number">6</span><span class="step-title">BUDGET</span>
				<div id="section-06" class="budget-checkresult validation-result"></div>
			</div>

			<div class="personal-details enquiry-stepholder-gradient block-gradient clearfix">
				<div class="center-wrapper" style="display:none">
					<div class="budget-info-box">Your budget is set too low and unlikely to encourage venues to send you quotes. Either increase your budget or change your requirements.</div>
					<div class="arrow-center-wrapper">
						<div class="arrow"></div>
						<div class="arrow border"></div>
					</div>
				</div>
				<div class="step-column">
					<div class="float-left budget-overall">
						<div class="overall-style">
							<label class="budget-label">Total:</label>
							<input type="text" class="js-budget-overall enquiry-input-field clear-val budget_required input-field-style text" name="budget" value="OVERALL..." style="background: url(/images/icon-pound.png) no-repeat; background-position: 14px 7px; background-color: #fff;"
							>
						</div>
					</div>
					<div class="float-right budget-perhead" >
						<div class="gudget-or-div">OR</div>
						<div class="perhead-style">
							<label class="budget-label">Per Head:</label>
							<input type="text" class="js-budget-perhead enquiry-input-field clear-val text js-value-per-head input-field-style" value="PER HEAD..." style="background: url(/images/icon-pound.png) no-repeat; background-position: 14px 7px; background-color: #fff;">
						</div>
					</div>
				</div>

				@if (Sentinel::check())
				<div class="clear" style="text-align:center;">
					<div class="enquiry-form-button send-button js-send-button-1 clear">
						<i class="fa fa-chevron-right" aria-hidden="true"></i><span class="fs-22"> FIND ME A VENUE</span>
					</div>
				</div>
				@endif

			</div>
		</div>
	</div>
</div>

@if (Sentinel::guest())
<!-- USER DETAILS -->
<div class="row mb-24">
	<div class="small-12 columns">

		<div class="home-section-header clear">
			<span class="step-number">7</span><span class="step-title">PERSONAL DETAILS</span>
			<div id="section-07" class="details-checkresult validation-result"></div>
		</div>
		<div class="step-column personal-details enquiry-stepholder-gradient block-gradient pb-24" >
			<div class="personal-details-left float-left">
				<input 
				type="text" 
				class="js-presonal-input-click js-presonal-input-blur enquiry-input-field clear-val details_required email input-field-style" 
				name="email" 
				value="EMAIL ADDRESS..." 
				style="background: url(/images/icon-email.png) no-repeat; background-position: 19px 14px; background-color: #fff;"
				>

				<!-- ENTER PASSWORD -->
				<input 
				type="password" 
				class="enquiry-input-field js-presonal-input-blur clear-val details_required input-field-style password1" 
				name="password" 
				placeholder="ENTER PASSWORD..."
				value="" 
				style="background: url(/images/icon-password.png) no-repeat; background-position: 19px 8px; background-color: #fff;"
				>

				<!-- REPEAT PASSWORD -->

				<input 
				type="password" 
				class="enquiry-input-field js-presonal-input-blur clear-val details_required input-field-style password2" 
				name="password2"
				placeholder="CONFIRM PASSWORD..." 
				value="" 
				style="background: url(/images/icon-password.png) no-repeat; background-position: 19px 8px; background-color: #fff;"
				>
			</div>
			<div class="personal-details-right float-right">
				<input type="text" class="js-presonal-input-click js-presonal-input-blur input-field-style enquiry-input-field clear-val details_required text" name="firstname" value="FIRST NAME..." style="background: url(/images/icon-person.png) no-repeat; background-position: 19px 7px; background-color: #fff;">
				<input type="text" class="js-presonal-input-click js-presonal-input-blur input-field-style enquiry-input-field clear-val details_required text" name="lastname" value="LAST NAME..." style="background: url(/images/icon-person.png) no-repeat; background-position: 19px 7px; background-color: #fff;">
				<input type="text" class="js-presonal-input-click js-presonal-input-blur input-field-style enquiry-input-field clear-val details_required text" name="phone" value="PHONE NUMBER..." style="background: url(/images/icon-phone.png) no-repeat; background-position: 19px 8px; background-color: #fff;">
			</div>
				<!-- FORGOT PASSWORD -->
				<p style="display: block;  margin: 0" class="forgot-password clear">
					<a style="color: #CF2828;" href="/forgot-password.php">Forgot Password?</a>
				</p>
			<div class="text-center clear">
				<p>
					I am happy to receive leads from
					<input type="checkbox" name="leads_agencies" value="agencies" checked="checked">
					Agencies and
					<input type="checkbox" name="leads_venues" value="venues" checked="checked"> Venues directly.
				</p>
				<div class="enquiry-form-button send-button js-send-button-2 clear" >
					<i class="fa fa-chevron-right" aria-hidden="true"></i><span class="fs-22"> FIND ME A VENUE</span>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
</form>

<div class="row mb-24">
	<div class="small-12 columns">
		<div class="home-page-description-wrap">
			<div class="home-page-description">
				<p>Locate the ideal venue for your business event, private party or wedding and save yourself time by using Venue Detective, a Free Venue Finding Service for anyone looking to arrange an event ranging from business conferences over several days, to a business meeting or a private party.</p>
				<p>Simply complete the enquiry form with details about your event and Venue Detective will find you the perfect venue. Your requirements will be matched to appropriate venues, saving you petrol, time and phonecalls in your quest for the perfect venue.</p>
				<h2 class="home-page-description-h2">Finding the Ideal Venue for Events is Easy Using Venue Detective</h2>
				<p>Simply complete the enquiry form and be matched with venues, who will then contact you to discuss the details of your event and provide you with a quotation. No more than three venues will contact you when you use this service, so you will not be left with numerous phone calls interrupting your day and event planning.</p>
				<p>The Venue Detective service is completely free to all customers; we are paid by the venues so that we can provide this service to you. We are also completely independent with no ties or loyalties to ANY venues or chains, so we will offer you the best service. We look forward to receiving your Venue Search Request form.</p>
			</div>
		</div>
	</div>
</div>

@endsection
