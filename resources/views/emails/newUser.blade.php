<div style="font: Verdana, Arial, Helvetica, sans-serif; margin: 0;padding: 16px;color: #363636; background-color:#e1e1e1;">

<div style="margin: 0 auto;  width:600px;">
    <img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/header.gif" border="0" usemap="#Map" />
    
  <div style="margin:0;padding: 16px;border-left: 1px solid #d7d7d7;border-right: 1px solid #d7d7d7; background-color:#ffffff;">
    
   <h2>Dear User</h2>

    <h3>Thanks for signing up to Venue Detective! We're delighted you've chosen this service and happy to say you're one step away from the best venue for your event.</h3>

    <p>Please confirm your email address by clicking on the link below. Once complete we'll match your requirements to venues that will suit you.</p>


    <a href="">Click here to confirm your email address</a>

    <h2>How Venue Detective Works</h2>

<p>While you focus on more important things, Venue Detective saves you time by sending quotes from venues that best match your budget and requirements.</p>
  <p>We're here to do all the hard work for you and although most of us won't be wearing long coats, deerstalker hats or using magnifying glasses, you won't find the same service anywhere else.</p>
  <p>We hope you enjoy the results Venue Detective delivers and look forward to helping you find the best venue time after time.</p>

<h3>Help Others Find the Best Venue</h3>

<p>If you like our service we'd be delighted if you could share the expereince on social media with your friends and followers. You can find us below on...</p>
    
    
    </div> 
      
  <div style="background-color:#bf2822;padding: 8px 16px;">
    <table>
      <tr>
      <td><a href="https://www.facebook.com/VenueDetective" target="_blank"><img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/ico-fb.gif" alt="facebook" /></a></td>
      <td><a href="https://twitter.com/venuedetective" target="_blank"><img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/ico-twitter.gif" alt="twitter" /></a></td>
      <td><a href="http://pinterest.com/venuedetective/" target="_blank"><img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/ico-pinterest.gif" alt="pintrest" /></a></td>
      <td><a href="https://plus.google.com/103513433799591656664/posts" target="_blank"><img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/ico-google-plus.gif" alt="googleplus" /></a></td>
      </tr>
    </table>
  </div>

</div>

</div>