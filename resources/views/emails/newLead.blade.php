<div style="font: Verdana, Arial, Helvetica, sans-serif; margin: 0;padding: 16px;color: #363636; background-color:#e1e1e1;">

<div style="margin: 0 auto;  width:600px;">
    <img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/header.gif" border="0" usemap="#Map" />
    
  <div style="margin:0;padding: 16px;border-left: 1px solid #d7d7d7;border-right: 1px solid #d7d7d7; background-color:#ffffff;">
    
    <h2>Dear User</h2>
    <p>Thank you for your venue search request.</p>
    <ul>
        <li>Shortly we will review your request and may call you to confirm the finer points of your venue search.</li>
        <li>We will match your requirements to suitable venues.</li>
        <li>You will receive proposed solutions for your venue requirements.</li>
    </ul>
    
    
    </div> 
      
  <div style="background-color:#bf2822;padding: 8px 16px;">
    <table>
      <tr>
      <td><a href="https://www.facebook.com/VenueDetective" target="_blank"><img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/ico-fb.gif" alt="facebook" /></a></td>
      <td><a href="https://twitter.com/venuedetective" target="_blank"><img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/ico-twitter.gif" alt="twitter" /></a></td>
      <td><a href="http://pinterest.com/venuedetective/" target="_blank"><img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/ico-pinterest.gif" alt="pintrest" /></a></td>
      <td><a href="https://plus.google.com/103513433799591656664/posts" target="_blank"><img src="http://ec2-52-50-253-44.eu-west-1.compute.amazonaws.com/blog/images/ico-google-plus.gif" alt="googleplus" /></a></td>
      </tr>
    </table>
  </div>

</div>

</div>