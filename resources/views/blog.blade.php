@extends('layouts.master')

@section('content')
<div class="row mt-24 mb-24">
	<div class="small-12 columns">
		<div class="constrainer">
			<div class="static-new-header">
				<h1>{{ $post->title }}</h1>
			</div>
			<div class="block-gradient clearfix">
				<div class="large-8 columns">

					{{ $post->content }}

					
				</div>				
			</div>

		</div>
	</div>
</div>

@endsection