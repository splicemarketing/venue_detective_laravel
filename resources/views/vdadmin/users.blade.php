@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="small-12 column Admin_Panel">
			@include('partials.vdadminnav')
			
			<div class="Panel small-12 medium-6 large-6 columns">
				<div class="Panel_Title Grey_Gradient">
					<span><a href="#">View Reports</a></span>
					<h4>Enquiries Summary</h4>
				</div>
				<div class="Panel_Content">
					<table>
					  <thead>
					    <tr>
					      <th>Type</th>
					      <th>Number</th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>New Enquiries</td>
					      <td>0</td>
					    </tr>
					    <tr>
					      <td>Quotes Sent</td>
					      <td>0</td>					      
					    </tr>
					   </tbody>
					</table>
				</div>
			</div>

			<div class="Panel small-12 medium-6 large-6 columns">
				<div class="Panel_Title Grey_Gradient">
					<h4>Credits / Subscriptions</h4>
				</div>
				<div class="Panel_Content">
				<table>
					  <thead>
					    <tr>
					      <th>Type</th>
					      <th>Number</th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>20 Credits</td>
					      <td>0</td>
					    </tr>
					    <tr>
					      <td>40 Credits</td>
					      <td>0</td>					      
					    </tr>
					    <tr>
					      <td>60 Credits</td>
					      <td>0</td>					      
					    </tr>
					    <tr>
					      <td>100 Credits</td>
					      <td>0</td>					      
					    </tr>
					   </tbody>
					</table>

				</div>
			</div>

		</div>
	</div>

@endsection
