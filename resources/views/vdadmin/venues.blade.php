@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="small-12 column Admin_Panel">
			@include('partials.vdadminnav')
			
			<div class="Panel small-12 medium-4 large-4 columns">
				<div class="Panel_Title Grey_Gradient">
					<h4>Summary</h4>
				</div>
				<div class="Panel_Content">
					<table>
					  <tbody>
					    <tr>
					      <td>Total Venues</td>
					      <td>0</td>
					    </tr>
					    <tr>
					      <td>New Venues Month</td>
					      <td>0</td>					      
					    </tr>
					    <tr>
					      <td>New Venues Week</td>
					      <td>0</td>					      
					    </tr>
					    <tr>
					      <td>New Venues Day</td>
					      <td>0</td>					      
					    </tr>
					   </tbody>
					</table>
				</div>
			</div>

			<div class="Panel small-12 medium-8 large-8 columns">
				<div class="Panel_Title Grey_Gradient">
					<h4>Venues</h4>
				</div>
				<div class="Panel_Content">
					<table>
					  <thead>
					    <tr>
					      <th>Venue Name</th>
					      <th>Contact</th>
					      <th>Email</th>
					      <th>Credits</th>					      
					    </tr>
					  </thead>
					  <tbody>
					  @foreach($venues as $venue)
					    <tr>
					      <td>{{ $venue->venue_name }}</td>
					      <td>{{ $venue->staff()->last_name }}</td>
					      <td>{{ $venue->staff->email }}</td>
					      <td>{{ $venue->staff->account_balance }}</td>
					    </tr>
					   @endforeach					
					   </tbody>
					</table>
				</div>
			</div>		

		</div>
	</div>

@endsection
