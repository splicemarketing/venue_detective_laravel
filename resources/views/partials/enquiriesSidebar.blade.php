<div class="Panel small-12 medium-4 large-3 columns">
				<div class="Panel_Title Grey_Gradient">					
					<h4>Venues</h4>
				</div>
				<div class="Panel_Content">
					<ul class="accordion" data-accordion>
					  <li class="accordion-item" data-accordion-item>
					    <a href="#" class="accordion-title">All Venues</a>
					    <div class="accordion-content" data-tab-content>
					     	<a href="/my_leads">New</a>
					     	<a href="/quote?status=awaiting">Awaiting Quote</a>
					     	<a href="/quote?status=quoted">Quoted</a>
					     	<a href="/quote?status=notinterested">Not Interested</a>
					    </div>
					  </li>

					  @foreach ($venues as $venue)
						<li class="accordion-item is-active" data-accordion-item>
					    <a href="#" class="accordion-title">{{ $venue->venue_name}}</a>
					    <div class="accordion-content" data-tab-content>
					     	<a href="/my_leads?venue={{$venue->id}}">New</a>
					     	<a href="/quote?venue={{$venue->id}}&status=awaiting">Awaiting Quote</a>
					     	<a href="/quote?venue={{$venue->id}}&status=quoted">Quoted</a>
					     	<a href="/quote?venue={{$venue->id}}&status=notinterested">Not Interested</a>
					    </div>
					  </li>
					  @endforeach
					  
					</ul>
				</div>
			</div>