<div class="bg-light-grey">
	<div class="row pt-20 pb-20">
		<div class="small-12 columns">
			<div class="footer-redesign">
				<div class="links-logo-wrapper">
					<div class="row">
						<div class="small-12 medium-8 large-8 venue-mob-full columns">
							<div class="row">
								<div class="small-12 medium-4 venue-mob-3-col mb-24-mob columns">
									<div class="footer-links-holder">
										<h3>USEFUL LINKS</h3>
										<ul>
											<li><a href="/">Home</a></li>
											<li><a href="/about">About Us</a></li>
											<li><a href="/faqs">FAQ’s</a></li>
											<li><a href="/blog">Blog</a></li>
											<li><a href="/contact">Contact Us</a></li>
										</ul>
									</div>
								</div>
								<div class="small-12 medium-4 venue-mob-3-col mb-24-mob columns">
									<div class="footer-links-holder">
										<h3>VENUE DETECTIVE</h3>
										<ul>
											<li><a href="/">Find a Venue</a></li>
											<li><a href="/register">Register a Venue</a></li>
											<li><a href="/help_location">Help by Location</a></li>
											<li><a href="/help_type">Help by Event Type</a></li>
										</ul>
									</div>
								</div>
								<div class="small-12 medium-4 venue-mob-3-col mb-24-mob columns">
									<div class="footer-links-holder">
										<h3>GET CONNECTED</h3>
										<ul>
											<li class="pb-8"><a href="//www.facebook.com/VenueDetective" target="_blank"><img src="/images/ico-fb.gif" alt=""><span class="pl-8">Facebook</span></a></li>
											<li class="pb-8"><a href="//twitter.com/venuedetective"><img src="/images/ico-twitter.gif" target="_blank" alt=""><span class="pl-8">Twitter</span></a></li>
											<li class="pb-8"><a href="//pinterest.com/venuedetective/"><img src="/images/ico-pinterest.gif" target="_blank" alt=""><span class="pl-8">Pinterest</span></a></li>
											<li class="pb-8"><a href="//plus.google.com/103513433799591656664/posts"><img src="/images/ico-google-plus.gif" target="_blank" alt=""><span class="pl-8">Google+</span></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>


						<div class="small-12 medium-4 large-4 venue-mob-none columns">
							<div class="logo-slogan-section text-center float-right">
								<a href="/" class="fs-45 venue-logo-font"><span class="black">VENUE</span><span class="red">DETECTIVE</span></a>
								<p class="footer-venue-sign">Your FREE venue finding service</p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<div class="bg-dark-grey">
	<div class="row pt-20 pb-20">
		<div class="small-12 columns">
			<div class="bottom-line clear">
				<div class="bottom-line-wrapper"><span class="float-left">© 2016 Venue Detective. All rights reserved. <a href="#">XML Site Map</a> </span><span class="float-right">Website Development by <a href="http://www.rockettech.co.uk/" target="_blank">RocketTech</a></span></div>
			</div>
		</div>
	</div>
</div>

