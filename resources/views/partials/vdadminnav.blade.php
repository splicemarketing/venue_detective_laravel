<ul class="Admin_Navigation">
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/admin/">Home</a></li>
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/admin/venues">Venues</a></li>
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/admin/users">Users</a></li>
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/admin/reports">Reports</a></li>
</ul>