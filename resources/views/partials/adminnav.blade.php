<ul class="Admin_Navigation">
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/venue">Dashboard</a></li>
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/my_leads">Enquiries</a></li>
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/my_venues">My Venues</a></li>
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/my_accounts">Credits/ Subscription</a></li>
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/venue_reports">Reporting</a></li>
	<li class="Admin_Navigation_Link"><a class="Grey_Gradient" href="/venue_profile">My Profile</a></li>
	<li class="Admin_Navigation_Link"><a href="#">Credits: {{ $credits }}</a></li>
</ul>
