<div class="top-bar">
	<div class="row">
		<div class="small-12 column">
			<div class="title-bar" data-responsive-toggle="example-menu" data-hide-for="medium" style="display:none">
				<button class="menu-icon" type="button" data-toggle></button>
				<div class="title-bar-title">Menu</div>
			</div>
			<div class="top-bar" id="example-menu">
			  <div class="top-bar-left">
			    <ul class="dropdown menu" data-dropdown-menu>
						<li class="menu-text">
							<a class="fs-38 venue-logo-font" href="/">
								<span class="black">VENUE</span><span class="white">DETECTIVE</span>
							</a>
						</li>
						<li><a href="/">Home</a></li>
						<li><a href="/register">Register a Venue</a></li>
						<li><a href="/about">About us</a></li>
						<li><a href="/faqs">FAQ's</a></li>
						<li><a href="/blog">Blog</a></li>
						<li><a href="/contact">Contact us</a></li>
						@if (Sentinel::check())
							@if(Sentinel::getUser()->inRole('venues'))						
							<li>
								<a href="#">My Account</a>
								<ul class="menu vertical">
									<li><a href="/my_leads">Enquiries</a></li>
									<li><a href="/my_venues">My Venues</a></li>
									<li><a href="/logout">Logout</a></li>
								</ul>
							</li>
							@endif
							@if(Sentinel::getUser()->inRole('users'))						
							<li>
								<a href="#">My Account</a>
								<ul class="menu vertical">
									<li><a href="/user/quotes">Quotes</a></li>
									<li><a href="/logout">Logout</a></li>
								</ul>
							</li>
							@endif
							@if(Sentinel::getUser()->inRole('admins'))						
							<li>
								<a href="#">My Account</a>
								<ul class="menu vertical">
									<li><a href="/logout">Logout</a></li>
								</ul>
							</li>
							@endif							
						@endif
						@if (Sentinel::guest())
							<li>
								<a href="/login">Sign In</a>
							</li>	
						@endif
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>