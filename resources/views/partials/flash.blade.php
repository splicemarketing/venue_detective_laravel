@if(session()->has('flash_success'))
	<script>
			swal({   
				title: "{{ session('flash_success.title') }}",   
				text: "{{ session('flash_success.message') }}",
				type: "success",   
				timer: 2000,   
				showConfirmButton: false });
	</script>
@endif
@if(session()->has('flash_success_dismiss'))
	<script>
			swal({
				title: "{{ session('flash_success_dismiss.title') }}",
				text:  "{{ session('flash_success_dismiss.message') }}",
				type:  "success"
			});
	</script>
@endif
@if(session()->has('flash_warning_dismiss'))
	<script>
			swal({
				title: "{{ session('flash_warning_dismiss.title') }}",
				text:  "{{ session('flash_warning_dismiss.message') }}",
				type:  "warning"
			});
	</script>
@endif
@if(session()->has('flash_info_dismiss'))
	<script>
			swal({
				title: "{{ session('flash_info_dismiss.title') }}",
				text:  "{{ session('flash_info_dismiss.message') }}",
				type:  "info"
			});
	</script>
@endif
@if(session()->has('flash_warning'))
	<script>
			swal({   
				title: "{{ session('flash_warning.title') }}",   
				text: "{{ session('flash_warning.message') }}",
				type: "warning",   
				timer: 2000,   
				showConfirmButton: false });
	</script>
@endif
@if($errors->has('email'))
	<script>
			swal({
				title: "Duplicate Email",
				text: "Sorry, this email is already registered.",
				type: "warning"
			})
	</script>
@endif