@extends('layouts.master')

@section('content')
<div class="row mt-24 mb-24">
	<div class="small-12 columns">
		<div class="constrainer">
			<div class="static-new-header">
				<h1>Venue Detective Blog</h1>
			</div>
			<div class="block-gradient clearfix">
				<div class="large-8 columns">
					<article>
						
						@foreach ($posts as $post)
						<div class="row">
							<div class="large-6 columns">
								<p><img src="http://placehold.it/600x370&amp;text=Look at me!" alt="image for article"></p>
							</div>
							<div class="large-6 columns">
								<h5><a href="/blog/{{ $post->slug }}">{{ $post->title }}</a></h5>
								<p>
									<span><i class="fi-torso"> By Thadeus &nbsp;&nbsp;</i></span>
									<span><i class="fi-calendar"> 11/23/16 &nbsp;&nbsp;</i></span>
									<span><i class="fi-comments"> 6 comments</i></span>
								</p>
								<p>{{ $post->short_description }}</p>
								<p><a href="/blog/{{ $post->slug }}">Read More</a></p>
							</div>
						</div>
						<hr>
						@endforeach

						{{ $posts->render() }}

					</article>
				</div>				
			</div>

		</div>
	</div>
</div>

@endsection