<?php print '<?xml version="1.0" encoding="utf-8"?>' ?>

<feed xmlns="http://www.w3.org/2005/Atom">
    <title>Venue Detective</title>
    <subtitle>Your free venue finding service</subtitle>
    <link href="#" rel="self" />
    <updated>{{ Carbon\Carbon::now()->toATOMString() }}</updated>
    <author>
        <name>Ben Jackson</name>
    </author>
    <id>...</id>

    @foreach($leads as $lead)
		<entry>
			<title>{{ $lead->title }}</title>
			<link>{{ $lead->link }}</link>
			<id>...</id>
			<updated>Now</updated>
			<description>{{ $lead->description }}</description>
		</entry>	
	@endforeach

</feed>