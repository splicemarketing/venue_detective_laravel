@extends('layouts.master')


@section('content')
<div class="row mt-24 mb-24">
	<div class="small-12 columns">
		<div class="constrainer block-gradient">
			<div class="static-new-header">
				<h1>Contact Us</h1>
			</div>
			<div class="">
				<p class="first"><strong>We want to hear from you!</strong>  Whether you are a venue owner, interested in joining the team or simply a fan of our work, use social media or send us a note.  We are working hard but will try to respond to all emails within a few hours.</p>

	<img src="/images/icon-email.jpg" alt="Mail Ico"></div>
			
				<strong>&nbsp;support@venuedetective.co.uk</strong>



	<h4>Get Connected:</h4>

	<a href="https://www.facebook.com/VenueDetective" target="_blank"><div><img src="/images/icon-fb.png" alt="fb"></div></a>
	<a href="https://twitter.com/venuedetective" target="_blank"><div><img src="/images/icon-twitter.png" alt="t"></div></a>
	<a href="http://pinterest.com/venuedetective/" target="_blank"><div><img src="/images/icon-pinterest.png" alt="p"></div></a>


	<div class="red" id="message_details">

		<img src="/images/icon-email-sml.png" style="padding: 8px 8px 0 0; float: left;"><span style="padding: 4px 0 2px; display: inline-block;">Send us a message</span>
			Name:<input type="text" id="name" class="enquiry-input-field clear-val location_required text js-loc-entry tt-input" name="name">(required)
			Email:<input type="text" id="email" class="email required" name="email">(required) 
			Phone:<input type="text" value="" id="phone" class="required numbers phone" name="phone" >
			Message:<textarea class="message required" id="message" name="message" ></textarea>(required)
			Word Verification:(required)
			<input type="image" src="/images/btn-send-form.png" value="Submit">
	</div>




		</div>
	</div>
</div>	
@endsection