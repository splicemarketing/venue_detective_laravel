@extends('layouts.master')


@section('content')
<div class="row mt-24 mb-24">
	<div class="small-12 columns">
		<div class="constrainer">
			<div class="static-new-header">
			    <h1>Venue Required for a {{ $lead->eventType->name }}</h1>
			</div>

			<div class="static-new-content block-gradient">
			        <h2>{{ $lead->eventType->name }} within {{ $lead->radius }} miles of {{ $lead->location }} for a group of {{ $lead->number_of_people}} people on {{ date('F d, Y', strtoTime($lead->dates->first()->date)) }}</h2>
			        <p><a href="/register">Register</a> your venue now to follow this lead!</p>
			        <div id="faq1" class="faq">
			            <table>
						  <thead>
						    <tr>
						      <th width="200">Requirements</th>
						      <th></th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <td>Accomodation Required?</td>
						      <td>{{ $lead->accomodation_required }}</td>						     
						    </tr>
						    <tr>
						      <td>Disabled Access Required?</td>
						      <td>{{ $lead->disabled_access_required }}</td>						      
						    </tr>
						    <tr>
						      <td>Wireless Internet?</td>
						      <td>{{ $lead->wireless_internet_required }}</td>						      
						    </tr>
						     <tr>
						      <td>Kids Facilities?</td>
						      <td>{{ $lead->kids_facilities_requried }}</td>						      
						    </tr>
						     <tr>
						      <td>Sit Down Meal?</td>
						      <td>{{ $lead->sit_down_meal }}</td>						      
						    </tr>
						     <tr>
						      <td>External Catering?</td>
						      <td>{{ $lead->external_catering }}</td>						      
						    </tr>
						     <tr>
						      <td>Finger Buffet?</td>
						      <td>{{ $lead->finger_buffet }}</td>						      
						    </tr>
						     <tr>
						      <td>Canapes?</td>
						      <td>{{ $lead->canapes }}</td>						      
						    </tr>
						     <tr>
						      <td>AV Facilities Required?</td>
						      <td>{{ $lead->av_facilities_required }}</td>						      
						    </tr>
						     <tr>
						      <td>Paid Bar?</td>
						      <td>{{ $lead->paid_bar }}</td>						      
						    </tr>
						  </tbody>
						</table> 
			    		
			        </div>  
			    <br>
			        
			</div>

		</div>
	</div>
</div>
@endsection