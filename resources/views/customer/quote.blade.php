@extends('layouts.master')

@section('content')
<div class="row">
	<div class="small-12 column Admin_Panel">
		<div class="Panel small-12 medium-8 large-9 columns">
				<div class="Panel_Title Grey_Gradient">
					<span><a href="#"></a></span>
					<h4>Quoted Enquiries - Ref: {{ $leadSummary->id }}</h4>
				</div>
				<div class="Panel_Content">
					<div class="callout secondary">
					 <h5>Quote Summary</h5>
					 <p>Use this table to review the quotes you have received for this enquiry. More details for each quote can be found below, where you can also accept or decline the quote. Once a quote has been accepted, the details of the venue will be revealed. </p>
						<table class="User_Quote_Summary_Table">
							<thead>
								<tr>
									<th>Venue</th>
									<th>Quote</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>	
							@foreach ($quotes as $index => $quote)				
								<tr>
									<td>{{ $index+1 }}</td>
									<td>£{{ $quote->total_cost }}</td>
									<td></td>
									<td><a href="#">Click Here to read all the details</a></td>
									<td></td>								
									<td></td>
									<td></td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>

					<div class="callout warning">
						
						<h5>Enquiry Summary</h5>
						<table class="User_Quotes_Table">
							<tbody>
								<tr>
									<td>Reference Number:</td>
									<td>{{ $leadSummary->id }}</td>
								</tr>
								<tr>
									<td>Event Type:</td>
									<td>{{ $leadSummary->eventType->name }}</td>
								</tr>
								<tr>
									<td>Event Date:</td>
									<td>{{ date('F d, Y', strtoTime($leadSummary->dates->first()->date)) }}</td>
								</tr>
								<tr>
									<td>Date/Time Submitted:</td>
									<td>{{ date('F d, Y', strtotime($leadSummary->created_at)) }}</td>
								</tr>
								<tr>
									<td>Estimated Budget:</td>
									<td>£{{ $leadSummary->budget }}</td>
								</tr>
							</tbody>
						</table>

					</div>

					<div class="callout secondary">
						<ul class="tabs" data-tabs id="quote-tabs">
						    @foreach ($quotes as $index => $quote)
						  		<li class="tabs-title Enquiries_Tab"><a href="#panel{{ $index+1 }}" aria-selected="true">Venue {{ $index+1 }} - £{{ $quote->total_cost }}</a></li>
							@endforeach	
						</ul>
						<div class="tabs-content Enquiries_Tab_Content_Block" data-tabs-content="quote-tabs">
						  
							@foreach ($quotes as $index => $quote)
								<div class="tabs-panel" id="panel{{ $index+1}}">
								  <h4>Venue {{ $index+1 }}</h4>

								  <h5>Venue Capacity</h5>
								  <p>{{ $quote->min_attendance }} - {{ $quote->max_attendance }}</p>		
								  
								  <h5>Disabled Access Available?</h5>
								  <p>{{ $quote->disabled_access }}</p>
	
								  <h5>Quote Includes</h5>
								  <p>{!! $quote->included_in_cost !!}</p>

								  <h5>Extras</h5>
								  <p>{!! $quote->extras !!}</p>

								  <h5>Quote Breakdown</h5>
								  <p>{!! $quote->price_breakdown !!}</p>

								  <h5>Why Choose this Venue?</h5>
								  <p>{!! $quote->plug !!}</p>

								  <h5>Notes</h5>
								  <p>{!! $quote->notes !!}</p>

								  <h5>Total Quote: £{{ $quote->total_cost }}</h5>

								</div>
							@endforeach							  
						  	
						</div>
					</div>

				</div>

		</div>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
	(function(){
		$('ul#quote-tabs li:nth-child(1)').addClass('is-active');
		$('div.tabs-content div:nth-child(1)').addClass('is-active');
	})();
</script>
@endsection