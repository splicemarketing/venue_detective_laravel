@extends('layouts.master')

@section('content')
<div class="row">
		<div class="small-12 column Admin_Panel">
			
			<div class="Panel small-12 columns">
				<div class="Panel_Title Grey_Gradient">
					<span><a href="#"></a></span>
					<h4>My Enquiries</h4>
				</div>
				<div class="Panel_Content">
					<table>
						<thead>
							<tr>
								<th>Ref</th>
								<th>Event Type</th>
								<th>Event Date</th>
								<th>Date Sent</th>
								<th>Location</th>
								<th>Budget</th>
								<th>Quotes Received</th>
							</tr>
						</thead>
						<tbody>
						@foreach ($quotes as $quote)
							<tr>
								<td>{{ $quote->id }}</td>
								<td>{{ $quote->eventType->name }}</td>
								<td>{{ date('F d, Y', strtoTime($quote->dates->first()->date)) }}</td>
								<td>{{ date('F d, Y', strtoTime($quote->created_at)) }}</td>
								<td>{{ $quote->location }}</td>								
								<td>£ {{ $quote->budget }}</td>
								<td>3</td>
								<td><a href="/user/quotes/{{ $quote->id }}">View</a></td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>
</div>

@endsection