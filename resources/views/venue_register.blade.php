@extends('layouts.master')

@section('content')
<div class="row">
	<div class="small-12 columns mt-24">
		<div class="constrainer">
			<div class="customer-howto-redesign">
				<div class="header">
					<h1 class="top-block-h1">REGISTER YOUR VENUE FOR FREE!</h1>
				</div>
				<img src="/images/ban-3-steps.jpg" alt="Three steps to register a venue">
				<div class="steps reg-header">
					<div class="dotted-border reg-dotted-border text-center">
						<ol>
							<li class="pt-8 pb-8">1. REGISTER YOUR VENUE FOR FREE</li>
							<li class="pt-8 pb-8">2. WE SEND YOU RELEVANT LEADS</li>
							<li class="pt-8 pb-8">3. PURCHASE LEADS THAT INTEREST YOU</li>
						</ol>
		<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="small-12 medium-12 large-10 large-centered columns">
		<div class="form-explanation-redesign  text-center">
			<p class="fs-20 grey">Simply fill out the form below to register your venue and receive bespoke leads to your inbox, absolutely FREE! <br>Buy the leads that interest you, knowing you'll be one of 3 venues competing for the business.</p>
			<p class="note">Note: once you have completed this form you will be required to activate your account via email before you can add your full venue details.</p>
		</div>
	</div>
</div>

<div class="map-holder" id="map-holder" style="display:none;"></div>

<div class="row max-width-960">
	<div class="small-12 medium-12 large-8 columns register-form-bg mb-24">
		
		<div class="enquiry-steps-holder-redesign">
			<div class="venue-step-holder-redesign">
				<div class="clear">
					<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script> -->
					<div class="row">
						<div class="small-12 columns">
							<div class="message js-message"></div>
						</div>
					</div>
					<div class="row">
							
					{!! Form::open(['url' => '/register', 'method' => 'post', 'id' => 'register_form']) !!}
						<!-- Did the user click on a advertised lead before he registered his venue?: -->
						<input type="hidden" name="action" value="register" id="action">
						<input type="hidden" name="latitude" class="latitude_reg" value="">
						<input type="hidden" name="longitude" class="longitude_reg" value="">


						<div class="small-12 medium-6 columns">
							<div class="venue-step-column form-right-border">
								<div class="header first clear">
									<img class="float-left mr-16" src="/images/ico-venue-details.png" alt="">
									<h2 class="reg-form-h2">YOUR VENUE</h2>
								</div>
								<div class="input-holder clear">
									<input type="text" name="venue_name" value="" placeholder="VENUE NAME... *" class="required text js-clear-val js-reg-input-req" required>
								</div>
								<div class="input-holder clear">
									<input type="text" name="address_one" value="" placeholder="ADDRESS LINE 1... *" class="required text js-clear-val js-reg-input-req" required>
								</div>
								<div class="input-holder clear">
									<input type="text" name="address_two" value="" placeholder="ADDRESS LINE 2..." class="js-clear-val" >
								</div>
								<div class="input-holder clear">
									<input type="text" name="address_three" value="" placeholder="ADDRESS LINE 3..." class="js-clear-val">
								</div>
								<div class="input-holder clear">
									<input type="text" name="city" value="" placeholder="TOWN/CITY... *" class="required text js-clear-val js-reg-input-req" required>
								</div>
								<div class="input-holder clear">
									<input type="text" name="county" value="" placeholder="COUNTY... *" class="required text js-clear-val js-reg-input-req" required>
								</div>
								<div class="input-holder clear">
									<input type="text" name="post_code" value="" placeholder="POSTCODE... *" class="required mixed js-clear-val js-reg-input-req js-postcode" required>
								</div>
							</div>
						</div>


						<div class="small-12 medium-6 columns">
							<div class="venue-step-column last">
								<div class="header last clear">
									<img class="float-left mr-16" src="/images/ico-contact-details.png" alt="">
									<h2 class="reg-form-h2">YOUR DETAILS</h2>
								</div>
								<div class="input-holder clear">
									<input type="text" name="first_name" value="" placeholder="FIRST NAME... *" class="required text js-clear-val js-reg-input-req" required>
								</div>
								<div class="input-holder clear">
									<input type="text" name="last_name" value="" placeholder="SURNAME... *" class="required text js-clear-val js-reg-input-req" required>
								</div>
								<div class="input-holder clear">
									<input type="text" name="job_title" value="" placeholder="JOB TITLE... *" class="required text js-clear-val js-reg-input-req" required>
								</div>
								<div class="input-holder clear">
									<input type="email" value="" placeholder="EMAIL... *" name="email" class="required js-email js-clear-val js-reg-input-req" required>
								</div>
								<div class="input-holder clear">
									<input type="tel" name="phone" value="" placeholder="PHONE... *" class="required text js-clear-val js-reg-input-req js-phone" required>
								</div>
								<div class="input-holder clear">
									<input type="password" name="password" value="" placeholder="PASSWORD... *" class="required text password1 js-clear-val js-reg-input-req" required>
								</div>
								<div class="input-holder clear">
									<input type="password" name="password2" value="" placeholder="REPEAT PASSWORD... *" class="required password2 js-clear-val js-reg-input-req" required>
								</div>
							</div>
						</div>




					</form>
					</div>
				</div>



				<div class="btn-row text-center">
					<div class="enquiry-form-button send-button js-reg-form-submit clear">
						<i class="fa fa-chevron-right" aria-hidden="true"></i>
						<span>REGISTER MY VENUE</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="small-12 medium-12 large-4 columns">
		<div class="latest-leads text-center">
			<h2 class="fs-22 grey font-fam">OUR LATEST LEADS</h2>
			<div class="leads-holder">
				<ul>
					<!-- Display four latest leads -->

					<li><a href="http://www.venuedetective.co.uk/venue_needed.php?id=447" target="_blank">
						<div class="lead-details" style="background:url(/images/business-224.jpg);">
							<div class="black-50-bg">
							</div>
							<div class="leads-text-block">
								<h3 class="fs-26 white mt-24">CONFERENCE</h3>
								<div class="white fs-16">50 PEOPLE</div>
								<div class="white fs-16">Coventry</div>
							</div>

						</div></a>
					</li>

					<li><a href="http://www.venuedetective.co.uk/venue_needed.php?id=447" target="_blank">
						<div class="lead-details" style="background:url(/images/business-224.jpg);">
							<div class="black-50-bg">
							</div>
							<div class="leads-text-block">
								<h3 class="fs-26 white mt-24">CONFERENCE</h3>
								<div class="white fs-16">50 PEOPLE</div>
								<div class="white fs-16">Coventry</div>
							</div>

						</div></a>
					</li>

					<li><a href="http://www.venuedetective.co.uk/venue_needed.php?id=447" target="_blank">
						<div class="lead-details" style="background:url(/images/business-224.jpg);">
							<div class="black-50-bg">
							</div>
							<div class="leads-text-block">
								<h3 class="fs-26 white mt-24">CONFERENCE</h3>
								<div class="white fs-16">50 PEOPLE</div>
								<div class="white fs-16">Coventry</div>
							</div>

						</div></a>
					</li>

					<li><a href="http://www.venuedetective.co.uk/venue_needed.php?id=447" target="_blank">
						<div class="lead-details" style="background:url(/images/business-224.jpg);">
							<div class="black-50-bg">
							</div>
							<div class="leads-text-block">
								<h3 class="fs-26 white mt-24">CONFERENCE</h3>
								<div class="white fs-16">50 PEOPLE</div>
								<div class="white fs-16">Coventry</div>
							</div>

						</div></a>
					</li>


				</ul>
			</div>
		</div>
	</div>
</div>

@endsection