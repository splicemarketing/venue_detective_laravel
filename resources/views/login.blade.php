@extends('layouts.master')

@section('content')
<div class="row Admin_Panel Login_Admin">
	<div class="Login_Panel">
		{!! Form::open(['url' => '/login', 'method' => 'post']) !!}
			{!! Form::label('email', 'Email Address') !!}
			{!! Form::email('email') !!}

			{!! Form::label('password', 'Password') !!}
			{!! Form::password('password') !!}

			<button class="btn Login_Submit" type="submit" value="Login">Login</button>

		{!! Form::close() !!}
	</div>
</div>
		
		

@endsection