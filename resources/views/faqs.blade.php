@extends('layouts.master')


@section('content')
<div class="row mt-24 mb-24">
	<div class="small-12 columns">
		<div class="constrainer">
			<div class="static-new-header">
			    <h1>Frequently Asked Questions</h1>
			</div>

			<!-- <div id="FAQ"> -->
			<div class="static-new-content block-gradient">
			        <h2>How do I use the Venue Detective service?</h2>
			        <div id="faq1" class="faq">
			            <p>Simply complete the enquiry form with details about your event and the venue you are looking for and we will save you time searching for a venue, at no charge to you.</p> 
			    		<p>Once you have submitted your details we will match your requirements with three venues, who will then contact you to discuss the details of your event.</p>
			        </div>  
			    <br>
			        <h2>Who is the Venue Detective service for?</h2>
			        <div id="faq2" class="faq">
			        	<p>The Venue Detective service is for anyone arranging an event who wants to use a venue. The service is suitable for both private individuals and businesses.</p>
			        </div> 
			    <br>
			        <h2>How much does the Venue Detective service cost?</h2>
			        <div id="faq3" class="faq">
			            <p>It costs absolutely nothing, the Venue Detective service is Free.</p>
			        </div>
			    <br>
			        <h2>What am I agreeing to?</h2>
			        <div id="faq4" class="faq">
			            <p class="last">All we ask is that you are open and honest about your requirements. In return we ask all of our venues to show our customers the same level of respect. Throughout the process you are under no obligation to accept a venue put forward by Venue Detectives. If you prefer to use an alternative or not go ahead for any reason you are free to do so. However, we would appreciate your feedback so we can improve our service for other people wishing to use Venue Detective for their venue requirements.</p>
			        </div>
			</div>

		</div>
	</div>
</div>
@endsection