@extends('layouts.master')


@section('content')
<div class="row mt-24 mb-24">
	<div class="small-12 columns">
		<div class="constrainer">
			<div class="static-new-header">
				<h1>About Venue Detective</h1>
			</div>
			<div class="static-new-content block-gradient">
				<p class="text-center">
					<img class="float-right" src="/images/img-figure-magnifier.jpg">
				</p>
				<p class="first">Venue Detective is a <b>Free Venue Finding Service</b> for anyone looking to arrange an event and needs to find that perfect venue. Venue Detective saves you hours of hunting around trying to find a venue that meets your needs. Our expertise means we know which questions to ask to match you to your ideal venue. Our professional, courteous service ensures that you will only be contacted by suitable venues. </p>
				
				<p>Simply complete the enquiry form with details about your event and the venue you are looking for and we will save you time searching for a venue, at no charge to you. Once you have submitted your details we will match your requirements with the appropriate venues. We will also make sure that no more than three venues contact you so you are not swamped with a flood of phone calls interrupting your day and event planning.</p>
				<h2>Finding the Ideal Venue for Events is Easy Using Venue Detective</h2>
				
				<p>If we do not have a suitable venue in our extensive database, we will actively seek out a venue just for you.</p>
				
				<p>Venue Detective is committed to providing the highest quality of service, to enable us to continuously improve our service we encourage all our customers to give us feedback about how we can improve even more, simply email <a style="color: #CF2828;" href="mailto:support@venuedetective.co.uk">support@venuedetective.co.uk</a> with your comments.</p>
				
				<p>The Venue Detective service is completely free to all customers; we are paid by the venues so that we can provide this service to you. We are also completely independent with no ties or loyalties to ANY venues or chains, so that we can offer the best service to you.</p>
				
				<p class="last">We look forward to receiving your Venue Search Request form.</p>
			</div>

		</div>
	</div>
</div>
@endsection