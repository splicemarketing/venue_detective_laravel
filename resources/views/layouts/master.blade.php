<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Venue Detective</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/vendor.css">   
  </head>
  
  <body class="body-bg-img">
   
    @include('partials.navigation')
    
    @yield('content')
    
    @include('partials.footer')
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="/js/jquery.matchHeight.js"></script>
    <script src="/js/typeahead.bundle.js"></script>
    <script src="/js/vendor.js"></script>
    <script src="/js/app.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlatfiHmpcI-iEjGvniqcp4kRFJS2F3CE&callback=make_map" async defer></script>
      
    @yield('scripts')
    @include('partials.flash')

    <script>
      $(document).foundation();
      $(function() {
          $('.js-three-pic-text').matchHeight({
              byRow: false,
              property: 'height',
              target: null,
              remove: false
          });
          $('.level-holder').matchHeight({
              byRow: false,
              property: 'height',
              target: null,
              remove: false
          });
          $('.reg-form-h2').matchHeight({
              byRow: false,
              property: 'height',
              target: null,
              remove: false
          });


      });
      
    </script>
  </body>
</html>