<?php

namespace App\Mailers;

use Mail;
use App\Models\User;


class UserMailer
{
	
	public function newUserWelcome($user)
	{
		Mail::send('emails.newUser', ['user' => $user], function ($message) use ($user) {
			$message->to($user->email)->subject('Welcome To Venue Detective!');
		});
	}

	public function newVenueRegistered($activation)
	{
		$user = User::find($activation->user_id);

		$data = [
			'name' => $user->first_name,
			'email' => $user->email,
			'id'	=> $activation->user_id, 
			'activation' => $activation->code,
		];

		Mail::send('emails.newVenueRegistered', $data, function ($message) use ($data) {
			$message->to($data['email'])->subject("Activate your Venue Detective Account");
		});
	}

	public function newLeadRegistered($lead)
	{
		$user = User::find($lead->customer_id);

		$data = [
			'name' => $user->first_name,
			'email' => $user->email,
		];
		
		Mail::send('emails.newLead', $data, function ($message) use ($data) {
			$message->to($data['email'])->subject("We're looking for great venues for you!");
		});
	}

	public function newQuoteSent($quote, $user)
	{
		Mail::send('emails.sendQuote', ['user' => $user, 'quote' => $quote], function ($message) use ($user) {
			$message->to($user->email)->subject("You've received a Quote!");
		});
	}



}