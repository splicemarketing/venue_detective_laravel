<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Sentinel;
use App\Models\User;
use App\Statistics\EnquiryStats;

class ViewComposerServiceProvider extends ServiceProvider
{
    
    
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.adminnav', function($view)
        {
            $user = Sentinel::getUser();   
            $view->with('credits', $user->account_balance);
        });

        view()->composer('partials.enquiriesSidebar', function($view)
        {
            $venues = User::find(Sentinel::getUser()->id)->venues()->get();
            $view->with('venues', $venues);
        });

        view()->composer('admin.dashboard', function($view)
        {
            $view->with('summary', EnquiryStats::enquirySummary());
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
