<?php

namespace App\Queries;
use Illuminate\Database\Eloquent\Builder;

class QuoteFilters extends QueryFilters
{
	
	/**
	 * Filter By Venue
	 * @param  id $venue
	 * @return Builder
	 */
	public function venue($venue)
	{
		return $this->builder->where('venue_quoted_id', $venue);
	}

	/**
	 * Filter by Quote status
	 * @return [type] [description]
	 */
	public function status($status)
	{
		switch ($status) {
			case "awaiting":
				return $this->builder->where('awaiting_quote', 1);
				break;
			case "quoted":
				return $this->builder->where('quoted', 1);
				break;
			case "notinterested":
				return $this->builder->where('not_interested', 1);
				break;
			default:
				return $this->builder->where('awaiting_quote', 1);
				break;
		}
		
	}
	
}