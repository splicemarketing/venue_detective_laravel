<?php

namespace App\Http;

class Flash {
	
	public function success_message($title, $message)
	{
		session()->flash('flash_success', [
			'title'	 	=> $title,
			'message'	=> $message
		]);
	}

	public function success_message_dismiss($title, $message)
	{
		session()->flash('flash_success_dismiss', [
			'title'		=>	$title,
			'message'	=> 	$message
		]);
	}

	public function warning_message_dismiss($title, $message)
	{
		session()->flash('flash_warning_dismiss', [
			'title'		=>	$title,
			'message'	=> 	$message
		]);
	}

	public function info_message_dismiss($title, $message)
	{
		session()->flash('flash_info_dismiss', [
			'title'		=>	$title,
			'message'	=> 	$message
		]);
	}

	public function warning_message($title, $message)
	{
		session()->flash('flash_warning', [
			'title'	 	=> $title,
			'message'	=> $message
		]);
	}

}