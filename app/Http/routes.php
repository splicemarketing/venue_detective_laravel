<?php
/**
 * Public Page Routes
 */
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::post('/enquiry', 'EnquiryController@newEnquiry');

// Venue Register
Route::get('/register', 'VenueController@index');
Route::post('/register', 'VenueController@register');
Route::get('/register/{id}/{activation}', 'VenueController@activate');

// Pages
Route::get('/about', 'HomeController@about');
Route::get('/faqs', 'HomeController@faqs');

Route::get('/contact', 'HomeController@contact');
Route::post('/contact', 'HomeController@contactsubmit');
// Route::get('/blog', 'BlogController@viewAllPosts');
// Route::get('/blog/{slug}', 'BlogController@viewSinglePost');

// Feed
Route::get('/feed', 'FeedController@index');
Route::get('/feed/{id}', 'FeedController@getLeadLandingPage');

// Sitemap
Route::get('/xml_sitemap', 'HomeController@sitemap');


// Login 
Route::group(['middleware' => 'guest'], function() {
	Route::get('/login', 'AccountsController@login');
	Route::post('/login', 'AccountsController@auth');
});

Route::get('/logout', 'AccountsController@logout');

/**
 * Venue Admin Routes
 */
Route::group(['middleware' => ['auth', 'venue']], function() {
	
	Route::get('/venue', ['as' => 'venue_admin', 'uses' => 'VenueController@dashboard']);
	
	Route::get('/my_leads', ['as' => 'my_leads', 'uses' => 'EnquiryController@allEnquiries']);
	Route::get('/my_leads/lead/{id}', 'EnquiryController@enquiryDetail');

	Route::post('/purchase_quote', 'QuotesController@purchase');
	Route::get('/quote', ['as' => 'quote', 'uses' => 'QuotesController@getQuotes']);
	Route::get('/quote/{id}', 'QuotesController@singleQuote');
	Route::post('/sendQuote', 'QuotesController@sendQuote');
	
	Route::get('/my_venues', 'VenueController@venues');
	Route::post('/my_venues/update/{id}', 'VenueController@updateVenue');
	
	Route::get('/my_accounts', 'AccountsController@account');
		Route::post('/credits/twenty', 'BillingController@twentyCredits');
		Route::post('/credits/fourty', 'BillingController@fourtyCredits');
		Route::post('/credits/sixty', 'BillingController@sixtyCredits');
		Route::post('/credits/onehundred', 'BillingController@oneHundredCredits');
	Route::post('/buy_subscription', 'BillingController@subscribe');
	
	Route::get('/venue_reports', 'AccountsController@reports');
	
	Route::get('/venue_profile', 'AccountsController@profile');
	
	Route::post('/venue_profile/edit', 'AccountsController@updateProfile');
});

/**
 * Customer Admin Routes
 */
Route::group(['middleware' => ['auth','customer']], function() {
	
	Route::get('/user/quotes', ['as' => 'user_admin', 'uses' => 'CustomerController@listQuotes']);
	Route::get('/user/quotes/{id}', 'CustomerController@viewQuotes');

});

/**
 * Venue Detective Admin Routes
 */
Route::group(['middleware' => ['auth','admin']], function() {
	Route::get('/admin', ['as' => 'vendec_admin', 'uses' => 'AdminController@dashboard']);

	Route::get('/admin/venues', 'AdminController@venues');

	Route::get('/admin/users', 'AdminController@users');

	Route::get('/admin/reports', 'AdminController@reports'); 

});


