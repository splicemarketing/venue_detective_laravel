<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Venue;


class AdminController extends Controller
{
    public function dashboard()
    {
    	return view('vdadmin.dashboard');
    }

    public function venues()
    {
    	$venues = Venue::with('staff')->get();
    	return view('vdadmin.venues')->with(['venues' => $venues]);
    }

    public function users()
    {
    	return view('vdadmin.users');
    }

    public function reports()
    {
    	return view('vdadmin.reports');
    }

}
