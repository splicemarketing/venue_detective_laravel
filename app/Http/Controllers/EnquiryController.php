<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Enquiry;
use App\Events\UserRegistered;
use App\Models\Date;
use App\Models\Lead;
use App\Models\User;
use Sentinel;


class EnquiryController extends Controller
{
    /**
     * Finds User or creates if new.
     * Registers a new Enquiry 
     * Fires a new Enquiry Event
     * 
     * @return [type] [description]
     */
    public function newEnquiry(Enquiry $enquiry)
    {
        // If User is registered
        if (Sentinel::check())
        {
             $lead = Lead::register([
                'customer_id' => Sentinel::getUser()->id,
                'event_type' => $enquiry['event_type'],
                'number_of_people' => $enquiry['number_of_people'],
                'longitude' => $enquiry['longitude_val'],
                'latitude' => $enquiry['latitude_val'],
                'location' => $enquiry['location'],
                'radius' => $enquiry['radius'],
                'accomodation_required' => $enquiry->has('accomodation_required'),
                'disabled_access_required' => $enquiry->has('disabled_access_required'),
                'wireless_internet_required' => $enquiry->has('wireless_internet_required'),
                'kids_facilities_requried' => $enquiry->has('kids_facilities_requried'),
                'is_food_required' => $enquiry->has('is_food_required'),
                'sit_down_meal' => $enquiry->has('sit_down_meal'),
                'external_catering' => $enquiry->has('external_catering'),
                'finger_buffet' => $enquiry->has('finger_buffet'),
                'canapes' => $enquiry->has('canapes'),
                'av_facilities_required' => $enquiry->has('av_facilities_required'),
                'av_facility_lecturn' => $enquiry->has('av_facility_lecturn'),
                'av_facility_projector_screen' => $enquiry->has('av_facility_projector_screen'),
                'av_facility_microphone' => $enquiry->has('av_facility_microphone'),
                'av_facility_autocue' => $enquiry->has('av_facility_autocue'),
                'av_facility_pa_system' => $enquiry->has('av_facility_pa_system'),
                'av_facility_video_recording' => $enquiry->has('av_facility_video_recording'),
                'venue_type_pubs_restaurants' => $enquiry->has('venue_type_pubs_restaurants'),
                'venue_type_hotels' => $enquiry->has('venue_type_hotels'),
                'venue_type_unusual' => $enquiry->has('venue_type_unusual'),
                'venue_type_church' => $enquiry->has('venue_type_church'),
                'venue_type_town_hall' => $enquiry->has('venue_type_town_hall'),
                'venue_type_garden' => $enquiry->has('venue_type_garden'),
                'venue_type_stately_home' => $enquiry->has('venue_type_stately_home'),
                'venue_type_sporting_leisure' => $enquiry->has('venue_type_sporting_leisure'),
                'venue_type_dedicated' => $enquiry->has('venue_type_dedicated'),
                'venue_type_barn' => $enquiry->has('venue_type_barn'),
                'venue_type_marquee' => $enquiry->has('venue_type_marquee'),
                'venue_type_historic' => $enquiry->has('venue_type_historic'),
                'venue_type_castle' => $enquiry->has('venue_type_castle'),
                'venue_type_nightclub' => $enquiry->has('venue_type_nightclub'),
                'drinks_required' => $enquiry->has('drinks_required'),
                'soft_drinks_only' => $enquiry->has('soft_drinks_only'),
                'tea_coffee' => $enquiry->has('tea_coffee'),
                'paid_bar' => $enquiry->has('paid_bar'),
                'lead_credit_price' => $enquiry['budget'],
                'lead_cash_price' => $enquiry['budget'],
                'budget' => $enquiry['budget']
            ]);

            switch ($enquiry['date-type']) {
                case '1':
                    $leadDate = new Date;
                    $leadDate->lead_id = $lead->id;
                    $leadDate->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate->save();
                    break;
                case '2':
                    $leadDate2 = new Date;
                    $leadDate2->lead_id = $lead->id;
                    $leadDate2->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate2->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate2->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate2->save();
                    $leadDate3 = new Date;
                    $leadDate3->lead_id = $lead->id;
                    $leadDate3->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate3->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate3->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate3->save();
                    break;
                case '3':
                    $leadDate4 = new Date;
                    $leadDate4->lead_id = $lead->id;
                    $leadDate4->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate4->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate4->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate4->save();
                    $leadDate5 = new Date;
                    $leadDate5->lead_id = $lead->id;
                    $leadDate5->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate5->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate5->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate5->save();
                    break;                
                default:
                    # code...
                    break;
            }

            flash_success("Great!", "Thank you for posting a venue search request. We\'re on it!");
            
            // Redirect to Success Page
            return redirect()->route('home');
        }
       

       if (Sentinel::guest())
       {
            $user = Sentinel::registerAndActivate([
                'email' => $enquiry['email'],
                'password' => $enquiry['password'],
                'first_name' => $enquiry['first_name'],
                'last_name' => $enquiry['last_name'],
                'phone' => $enquiry['phone']
            ]);

            Sentinel::loginAndRemember($user);  

            // Assign the User Role to the User
            $role = Sentinel::findRoleByName('Users');
            $newUser = User::find($user->id);
            $newUser->roles()->attach($role);

            event(new UserRegistered($newUser));

            $lead = Lead::register([
                'customer_id' => $user->id,
                'event_type' => $enquiry['event_type'],
                'number_of_people' => $enquiry['number_of_people'],
                'longitude' => $enquiry['longitude_val'],
                'latitude' => $enquiry['latitude_val'],
                'location' => $enquiry['location'],
                'radius' => $enquiry['radius'],
                'accomodation_required' => $enquiry->has('accomodation_required'),
                'disabled_access_required' => $enquiry->has('disabled_access_required'),
                'wireless_internet_required' => $enquiry->has('wireless_internet_required'),
                'kids_facilities_requried' => $enquiry->has('kids_facilities_requried'),
                'is_food_required' => $enquiry->has('is_food_required'),
                'sit_down_meal' => $enquiry->has('sit_down_meal'),
                'external_catering' => $enquiry->has('external_catering'),
                'finger_buffet' => $enquiry->has('finger_buffet'),
                'canapes' => $enquiry->has('canapes'),
                'av_facilities_required' => $enquiry->has('av_facilities_required'),
                'av_facility_lecturn' => $enquiry->has('av_facility_lecturn'),
                'av_facility_projector_screen' => $enquiry->has('av_facility_projector_screen'),
                'av_facility_microphone' => $enquiry->has('av_facility_microphone'),
                'av_facility_autocue' => $enquiry->has('av_facility_autocue'),
                'av_facility_pa_system' => $enquiry->has('av_facility_pa_system'),
                'av_facility_video_recording' => $enquiry->has('av_facility_video_recording'),
                'venue_type_pubs_restaurants' => $enquiry->has('venue_type_pubs_restaurants'),
                'venue_type_hotels' => $enquiry->has('venue_type_hotels'),
                'venue_type_unusual' => $enquiry->has('venue_type_unusual'),
                'venue_type_church' => $enquiry->has('venue_type_church'),
                'venue_type_town_hall' => $enquiry->has('venue_type_town_hall'),
                'venue_type_garden' => $enquiry->has('venue_type_garden'),
                'venue_type_stately_home' => $enquiry->has('venue_type_stately_home'),
                'venue_type_sporting_leisure' => $enquiry->has('venue_type_sporting_leisure'),
                'venue_type_dedicated' => $enquiry->has('venue_type_dedicated'),
                'venue_type_barn' => $enquiry->has('venue_type_barn'),
                'venue_type_marquee' => $enquiry->has('venue_type_marquee'),
                'venue_type_historic' => $enquiry->has('venue_type_historic'),
                'venue_type_castle' => $enquiry->has('venue_type_castle'),
                'venue_type_nightclub' => $enquiry->has('venue_type_nightclub'),
                'drinks_required' => $enquiry->has('drinks_required'),
                'soft_drinks_only' => $enquiry->has('soft_drinks_only'),
                'tea_coffee' => $enquiry->has('tea_coffee'),
                'paid_bar' => $enquiry->has('paid_bar'),
                'lead_credit_price' => $enquiry['budget'],
                'lead_cash_price' => $enquiry['budget'],
                'budget' => $enquiry['budget']
            ]);

            switch ($enquiry['date-type']) {
               case '1':
                    $leadDate = new Date;
                    $leadDate->lead_id = $lead->id;
                    $leadDate->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate->save();
                    break;
                case '2':
                    $leadDate2 = new Date;
                    $leadDate2->lead_id = $lead->id;
                    $leadDate2->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate2->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate2->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate2->save();
                    $leadDate3 = new Date;
                    $leadDate3->lead_id = $lead->id;
                    $leadDate3->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate3->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate3->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate3->save();
                    break;
                case '3':
                    $leadDate4 = new Date;
                    $leadDate4->lead_id = $lead->id;
                    $leadDate4->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate4->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate4->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate4->save();
                    $leadDate5 = new Date;
                    $leadDate5->lead_id = $lead->id;
                    $leadDate5->date = date('Y-m-d H:i:s', $enquiry['timestampday0']);
                    $leadDate5->start = date('Y-m-d H:i:s', $enquiry['timestampstart0']);
                    $leadDate5->finish = date('Y-m-d H:i:s', $enquiry['timestampend0']);
                    $leadDate5->save();
                    break;                
                default:
                    # code...
                    break;
            }
            
            flash_success_dismiss("Great!", "Thank you for posting a venue search request. We\'re on it!");            

            // Redirect to Success Page
            return redirect()->route('home');
       }
        
    }

    /**
     * Get all enquiries for all Venues for A Venue User
     * @return View Enquiries
     */
    public function allEnquiries(Request $request)
    {
        if($request['venue'])
        {
            $enquiries = Lead::with('event_types', 'dates')->leadsForVenue($request['venue']);     
            return view('admin.enquiries')->with(['enquiries' => $enquiries]);
        }
        
        $enquiries = Lead::with('event_types', 'dates')->leadsForVenues();
    	return view('admin.enquiries')->with(['enquiries' => $enquiries]);
    }


    public function enquiryDetail($id, Request $request)
    {
        $enquiry = Lead::findOrFail($id);
        $venueInRange = $request['venue'];
        return view('admin.enquiry')->with(['enquiry' => $enquiry, 'venue_id' => $venueInRange]);
    }

}
