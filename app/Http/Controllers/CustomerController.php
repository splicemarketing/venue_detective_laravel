<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lead;
use App\Models\Quote;
use App\Models\User;
use Sentinel;

class CustomerController extends Controller
{
    

    /**
     * View the customers Enquiries
     * @return [type] [description]
     */
    public function listQuotes()
    {
    	$user = User::find(Sentinel::getUser()->id);
    	$quotes = Lead::where('customer_id', '=', $user->id)->get();
        return view('customer.quotes')->with(['quotes' => $quotes]);
    }

    
    /**
     * View the Quotes given for a customers enquiry
     * Include a summary of the Enquiry
     * @param  $id Quote ID
     */
    public function viewQuotes($id)
    {
        $leadSummary = Lead::find($id);
        $quotes = Quote::where('lead_id', '=', $id)->get();
        return view('customer.quote')->with(['quotes' => $quotes, 'leadSummary' => $leadSummary]);
    }


}
