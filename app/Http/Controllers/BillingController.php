<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Billing\BillingInterface;
use App\Models\User;
use Sentinel;


class BillingController extends Controller
{
	public $billingInterface;

	public function __construct(BillingInterface $billingInterface)
	{
		$this->billingInterface = $billingInterface;
	}

    public function twentyCredits(Request $request)
    {
    	$data = ['email' => $request['email'], 'stripeToken' => $request['stripeToken']];
    	
    	$this->billingInterface->charge($data, 3000); // £30 for 20
    	
    	$this->addCreditsToUser(20);

    	flash_success("Great!", "Your credits have been purchased");

    	return redirect()->back();
    }

    public function fourtyCredits(Request $request)
    {
    	$data = ['email' => $request['email'], 'stripeToken' => $request['stripeToken']];

    	$this->billingInterface->charge($data, 5000); // £50 for 40

    	$this->addCreditsToUser(40);

    	flash_success("Great!", "Your credits have been purchased");

    	return redirect()->back();
    }

    public function sixtyCredits(Request $request)
    {
    	$data = ['email' => $request['email'], 'stripeToken' => $request['stripeToken']];

    	$this->billingInterface->charge($data, 6000); // £60 for 60

    	$this->addCreditsToUser(60);

    	flash_success("Great!", "Your credits have been purchased");

    	return redirect()->back();
    }

    public function oneHundredCredits(Request $request)
    {
    	$data = ['email' => $request['email'], 'stripeToken' => $request['stripeToken']];

    	$this->billingInterface->charge($data, 7500); // £75 for 100

    	$this->addCreditsToUser(100);

    	flash_success("Great!", "Your credits have been purchased");

    	return redirect()->back();
    }

    public function addCreditsToUser($amount)
    {
    	$user = User::find(Sentinel::getUser()->id);
    	$user->account_balance = $user->account_balance + $amount;
    	$user->save();
    }	

    public function subscribe()
    {

    }
}
