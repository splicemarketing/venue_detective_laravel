<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterVenue;
use App\Events\VenueRegistered;
use App\Models\Venue;
use App\Models\User;
use Activation;
use Sentinel;

class VenueController extends Controller
{
    
    /**
     * Serve the Venue Registration Page
     * @return [type] [description]
     */
	public function index()
	{
		return view('venue_register');
	}

    
	/**
	 * Validate the Venue Registration, 
	 * Fire a Venue Registration Event,
	 * Save to Database
	 * @return View Venue Register
	 */
    public function register(RegisterVenue $registervenue)
    {       

        if (Sentinel::guest())
        {            
            // Create the new Venue User
            $user = Sentinel::register([
                'email' => $registervenue['email'],
                'password' => $registervenue['password'],
                'first_name' => $registervenue['first_name'],
                'last_name' => $registervenue['last_name'],
                'phone' => $registervenue['phone']
            ]);                     
            

            // Assign the Venue Role to the User
            $role = Sentinel::findRoleByName('Venues');
            $newUser = User::find($user->id);            
            $newUser->roles()->attach($role);

            // Register the new Venue
            $venue = Venue::register([ 
                'venue_name' => $registervenue['venue_name'],
                'address_one' => $registervenue['address_one'],
                'address_two' => $registervenue['address_two'],
                'address_three' => $registervenue['address_three'],
                'city' => $registervenue['city'],
                'county' => $registervenue['county'],
                'longitude' => $registervenue['longitude'],
                'latitude' => $registervenue['latitude'],
                'post_code' => $registervenue['post_code'],
            ]);

            $newVenue = Venue::find($venue->id);
            $newVenue->staff()->attach($newUser);

            // Send Activation Email
            $activation = Activation::create($newUser);
            event(new VenueRegistered($activation));

            // Flash a success message
            flash_success_dismiss('Great!', "Please check your inbox to activate your account."); 

            // Login and redirect to Venue Admin
            // Sentinel::loginAndRemember($user);   
            return redirect()->route('home');
        }   
    }

    /**
     * Activate a Venue Account, and Login the User.
     * 
     * @param  $id  User Id       
     * @param  $activation  Activation Code
     * @return             
     */
    public function activate($id, $activation)
    {
        $user = Sentinel::findById($id);

        if (Activation::complete($user, $activation))
        {
            Sentinel::loginAndRemember($user);
            flash_success_dismiss('Great!', "Your Account has been activated.");   
            return redirect()->route('venue_admin');
        }
        else
        {
            // Flash a success message
            flash_success_dismiss('Sorry!', "This activation code is invalid, please contact Venue Detective.");
            return redirect()->route('home'); 
        }
    }

    /**
     * Dashboard for Venue Admin after login
     * @return [type] [description]
     */
    public function dashboard()
    {
    	return view('admin.dashboard');
    }

    /**
     * Get All Venues for User
     * @return View Venues
     */
    public function venues()
    {
        $venues = User::find(Sentinel::getUser()->id)->venues()->get();
    	return view('admin.venues')->with(['venues' => $venues]);
    }

    public function updateVenue(Request $request, $id)
    {
        $venue = Venue::find($id);
        $venue->venue_name = $request->venue_name;
        $venue->address_one = $request->address_one;
        $venue->address_two = $request->address_two;
        $venue->city = $request->city;
        $venue->county = $request->county;
        $venue->phone = $request->phone;
        $venue->website = $request->website;
        $venue->post_code = $request->post_code;
        $venue->will_receive_agm = $request->has('will_receive_agm');
        $venue->will_receive_breakfast_meeting = $request->has('will_receive_breakfast_meeting');
        $venue->will_receive_conference = $request->has('will_receive_conference');
        $venue->will_receive_christmas_party = $request->has('will_receive_christmas_party');
        $venue->will_receive_corporate_hospitality = $request->has('will_receive_corporate_hospitality');
        $venue->will_receive_interviews = $request->has('will_receive_interviews');
        $venue->will_receive_meeting = $request->has('will_receive_meeting');
        $venue->will_receive_product_launch = $request->has('will_receive_product_launch');
        $venue->will_receive_seminar = $request->has('will_receive_seminar');
        $venue->will_receive_sit_down_dinner = $request->has('will_receive_sit_down_dinner');
        $venue->will_receive_stand_up_reception = $request->has('will_receive_stand_up_reception');
        $venue->will_receive_team_meeting = $request->has('will_receive_team_meeting');
        $venue->will_receive_training_course = $request->has('will_receive_training_course');
        $venue->will_receive_anniversary = $request->has('will_receive_anniversary');
        $venue->will_receive_barmitzvah = $request->has('will_receive_barmitzvah');
        $venue->will_receive_baptism = $request->has('will_receive_baptism');
        $venue->will_receive_birthday = $request->has('will_receive_birthday');
        $venue->will_receive_christmas = $request->has('will_receive_christmas');
        $venue->will_receive_engagement = $request->has('will_receive_engagement');
        $venue->will_receive_hen_stag = $request->has('will_receive_hen_stag');
        $venue->will_receive_retirement = $request->has('will_receive_retirement');
        $venue->will_receive_civil_ceremony = $request->has('will_receive_civil_ceremony');
        $venue->will_receive_ethnic = $request->has('will_receive_ethnic');
        $venue->will_receive_reception_only = $request->has('will_receive_reception_only');
        $venue->will_receive_reception_ceremony = $request->has('will_receive_reception_ceremony');
        $venue->save();

        flash_success('Success!', "Venue successfully updated.");

        return back();

    }
}
