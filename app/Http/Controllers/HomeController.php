<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Contact;

class HomeController extends Controller
{
    
	/**
     * Get a list of event types
     * Serve the Home page
     * @return view
     */
    public function index()
    {

    	return view('home', ['events' => [
            [   'ev_name' => 'BUSINESS',
                'ev_src' => '/images/business-01.jpg',
                'ev_subcat'=>[
                    ['name'=>'AGM','id' => '1','src'=>'/images/agm.jpg'],                    
                    ['name'=>'Breakfast Meetings','id' => '2','src'=>'/images/breakfast-meeting.jpg'],
                    ['name'=>'Conference','id' => '3','src'=>'/images/business-sub-4.jpg'],
                    ['name'=>'Christmas Party','id' => '4','src'=>'/images/business-xmas-party.jpg'],
                    ['name'=>'Corporate Hospitality','id' => '5','src'=>'/images/corporate-hospitality.jpg'], 
                    ['name'=>'Interviews','id' => '6','src'=>'/images/job-interview.jpg'], 
                    ['name'=>'Meeting','id' => '7','src'=>'/images/business-sub-5-meeting.jpg'], 
                    ['name'=>'Product Launch','id' => '8','src'=>'/images/business-sub-6-product-launch.jpg'], 
                    ['name'=>'Seminar','id' => '9','src'=>'/images/seminar.jpg'], 
                    ['name'=>'Sit-Down Dinner','id' => '10','src'=>'/images/sit-down-dinner.jpg'], 
                    ['name'=>'Stand-Up Reception','id' => '11','src'=>'/images/stand-up-reception.jpg'], 
                    ['name'=>'Training Course','id' => '13','src'=>'/images/training-course.jpg'],                       
                ],
            ],
            [   'ev_name' => 'PARTY',
                'ev_src' => '/images/party-01.jpg',
                'ev_subcat'=>[
                    ['name'=>'Anniversary','id' => '14', 'src'=>'/images/party-sub-15-Engagement.jpg'],
                    ['name'=>'Bar/Bat Mitzvah','id' => '15', 'src'=>'/images/party-sub-12-Bar-Bat-Mitzvah.jpg'],
                    ['name'=>'Baptism/Christening','id' => '16', 'src'=>'/images/Baptism-christening.jpg'],
                    ['name'=>'Birthday','id' => '17', 'src'=>'/images/party-sub-13-Birthday.jpg'],
                    ['name'=>'Christmas','id' => '18', 'src'=>'/images/party-sub-14-Christmas.jpg'],
                    ['name'=>'Engagement','id' => '19', 'src'=>'/images/party-sub-15-Engagement.jpg'],
                    ['name'=>'Hen/Stag','id' => '20', 'src'=>'/images/hen-stag-party.jpg'],
                    ['name'=>'Retirement','id' => '21', 'src'=>'/images/retirement.jpg'],
                    ['name'=>'Other','id' => '22', 'src'=>'/images/party-sub-16.jpg'],
                ]
            ],
            [   'ev_name' => 'WEDDING',
                'ev_src' => '/images/wedding-01.jpg',
                'ev_subcat'=>[
                ['name'=>'Civil Ceremony','id' => '23', 'src'=>'/images/civil-ceremony.jpg'],
                ['name'=>'Ethnic','id' => '24', 'src'=>'/images/ethnic-wedding.jpg'],
                ['name'=>'Reception Only','id' => '25', 'src'=>'/images/business-sub-6-product-launch.jpg'],
                ['name'=>'Reception + Ceremony','id' => '26', 'src'=>'/images/wedding-sub-9.jpg'],
                ]
            ],

        ]]);
    }

    /**
     * Serve the About page
     * @return view
     */
    public function about()
    {
    	return view('about');
    }

    /**
     * Serve the Faq page
     * @return view
     */
    public function faqs()
    {
    	return view('faqs');
    }

    /**
     * Serve the Contact page
     * @return view
     */
    public function contact()
    {
    	return view('contact');
    }

    /**
     * Validate the Submit form, fire the contact event and save to database
     * @return [type] [description]
     */
    public function contactsubmit()
    {
    	return view('contact');
    }
}
