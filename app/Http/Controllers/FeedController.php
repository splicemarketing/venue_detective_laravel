<?php

namespace App\Http\Controllers;

use App\Models\Feed;
use App\Models\Lead;

class FeedController extends Controller
{
    public function index()
    {
    	// Add some time constraints to this
    	//  i.e only leads within next month, none from past
    	$data['leads'] = Feed::get();

    	return response()
    				->view('feed', $data, 200)
    				->header('Content-Type', 'application/atom+xml; charset=UTF-8');
    }

    public function getLeadLandingPage($id)
    {
    	$lead = Lead::findOrFail($id);
    	return view('enquiry_landing')->with(['lead' => $lead]);
    }
}
