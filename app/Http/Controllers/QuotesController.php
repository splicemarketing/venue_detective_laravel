<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Quote;
use App\Queries\QuoteFilters;
use App\Quoting\AddLeadBuyersCount;
use App\Quoting\CreditCheck;
use App\Quoting\CreateNewQuote;
use App\Quoting\SendQuote;
use App\Quoting\UpdateQuote;

class QuotesController extends Controller
{
    
	/**
	 * Purchasing a Lead
	 */
    public function purchase(Request $request)
    { 		
    	$tasks = [
    		CreditCheck::class,
    		CreateNewQuote::class,
    		AddLeadBuyersCount::class
    	];

    	try { 

            foreach ($tasks as $task) {
        		(new $task)->handle($request);
        	}

    	   flash_success("Success!", "You have bought the lead!");
    	   return redirect()->route('my_leads');
        }
        catch (\App\Exceptions\NotEnoughCredits $e) {
            flash_warning_dismiss("Sorry!", "You do not have enough credits! Please purchase more to continue.");
            return redirect()->back();
        }
    }

    /**
     * Return Quotes for User, filtered by Venue and Quote status
     * @param  Request $request [description]
     * @return View  Quotes
     */
    public function getQuotes(QuoteFilters $quotefilters)
    {
    	$quotes = Quote::with('lead', 'lead.dates')->filter($quotefilters)->get();
    	
    	return view('admin.quotes')->with(['quotes' => $quotes]);
    }

    
    /**
     * Retrieve a single Quote.
     * @param  $id Quote ID
     * @return View Quote
     */
    public function singleQuote($id)
    {
        $quote = Quote::find($id);

        return view('admin.quote')->with(['quote' => $quote]);
    }

    
    public function sendQuote(Request $request)
    {
        $tasks = [
            UpdateQuote::class,
            SendQuote::class
        ];
        
        foreach ($tasks as $task) {
            (new $task)->handle($request);
        }

        flash_success("Great!", "Your Quote has been Sent!");

        return redirect()->route('quote');       

    }
}
