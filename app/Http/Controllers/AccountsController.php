<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Login;
use App\Models\User;
use Activation;
use Sentinel;

class AccountsController extends Controller
{
    
    /**
     * 
     * @return the login page
     */
    public function login()
    {
        return view('login');
    }

    /**
     * Authenticate Our User
     * @param  Login  $login Request Object
     * @return [type]        [description]
     */
    public function auth(Login $login)
    {
        $credentials = [
            'email' => $login['email'],
            'password' => $login['password']
        ];

        try {
            
            if (Sentinel::authenticateAndRemember($credentials)) {
                
                $user = Sentinel::findByCredentials($credentials);
            
                Sentinel::loginAndRemember($user);                   
            
                $admin = Sentinel::findRoleByName('Admins');
                $venue = Sentinel::findRoleByName('Venues');
                $users = Sentinel::findRoleByName('Users');
        
                if ($user->inRole($venue)) {
                    return redirect()->route('venue_admin');
                }

                if ($user->inRole($admin)) {
                    return redirect()->route('vendec_admin');
                }
                
                if ($user->inRole($users)) {
                    return redirect()->route('user_admin');
                }
                
            }

            return redirect()->back()->withInput()->withErrorMessage('Invalid credentials provided');
        } 
        
        catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {
            flash_success_dismiss('Warning!', "Please check your inbox to activate your account.");
            return redirect()->back(); 
        } 

        catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {
            return redirect()->back()->withInput()->withErrorMessage($e->getMessage());
        }
    }

    public function logout()
    {
        Sentinel::logout();
        
        return redirect('/');
    }

    /**
     * Return the Subscription details for A venue user
     * @return [type] [description]
     */
    public function account()
    {
    	return view('admin.account');
    }

    /**
     * Return Reports Charts for the Venue User
     * @return View Reports
     */
    public function reports()
    {
    	return view('admin.reports');
    }


    /**
     * The Profile Page
     * @return [type] [description]
     */
    public function profile()
    {
        $profile = User::find(Sentinel::getUser()->id);
    	return view('admin.profile')->with(['profile' => $profile]);
    }

    /**
     * [updateProfile description]
     * @return [type] [description]
     */
    public function updateProfile(Request $request)
    {
        $profile = User::find(Sentinel::getUser()->id);
        $profile->update($request->all());

        flash_success("Success.", "Your Profile has been updated.");
        return redirect()->back();
    }


}
