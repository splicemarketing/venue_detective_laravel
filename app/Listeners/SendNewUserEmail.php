<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mailers\UserMailer;

class SendNewUserEmail
{    

    protected $usermailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserMailer $usermailer)
    {
        $this->usermailer = $usermailer;
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $this->usermailer->newUserWelcome($event->user);
    }
}
