<?php

namespace App\Listeners;

use App\Events\LeadRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mailers\UserMailer;

class SendNewLeadEmail
{

    protected $usermailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserMailer $usermailer)
    {
        $this->usermailer = $usermailer;
    }

    /**
     * Handle the event
     * @param  LeadRegistered  $event
     * @return void
     */
    public function handle(LeadRegistered $event)
    {
        $this->usermailer->newLeadRegistered($event->lead);
    }



}
