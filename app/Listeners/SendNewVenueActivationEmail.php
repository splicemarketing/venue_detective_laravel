<?php

namespace App\Listeners;

use App\Events\VenueRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mailers\UserMailer;

class SendNewVenueActivationEmail
{
    
    protected $usermailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserMailer $usermailer)
    {
        $this->usermailer = $usermailer;
    }

    /**
     * Handle the event.
     *
     * @param  VenueRegistered  $event
     * @return void
     */
    public function handle(VenueRegistered $event)
    {
        $this->usermailer->newVenueRegistered($event->activation);
    }
}
