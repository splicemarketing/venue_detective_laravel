<?php

namespace App\Listeners;

use App\Events\LeadRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Feed;

class PostLeadToFeed
{
    
    protected $feed;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Feed $feed)
    {
        $this->feed = $feed;
    }

    /**
     * Handle the event.
     *
     * @param  LeadRegistered  $event
     * @return void
     */
    public function handle(LeadRegistered $event)
    {
        $this->feed['title'] = "New lead in #".$event->lead['location'];
        $this->feed['link'] = "https://www.venuedetective.co.uk/feed/".$event->lead['id'];
        $this->feed['description'] = "Venue required for a ".$event->lead->eventType->name." with ".$event->lead['number_of_people']." people within ".$event->lead['radius']." miles of #".$event->lead['location'];
        $this->feed->save();
    }
}
