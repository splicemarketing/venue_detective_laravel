<?php

namespace App\Quoting;

use App\Models\Quote;
use App\Mailers\UserMailer;
use App\Models\Lead;

class SendQuote implements QuotingContract {

	public function handle($request)
	{
		$quote = Quote::find($request['quote_id']);

		$lead = $quote->lead()->first();

		$user = Lead::find($lead->id)->user()->first();

		(new UserMailer)->newQuoteSent($quote, $user);
	}
}