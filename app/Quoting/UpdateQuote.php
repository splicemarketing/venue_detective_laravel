<?php

namespace App\Quoting;

use App\Models\Quote;

class UpdateQuote implements QuotingContract {
	
	public function handle($request)
	{
		$quote = Quote::find($request['quote_id']);
		$quote['min_attendance'] = $request['min_attendance'];
		$quote['max_attendance'] = $request['max_attendance'];
		$quote['disabled_access'] = $request['disabled_access'];
		$quote['included_in_cost'] = $request['included_in_cost'];
		$quote['extras'] = $request['extras'];
		$quote['price_breakdown'] = $request['price_breakdown'];
		$quote['plug'] = $request['plug'];
		$quote['notes'] = $request['notes'];
		$quote['total_cost'] = $request['total_cost'];
		$quote['awaiting_quote'] = 0;
		$quote['quoted'] = 1;
		$quote->save();

	}
}