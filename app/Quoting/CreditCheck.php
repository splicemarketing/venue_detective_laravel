<?php

namespace App\Quoting;

use App\Models\Lead;
use App\Models\User;
use Sentinel;

class CreditCheck implements QuotingContract {
	
	/**
	 * Check if user has enough credits
	 */
	public function handle($request)
	{
		$user = User::find(Sentinel::getUser()->id);
		$creditBalance = $user->account_balance;
		
		$lead = Lead::findOrFail($request->lead_id);
		$leadcost = $lead->lead_credit_price;
		
		if ($creditBalance < $leadcost) {
			throw new \App\Exceptions\NotEnoughCredits;
		}
		else {
			$user->account_balance = ($creditBalance - $leadcost);
			$user->save();
		}			
	}
}