<?php

namespace App\Quoting;
use App\Models\Quote;
use Sentinel;

class CreateNewQuote implements QuotingContract {
	
	/**
	 * Associate the lead with the users quotes
	 * @return Void
	 */
	public function handle($request)
	{
		$quote = new Quote;
		$quote->lead_id = $request->lead_id;
		$quote->user_id = Sentinel::getUser()->id;
		$quote->venue_quoted_id = $request->venue_id;
		$quote->save();
	}
}