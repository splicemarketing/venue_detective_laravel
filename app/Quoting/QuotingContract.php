<?php

namespace App\Quoting;

interface QuotingContract {

	public function handle($request);

}