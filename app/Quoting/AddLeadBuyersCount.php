<?php

namespace App\Quoting;

use App\Models\Lead;

class AddLeadBuyersCount implements QuotingContract {
	
	/**
	 * Add to number of buyers count on lead
	 * @return [type] [description]
	 */
	public function handle($request)
	{
		$lead = Lead::find($request->lead_id);
		$lead->number_of_quotes = $lead->number_of_quotes - 1;
		$lead->save(); 
	}
}