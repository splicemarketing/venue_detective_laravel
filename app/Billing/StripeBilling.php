<?php

namespace App\Billing;

use Stripe\Stripe;
use Stripe\Charge;

class StripeBilling implements BillingInterface {

	public function __construct()
	{
		Stripe::setApiKey(config('stripe.secret_key'));
	}

	public function charge(array $data, $amount)
	{
		try 
		{
			return Charge::create([
				'amount' =>	$amount,
				'currency' => 'gbp',
				'description' => $data['email'],
				'card'	=> $data['stripeToken']
			]);	
		}
		/**
		 * Card was declined
		 */
		catch (\Stripe\Error\Card $e)
		{
			dd('Card was declined');
		}
		/**
		 * Too many requests made to the API too quickly
		 */
		catch (\Stripe\Error\RateLimit $e)
		{
			dd('Rate Limit Error');
		}
		/**
		 * Invalid Parameters were supplied to Stripe's API
		 */
		catch (\Stripe\Error\InvalidRequest $e)
		{
			dd('Invalid Parameters were supplied to Stripes API');
		}
		/**
		 * Authentication with Stripe's API failed
		 */
		catch (\Stripe\Error\Authentication $e)
		{
			dd('Stripe Authentication failed');
		}
		/**
		 * Network communication with Stripe failed
		 */
		catch (\Stripe\Error\ApiConnection $e)
		{
			dd('Stripe API Connection Error');
		}
		/**
		 * Display a very generic error to the user
		 * Send Venue Detective Admin an Email
		 */
		catch (\Stripe\Error\Base $e)
		{
			dd("Generic Stripe Error");
		}
		/**
		 * Something else happened, unrelated to Stripe
		 */
		catch (Exception $e)
		{
			dd("General Exception");
		}
		
	}
}