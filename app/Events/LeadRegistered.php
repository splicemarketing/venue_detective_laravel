<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use App\Models\Lead;

class LeadRegistered extends Event
{
    
    use SerializesModels;

    /**
     * @var Lead
     */
    public $lead;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }
    
}
