<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class VenueRegistered extends Event
{
    use SerializesModels;

    
    /**
    * User Activation Code
    * @var 
    */
    public $activation;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($activation)
    {
        $this->activation = $activation;
    }

}
