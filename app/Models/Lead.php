<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Events\LeadRegistered;
use App\Traits\LeadRange;
use App\Traits\LeadCalculate;
use App\Traits\LeadEventTypePreference;
use App\Models\User;
use App\Models\Venue;
use Sentinel;

class Lead extends Model
{

    use LeadRange;
    use LeadCalculate;
    use LeadEventTypePreference;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                'customer_id', 
                'event_type', 
                'number_of_people',
                'longitude',
                'latitude',
                'location',
                'radius',
                'level_of_venue',
                'accomodation_required',
                'disabled_access_required', 
                'wireless_internet_required',
                'kids_facilities_requried',
                'is_food_not_required', 
                'sit_down_meal',
                'external_catering',
                'finger_buffet', 
                'canapes', 
                'av_facilities_required', 
                'av_facility_lecturn', 
                'av_facility_projector_screen', 
                'av_facility_microphone',
                'av_facility_autocue', 
                'av_facility_pa_system', 
                'av_facility_video_recording',
                'venue_type_pubs_restaurants',
                'venue_type_hotels',
                'venue_type_unusual',
                'venue_type_church',
                'venue_type_town_hall',
                'venue_type_garden',
                'venue_type_stately_home',
                'venue_type_sporting_leisure',
                'venue_type_dedicated',
                'venue_type_barn',
                'venue_type_marquee',
                'venue_type_historic',
                'venue_type_castle',
                'venue_type_nightclub',     
                'is_drinks_not_required', 
                'soft_drinks_only',
                'tea_coffee',
                'paid_bar', 
                'lead_credit_price',
                'lead_cash_price',
                'budget', 
    ];


    public static function register($enquiry)
    {
    	$lead = static::create($enquiry);

    	event(new LeadRegistered($lead));

        return $lead;
    }

    /******************************
     * Relationships
     ****************************/

    /**
     * A lead belongs to a single user
     */    
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'customer_id');
    }

    /**
     * A Lead can be quoted multiple Times
     * @return [type] [description]
     */
    public function quotes()
    {
        return $this->hasMany('App\Models\Quote');
    }

    public function dates()
    {
        return $this->hasMany('App\Models\Date');
    }

    public function eventType()
    {
        return $this->belongsTo('App\Models\EventType', 'event_type');
    }

   /**************************
    * Accessors & Mutators
    **************************/


    /**
     * Calculate Lead price by budget
     */
    public function setLeadCreditPriceAttribute($budget)
    {
        $this->attributes['lead_credit_price'] = $this->calculateLeadPrice($budget);
    }

    public function setLeadCashPriceAttribute($budget)
    {
        $this->attributes['lead_cash_price'] = $budget * 0.02;
    }

    public function getDisabledAccessRequiredAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getcanapesAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getfingerBuffetAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getexternalCateringAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getsitDownMealAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getkidsFacilitiesRequriedAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getpaidBarAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getAccomodationRequiredAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getWirelessInternetRequiredAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getAvFacilitiesRequiredAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypePubsRestaurantsAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeHotelsAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeUnusualAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeChurchAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeTownHallAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeGardenAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeStatelyHomeAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeSportingLeisureAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeDedicatedAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeBarnAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeMarqueeAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeHistoricAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeCastleAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getvenueTypeNightclubAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    public function getIsDrinksNotRequiredAttribute($value)
    {
        switch ($value) {
            case '0':
                return "Yes";
                break;
            case '1':
                return "No";
            default:
                return "No";
                break;
        }
    }

    public function getIsFoodNotRequiredAttribute($value)
    {
        switch ($value) {
            case '0':
                return "Yes";
                break;
            case '1':
                return "No";
            default:
                return "No";
                break;
        }
    }


    /**********************************
     * Scopes
     **********************************/
    
    /**
     * Find all leads within range of the users
     * Venues, by the Lead Longitude & Latitude,
     * the Users' Venues Longitude & Latitude,
     * and the Lead radius. 
     * @return Leads
     */
    public function scopeLeadsForVenues()
    { 
        $allLeads = static::liveLeads();
        $venues = User::find(Sentinel::getUser()->id)->venues()->get();
        $leadsForVenue = array();

        foreach ($venues as $venue)
        {
            foreach ($allLeads as $lead) 
            {
                if( $this->checkVenueLeadPreferences($venue, $lead) )
                {
                    if ( $this->isVenueWithinRangeOfLead($venue, $lead) ) {
                        $lead['venueInRange'] = $venue['venue_name'];
                        $lead['venue_id'] = $venue['id'];
                        $leadsForVenue[] = $lead;
                    }
                }                
            }
        }                   

        return $leadsForVenue;
    }

    /**
     * Get Number of Leads for A User
     * @return Int Count
     */
    public function scopeLeadsForUserCount()
    {
        $allLeads = static::liveLeads();
        $venues = User::find(Sentinel::getUser()->id)->venues()->get();

        $count = 0;

        foreach ($venues as $venue)
        {
            foreach ($allLeads as $lead) 
            {
                if ( $this->isVenueWithinRangeOfLead($venue, $lead) ) {
                    $count++;
                }                               
            }
        }                   
        return $count;
    }

    /**
     * Find all leads within range of a single venue
     * @param  [type] $query Query Object
     * @param  [type] $id    Id of Venue
     * @return array
     */ 
    public function scopeLeadsForVenue($query, $id)
    {
        $allLeads = static::liveLeads();
        $venue = User::find(Sentinel::getUser()->id)->venues()->where('id', '=', $id)->first();
        $leadsForVenue = array();
        
        foreach ($allLeads as $lead) 
        {
            if( $this->checkVenueLeadPreferences($venue, $lead) ) {
                if ( $this->isVenueWithinRangeOfLead($venue, $lead) ) {
                    $lead['venueInRange'] = $venue['venue_name'];
                    $lead['venue_id'] = $venue['id'];
                    $leadsForVenue[] = $lead;
                }
            }                
        }

        return $leadsForVenue;
    }


    public function scopeLiveLeads()
    {
        $liveLeads = static::where('completed', 0)
                            ->where('number_of_quotes', '>', 0)
                            ->get();
        return $liveLeads;
    }

    public function scopeLiveLeadsCount()
    {
        $count = static::where('completed', 0)
                        ->where('number_of_quotes', '>', 0)
                        ->count();   
        return $count;
        
    }

}
