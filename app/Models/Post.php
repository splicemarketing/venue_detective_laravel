<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

	public function scopeBySlug($query, $slug)
	{
		return $query->whereSlug($slug)->first();
	}
	
}