<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    public function leads()
    {
    	return $this->hasMany('App\Models\Lead', 'event_type');
    }
}
