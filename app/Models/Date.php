<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{

	protected $fillable = [
		'lead_id',
		'date',
		'start',
		'finish',
	];
    
    public function lead()
    {
    	return $this->belongsTo('App\Models\Lead');
    }



}