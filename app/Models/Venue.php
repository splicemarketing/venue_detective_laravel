<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'venue_name',
    	'address_one',
    	'address_two',
    	'city',
    	'county',
        'phone',
        'website',
    	'post_code',
        'longitude',
        'latitude',
        'will_receive_agm',
        'will_receive_breakfast_meeting',
        'will_receive_conference',
        'will_receive_christmas_party',
        'will_receive_corporate_hospitality',
        'will_receive_interviews',
        'will_receive_meeting',
        'will_receive_product_launch',
        'will_receive_seminar',
        'will_receive_sit_down_dinner',
        'will_receive_stand_up_reception',
        'will_receive_team_meeting',
        'will_receive_training_course',
        'will_receive_anniversary',
        'will_receive_barmitzvah',
        'will_receive_baptism',
        'will_receive_birthday',
        'will_receive_christmas',
        'will_receive_engagement',
        'will_receive_hen_stag',
        'will_receive_retirement',
        'will_receive_civil_ceremony',
        'will_receive_ethnic',
        'will_receive_reception_only',
        'will_receive_reception_ceremony',
    ];

    public static function register($venue)
    {
    	$newVenue = static::create($venue);

    	return $newVenue;
    }

    /**
     * Relationships
     */

    /**
     * A Venue Can have multiple Staff
     */
    
    public function staff()
    {
    	return $this->belongsToMany('App\Models\User');
    }


}
