<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Filterable;

class Quote extends Model
{
    
    use Filterable;

	
    /*****************************
     * Relationships
     ******************************/

	/**
	 * A quote is for a specific lead
	 * @return [type] [description]
	 */
    public function lead()
    {
    	return $this->belongsTo('App\Models\Lead', 'lead_id');
    }


    /********************************
     * Accessors & Mutators
     */

    public function getDisabledAccessAttribute($value)
    {
        switch ($value) {
            case '0':
                return "No";
                break;
            case '1':
                return "Yes";
            default:
                return "No";
                break;
        }
    }

    /*******************************
     * Scopes
     *******************************/

    public function scopeLeadsAwaitingQuotesForUser($query, $id)
    {
        return $query->where('user_id', $id)
                     ->where('awaiting_quote', 1);
                        
    }

    public function scopeLeadsQuotedForUser($query, $id)
    {
        return $query->where('user_id', $id)
                     ->where('quoted', 1);
    }
    

}
