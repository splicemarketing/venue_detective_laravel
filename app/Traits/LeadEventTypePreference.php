<?php

namespace App\Traits;

trait LeadEventTypePreference
{
	/**
	 * 
	 */
	public function checkVenueLeadPreferences($venue, $lead)
	{
		switch ($lead->eventType->id) {
			case '1':
				if($venue->will_receive_agm){return true;}
				return false;
				break;
			case '2':
				if($venue->will_receive_breakfast_meeting){return true;}
				return false;
				break;
			case '3':
				if($venue->will_receive_conference){return true;}
				return false;
				break;
			case '4':
				if($venue->will_receive_christmas_party){return true;}
				return false;
				break;
			case '5':
				if($venue->will_receive_corporate_hospitality){return true;}
				return false;
				break;
			case '6':
				if($venue->will_receive_interviews){return true;}
				return false;
				break;
			case '7':
				if($venue->will_receive_meeting){return true;}
				return false;
				break;
			case '8':
				if($venue->will_receive_product_launch){return true;}
				return false;
				break;
			case '9':
				if($venue->will_receive_seminar){return true;}
				return false;
				break;
			case '10':
				if($venue->will_receive_sit_down_dinner){return true;}
				return false;
				break;
			case '11':
				if($venue->will_receive_stand_up_reception){return true;}
				return false;
				break;
			case '12':
				if($venue->will_receive_team_meeting){return true;}
				return false;
				break;
			case '13':
				if($venue->will_receive_training_course){return true;}
				return false;
				break;
			case '14':
				if($venue->will_receive_anniversary){return true;}
				return false;
				break;
			case '15':
				if($venue->will_receive_barmitzvah){return true;}
				return false;
				break;
			case '16':
				if($venue->will_receive_baptism){return true;}
				return false;
				break;
			case '17':
				if($venue->will_receive_birthday){return true;}
				return false;
				break;
			case '18':
				if($venue->will_receive_christmas){return true;}
				return false;
				break;
			case '19':
				if($venue->will_receive_engagement){return true;}
				return false;
				break;
			case '20':
				if($venue->will_receive_hen_stag){return true;}
				return false;
				break;
			case '21':
				if($venue->will_receive_retirement){return true;}
				return false;
				break;
			case '23':
				if($venue->will_receive_civil_ceremony){return true;}
				return false;
				break;
			case '24':
				if($venue->will_receive_ethnic){return true;}
				return false;
				break;
			case '25':
				if($venue->will_receive_reception_only){return true;}
				return false;
				break;
			case '26':
				if($venue->will_receive_reception_ceremony){return true;}
				return false;
				break;			
			default:
				return true;
				break;
		}
	}



}