<?php

namespace App\Traits;

trait LeadRange
{
	
	/**
	 * Uses Pythagoras to calculate if the two points are within range
	 * Assumes a flat earth, margin of error within range given the distances wont be huge.
	 * @param  Venue  $venue 
	 * @param  Lead   $lead  
	 * @return boolean   returns true if points are within range.
	 */
	public function isVenueWithinRangeOfLead($venue, $lead)
	{
		if ( pow((($venue['latitude']) - ($lead['latitude'])), 2) + pow((($venue['longitude']) - ($lead['longitude'])), 2) <= pow($lead['radius'], 2) ) {
		 	return true;
		}

	}

}