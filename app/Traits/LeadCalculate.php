<?php

namespace App\Traits;

trait LeadCalculate
{
	
	 /**
     * Calculate Lead Price
     * @param  Integer $budget Lead Budget
     * @return Integer Lead Price
     */
    public function calculateLeadPrice($budget)
    {
        $leadPrice = 1;
        
        if ($budget < 2500) {
            $leadPrice = 1;
            return $leadPrice;
        }

        if ($budget < 7000 ) {
            $leadPrice = 2;
            return $leadPrice;
        }

        if ($budget > 7000) {
            $leadPrice = 3;
            return $leadPrice;
        }
    }

}