<?php

namespace App\Traits;
use App\Queries\QueryFilters;

trait Filterable
{
	public function scopeFilter($query, QueryFilters $queryfilters)
	{
		return $queryfilters->apply($query);
	}
}