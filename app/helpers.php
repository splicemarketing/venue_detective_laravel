<?php

function flash_success($title, $message)
{
	$flash = app('App\Http\Flash');

	return $flash->success_message($title, $message);
}

function flash_success_dismiss($title, $message)
{
	$flash = app('App\Http\Flash');

	return $flash->success_message_dismiss($title, $message);
}

function flash_warning_dismiss($title, $message)
{
	$flash = app('App\Http\Flash');

	return $flash->warning_message_dismiss($title, $message);
}

function flash_info_dismiss($title, $message)
{
	$flash = app('App\Http\Flash');

	return $flash->info_message_dismiss($title, $message);
}

function flash_warning($title, $message)
{
	$flash = app('App\Http\Flash');

	return $flash->warning_message($title, $message);
}