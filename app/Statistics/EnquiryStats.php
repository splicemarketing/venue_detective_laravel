<?php

namespace App\Statistics;

use App\Models\Lead;
use App\Models\Quote;
use Sentinel;

class EnquiryStats
{	

	public static function enquirySummary()
	{
		$id = Sentinel::getUser()->id;
		return [
            'total' => Lead::liveLeadsCount(),
            'leadsforuser' => Lead::leadsForUserCount(),
            'leadstoquote' => Quote::leadsAwaitingQuotesForUser($id)->count(),
            'leadsquoted'  => Quote::leadsQuotedForUser($id)->count(),
        ];
	}
}