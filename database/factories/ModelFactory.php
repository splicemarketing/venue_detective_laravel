<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' =>$faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
    ];
});

$factory->define(App\Models\Lead::class, function(Faker\Generator $faker) {
	return [
		'customer_id' => factory(App\Models\User::class)->create()->id,
        'event_type' => $faker->numberBetween(1,26),
        'number_of_people' => $faker->numberBetween(1,30),
        'longitude' => $faker->longitude(-2,2),
        'latitude' => $faker->latitude(49,51),
        'radius' => $faker->numberBetween(2,15),
        'location'  => $faker->city,
        'accomodation_required' => $faker->boolean,
        'disabled_access_required' => $faker->boolean,
        'wireless_internet_required' => $faker->boolean,
        'kids_facilities_requried' => $faker->boolean,
        'is_food_not_required' => $faker->boolean,
        'sit_down_meal' => $faker->boolean,
        'external_catering' => $faker->boolean,
        'finger_buffet' => $faker->boolean,
        'canapes' => $faker->boolean,
        'av_facilities_required' => $faker->boolean,
        'av_facility_lecturn' => $faker->boolean,
        'av_facility_projector_screen' => $faker->boolean,
        'av_facility_microphone' => $faker->boolean,
        'av_facility_autocue' => $faker->boolean,
        'av_facility_pa_system' => $faker->boolean,
        'av_facility_video_recording' => $faker->boolean,
        'is_drinks_not_required' => $faker->boolean,
        'soft_drinks_only' => $faker->boolean,
        'tea_coffee'=> $faker->boolean,
        'paid_bar' => $faker->boolean,
        'lead_credit_price' => $faker->numberBetween(10,200),
        'lead_cash_price' => $faker->numberBetween(20,2000),
        'budget' => $faker->numberBetween(100,10000),
	];
});

$factory->define(App\Models\EventType::class, function(Faker\Generator $faker) {
       return [
            'parent_id' => 0,
            'name' => $faker->word,
            'image_link' => $faker->imageUrl(300,300,'cats'),
       ];
});

$factory->define(App\Models\Date::class, function(Faker\Generator $faker) {
    return [
        'lead_id' => $faker->numberBetween(1,15),
        'date'  => $faker->dateTimeThisMonth($max = 'now'),
        'start' =>  $faker->dateTimeThisMonth($max = 'now'),
        'finish'   =>  $faker->dateTimeThisMonth($max = 'now'),
    ];
});

$factory->define(App\Models\Venue::class, function(Faker\Generator $faker) {
        return [
            'venue_name' => $faker->name,
            'address_one' => $faker->streetName,
            'address_two' => $faker->streetName,
            'city' => $faker->city,
            'county' => $faker->city,
            'post_code' => $faker->postcode,
            'phone' => $faker->phoneNumber,
            'website' => $faker->domainName,
            'longitude' => $faker->longitude(-2,2),
            'latitude' => $faker->latitude(49,52),
            'isComplete' => $faker->boolean,
            'will_receive_agm' => $faker->boolean,
            'will_receive_breakfast_meeting' => $faker->boolean,
            'will_receive_conference' => $faker->boolean,
            'will_receive_christmas_party' => $faker->boolean,
            'will_receive_corporate_hospitality' => $faker->boolean,
            'will_receive_interviews' => $faker->boolean,
            'will_receive_meeting' => $faker->boolean,
            'will_receive_product_launch' => $faker->boolean,
            'will_receive_seminar' => $faker->boolean,
            'will_receive_sit_down_dinner' => $faker->boolean,
            'will_receive_stand_up_reception' => $faker->boolean,
            'will_receive_team_meeting' => $faker->boolean,
            'will_receive_training_course' => $faker->boolean,
            'will_receive_anniversary' => $faker->boolean,
            'will_receive_barmitzvah' => $faker->boolean,
            'will_receive_baptism' => $faker->boolean,
            'will_receive_birthday' => $faker->boolean,
            'will_receive_christmas' => $faker->boolean,
            'will_receive_engagement' => $faker->boolean,
            'will_receive_hen_stag' => $faker->boolean,
            'will_receive_retirement' => $faker->boolean,
            'will_receive_civil_ceremony' => $faker->boolean,
            'will_receive_ethnic' => $faker->boolean,
            'will_receive_reception_only' => $faker->boolean,
            'will_receive_reception_ceremony' => $faker->boolean,
        ];
});
