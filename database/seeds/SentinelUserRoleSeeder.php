<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class SentinelUserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_users')->delete();
        
        $ben = Sentinel::findByCredentials(['login' => 'ben@venuedetective.co.uk']);
        $xan = Sentinel::findByCredentials(['login' => 'xan@venuedetective.co.uk']);
        $ray = Sentinel::findByCredentials(['login' => 'ray@venuedetective.co.uk']);
        $venueTest = Sentinel::findByCredentials(['login' => 'venue@venuedetective.co.uk']);
        $userTest = Sentinel::findByCredentials(['login' => 'user@venuedetective.co.uk']);
        
        $adminRole = Sentinel::findRoleByName('Admins');        
        $venueRole = Sentinel::findRoleByName('Venues');
        $userRole = Sentinel::findRoleByName('Users');
       
        // Assign the roles to the users
        $adminRole->users()->attach($ben);
        $adminRole->users()->attach($xan);
        $adminRole->users()->attach($ray);
        $venueRole->users()->attach($venueTest);
        $userRole->users()->attach($userTest);
        
        $this->command->info('Users assigned to roles seeded!');
    }
}