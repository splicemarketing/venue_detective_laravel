<?php

use Illuminate\Database\Seeder;
use App\Models\Venue;
use App\Models\User;
use Faker\Provider;

class VenueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Venue::class, 150)->create()->each(function($venue) {
        	$venue->staff()->save(factory(User::class)->make());
        });
    }
}
