<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class SentinelUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        
        Sentinel::registerAndActivate([
            'email'    => 'ben@venuedetective.co.uk',
            'password' => 'hftryekjhdfpoiwerkgasdowygrsad',
            'first_name' => 'Ben',
            'last_name' => 'Jackson',
        ]);
        
        Sentinel::registerAndActivate([
            'email'    => 'xan@venuedetective.co.uk',
            'password' => 'hgdfytewrwkljgherbasdowgqerlhbg',
            'first_name' => 'Xan',
            'last_name' => 'Phillips',
        ]);
        
        Sentinel::registerAndActivate([
            'email'    => 'ray@venuedetective.co.uk',
            'password' => 'etryfhdgrytwerklgsadf82',
            'first_name' => 'Ray',
            'last_name' => 'Vernon',
        ]);

        Sentinel::registerAndActivate([
            'email'    => 'venue@venuedetective.co.uk',
            'password' => 'venue',
            'first_name' => 'Venue',
            'last_name' => 'Admin',
        ]);

        Sentinel::registerAndActivate([
            'email'    => 'user@venuedetective.co.uk',
            'password' => 'user',
            'first_name' => 'User',
            'last_name' => 'Admin',
        ]);
        
        $this->command->info('Users seeded!');
    }
}