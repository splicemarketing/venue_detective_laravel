<?php

use Illuminate\Database\Seeder;
use App\Models\Date;
use Faker\Provider;

class DateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(Date::class, 100)->create();
    }
    
}
