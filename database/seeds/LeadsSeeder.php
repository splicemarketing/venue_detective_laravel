<?php

use Illuminate\Database\Seeder;
use App\Models\Lead;
use Faker\Provider;

class LeadsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Lead::class, 15)->create();
    }
}
