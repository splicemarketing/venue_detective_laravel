<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        $this->call(SentinelRoleSeeder::class);
        $this->call(SentinelUserSeeder::class);
        $this->call(SentinelUserRoleSeeder::class);
        $this->call(EventTypeSeeder::class);
        $this->call(LeadsSeeder::class);
        $this->call(VenueSeeder::class);
        $this->call(DateSeeder::class);
        $this->command->info('All tables seeded!');
        
        Model::reguard();
    }
}
