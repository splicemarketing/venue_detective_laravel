<?php

use Illuminate\Database\Seeder;
use App\Models\EventType;
use Faker\Provider;

class EventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EventType::class, 26)->create();
    }
}
