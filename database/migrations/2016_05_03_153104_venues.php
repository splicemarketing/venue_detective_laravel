<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Venues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('venue_name');
            $table->string('address_one');
            $table->string('address_two');
            $table->string('city');
            $table->string('county');
            $table->string('post_code');
            $table->string('phone');
            $table->string('website');
            $table->decimal('longitude', 10, 8);
            $table->decimal('latitude', 10, 8);
            $table->boolean('isComplete')->default(0);            
            $table->boolean('will_receive_agm')->default(1);            
            $table->boolean('will_receive_breakfast_meeting')->default(1);
            $table->boolean('will_receive_conference')->default(1);
            $table->boolean('will_receive_christmas_party')->default(1);
            $table->boolean('will_receive_corporate_hospitality')->default(1);
            $table->boolean('will_receive_interviews')->default(1);
            $table->boolean('will_receive_meeting')->default(1);
            $table->boolean('will_receive_product_launch')->default(1);
            $table->boolean('will_receive_seminar')->default(1);
            $table->boolean('will_receive_sit_down_dinner')->default(1);
            $table->boolean('will_receive_stand_up_reception')->default(1);
            $table->boolean('will_receive_team_meeting')->default(1);
            $table->boolean('will_receive_training_course')->default(1);
            $table->boolean('will_receive_anniversary')->default(1);
            $table->boolean('will_receive_barmitzvah')->default(1);
            $table->boolean('will_receive_baptism')->default(1);
            $table->boolean('will_receive_birthday')->default(1);
            $table->boolean('will_receive_christmas')->default(1);
            $table->boolean('will_receive_engagement')->default(1);
            $table->boolean('will_receive_hen_stag')->default(1);
            $table->boolean('will_receive_retirement')->default(1);                        
            $table->boolean('will_receive_civil_ceremony')->default(1);
            $table->boolean('will_receive_ethnic')->default(1);
            $table->boolean('will_receive_reception_only')->default(1);
            $table->boolean('will_receive_reception_ceremony')->default(1);            
            $table->timestamps();
        });

        Schema::create('user_venue', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->integer('venue_id')->unsigned()->index();
            $table->timestamps();
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('venues');
        Schema::drop('user_venue');
    }
}
