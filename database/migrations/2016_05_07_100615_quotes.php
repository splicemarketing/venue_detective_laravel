<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Quotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lead_id')->unsigned();
            $table->foreign('lead_id')->references('id')->on('leads');
            $table->integer('venue_quoted_id')->unsigned()->nullable();
            $table->foreign('venue_quoted_id')->references('id')->on('venues');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('min_attendance');
            $table->integer('max_attendance');
            $table->boolean('disabled_access');
            $table->text('included_in_cost')->nullable();
            $table->text('extras')->nullable();
            $table->text('price_breakdown')->nullable();
            $table->text('plug')->nullable();
            $table->text('notes')->nullable();            
            $table->integer('total_cost');            
            $table->boolean('awaiting_quote')->default(1);
            $table->boolean('quoted')->default(0);
            $table->boolean('not_interested')->default(0);                      
            $table->boolean('read');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quotes');
    }
}
