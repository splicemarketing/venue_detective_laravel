<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Leads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('users');
            $table->integer('event_type')->unsigned();
            $table->foreign('event_type')->references('id')->on('event_types');
            $table->integer('number_of_people')->unsigned();
            $table->string('location');
            $table->string('post_code');
            $table->decimal('longitude', 10, 8);
            $table->decimal('latitude', 10, 8);
            $table->boolean('completed')->default(0);
            $table->integer('radius');
            $table->boolean('accomodation_required')->default(0);
            $table->boolean('disabled_access_required')->default(0);
            $table->boolean('wireless_internet_required')->default(0);
            $table->boolean('kids_facilities_requried')->default(0);
            $table->boolean('is_food_not_required')->default(1);
            $table->boolean('sit_down_meal')->default(0);
            $table->boolean('external_catering')->default(0);
            $table->boolean('finger_buffet')->default(0);
            $table->boolean('canapes')->default(0);
            $table->boolean('av_facilities_required')->default(0);
            $table->boolean('av_facility_lecturn')->default(0);
            $table->boolean('av_facility_projector_screen')->default(0);
            $table->boolean('av_facility_microphone')->default(0);
            $table->boolean('av_facility_autocue')->default(0);
            $table->boolean('av_facility_pa_system')->default(0);
            $table->boolean('av_facility_video_recording')->default(0);
            $table->boolean('venue_type_pubs_restaurants')->default(0);
            $table->boolean('venue_type_hotels')->default(0);
            $table->boolean('venue_type_unusual')->default(0);
            $table->boolean('venue_type_church')->default(0);
            $table->boolean('venue_type_town_hall')->default(0);
            $table->boolean('venue_type_garden')->default(0);
            $table->boolean('venue_type_stately_home')->default(0);
            $table->boolean('venue_type_sporting_leisure')->default(0);
            $table->boolean('venue_type_dedicated')->default(0);
            $table->boolean('venue_type_barn')->default(0);
            $table->boolean('venue_type_marquee')->default(0);
            $table->boolean('venue_type_historic')->default(0);
            $table->boolean('venue_type_castle')->default(0);
            $table->boolean('venue_type_nightclub')->default(0);     
            $table->boolean('is_drinks_not_required')->default(1);
            $table->boolean('soft_drinks_only')->default(0);
            $table->boolean('tea_coffee')->default(0);
            $table->boolean('paid_bar')->default(0);
            $table->integer('number_of_quotes')->default(3);
            $table->integer('lead_credit_price');
            $table->integer('lead_cash_price');
            $table->integer('budget');
            $table->timestamps();
        });

        Schema::create('dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lead_id')->references('id')->on('leads');
            $table->timestamp('date');
            $table->timestamp('start');
            $table->timestamp('finish');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leads');
        Schema::drop('dates');
    }

    
}
