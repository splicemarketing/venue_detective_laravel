var elixir = require('laravel-elixir');

elixir(function(mix) {

	mix.copy('node_modules/foundation-sites/dist/foundation.min.js', 'resources/assets/js');
	mix.copy('node_modules/foundation-sites/dist/foundation.min.css', 'resources/assets/sass');
    
	mix.copy('node_modules/sweetalert/dist/sweetalert.min.js', 'resources/assets/js');
	mix.copy('node_modules/sweetalert/dist/sweetalert.css', 'resources/assets/sass');

	mix.copy('node_modules/chart.js/dist/Chart.min.js', 'resources/assets/js');
	mix.copy('node_modules/tinymce/tinymce.min.js', 'resources/assets/js');
	mix.copy('node_modules/tinymce/themes/modern/theme.min.js', 'resources/assets/js');

    mix.sass(['foundation.min.css', 'sweetalert.css'], 'public/css/vendor.css');
    mix.sass(['app.scss', 'register.scss', 'pages.scss']);
   
    mix.scripts(['foundation.min.js', 'sweetalert.min.js', 'Chart.min.js', 'tinymce.min.js'], 'public/js/vendor.js');
    mix.scripts('theme.min.js', '/public/js/themes/modern/theme.js');
    mix.scripts('app.js');

});
